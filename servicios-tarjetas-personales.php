<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li class="active"><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">

                <div class="row">
                    <div class="col-md-12">
                        <a href="#" title="Elegí el tuyo" data-toggle="modal" data-target="#myModal">
                            <img src="images/disenos_personalizables.png" alt="Diseños Personalizables" class="img-responsive">
                        </a>
                    </div>
                </div>

                <!-- Modal -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">

                        <!-- Modal content-->
                        <div class="modal-content" style="width:460px; margin:0 auto; background-color: #2d2645;">
                            <div class="modal-body">

                                <h2 class="text-center">DISEÑOS PERSONALIZABLES</h2>

                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators">
                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#myCarousel" data-slide-to="1"></li>
                                        <li data-target="#myCarousel" data-slide-to="2"></li>
                                        <li data-target="#myCarousel" data-slide-to="3"></li>
                                        <li data-target="#myCarousel" data-slide-to="4"></li>
                                        <li data-target="#myCarousel" data-slide-to="5"></li>
                                        <li data-target="#myCarousel" data-slide-to="6"></li>
                                        <li data-target="#myCarousel" data-slide-to="7"></li>
                                        <li data-target="#myCarousel" data-slide-to="8"></li>
                                    </ol>

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        <div class="item active" data-slide-number="0">
                                            <img src="images/tarjetas-personales/01.jpg" alt="" class="img-responsive">
                                        </div>

                                        <div class="item" data-slide-number="1">
                                            <img src="images/tarjetas-personales/02.jpg" alt="" class="img-responsive">
                                        </div>

                                        <div class="item" data-slide-number="2">
                                            <img src="images/tarjetas-personales/03.jpg" alt="" class="img-responsive">
                                        </div>

                                        <div class="item" data-slide-number="3">
                                            <img src="images/tarjetas-personales/04.jpg" alt="" class="img-responsive">
                                        </div>

                                        <div class="item" data-slide-number="4">
                                            <img src="images/tarjetas-personales/05.jpg" alt="" class="img-responsive">
                                        </div>

                                        <div class="item" data-slide-number="5">
                                            <img src="images/tarjetas-personales/06.jpg" alt="" class="img-responsive">
                                        </div>

                                        <div class="item" data-slide-number="6">
                                            <img src="images/tarjetas-personales/07.jpg" alt="" class="img-responsive">
                                        </div>

                                        <div class="item" data-slide-number="7">
                                            <img src="images/tarjetas-personales/08.jpg" alt="" class="img-responsive">
                                        </div>

                                        <div class="item" data-slide-number="8">
                                            <img src="images/tarjetas-personales/09.jpg" alt="" class="img-responsive">
                                        </div>
                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="carousel-control left" href="#myCarousel" data-slide="prev"><img src="images/arrowL.svg" style="width:35px; margin-left: 20px;"></a>
                                    <a class="carousel-control right" href="#myCarousel" data-slide="next"><img src="images/arrowR.svg" style="width:35px; margin-right: 20px;"></a>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <!-- thumb navigation carousel -->
                                <div class="col-md-12" id="slider-thumbs">
                                    <!-- thumb navigation carousel items -->
                                    <ul class="list-inline">
                                        <li>
                                            <a id="carousel-selector-0" class="selected">
                                                <img src="images/tarjetas-personales/01.jpg" style="width:25px;">
                                            </a>
                                        </li>
                                        <li>
                                            <a id="carousel-selector-1">
                                                <img src="images/tarjetas-personales/02.jpg" style="width:25px;">
                                            </a>
                                        </li>
                                        <li>
                                            <a id="carousel-selector-2">
                                                <img src="images/tarjetas-personales/03.jpg" style="width:25px;">
                                            </a>
                                        </li>
                                        <li>
                                            <a id="carousel-selector-3">
                                                <img src="images/tarjetas-personales/04.jpg" style="width:25px;">
                                            </a>
                                        </li>
                                        <li>
                                            <a id="carousel-selector-4">
                                                <img src="images/tarjetas-personales/05.jpg" style="width:25px;">
                                            </a>
                                        </li>
                                        <li>
                                            <a id="carousel-selector-5">
                                                <img src="images/tarjetas-personales/06.jpg" style="width:25px;">
                                            </a>
                                        </li>
                                        <li>
                                            <a id="carousel-selector-6">
                                                <img src="images/tarjetas-personales/07.jpg" style="width:25px;">
                                            </a>
                                        </li>
                                        <li>
                                            <a id="carousel-selector-7">
                                                <img src="images/tarjetas-personales/08.jpg" style="width:25px;">
                                            </a>
                                        </li>
                                        <li>
                                            <a id="carousel-selector-8">
                                                <img src="images/tarjetas-personales/09.jpg" style="width:25px;">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h2>LISTA DE PRECIOS</h2>
                <h3>TARJETAS PERSONALES CUADRADAS 6x6 CM</h3>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Simple Faz</th>
                            <th>Adicional Laminado S/F</th>
                            <th>Doble Faz</th>
                            <th>Adicional Laminado D/F</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>x100</td>
                            <td>$126</td>
                            <td>$60</td>
                            <td>$195</td>
                            <td>$70</td>
                        </tr>
                        <tr>
                            <td>x200</td>
                            <td>$215</td>
                            <td>$80</td>
                            <td>$314</td>
                            <td>$85</td>
                        </tr>
                        <tr>
                            <td>x300</td>
                            <td>$259</td>
                            <td>$95</td>
                            <td>$374</td>
                            <td>$100</td>
                        </tr>
                        <tr>
                            <td>x400</td>
                            <td>$325</td>
                            <td>$115</td>
                            <td>$534</td>
                            <td>$165</td>
                        </tr>
                        <tr>
                            <td>x500</td>
                            <td>$369</td>
                            <td>$140</td>
                            <td>$633</td>
                            <td>$180</td>
                        </tr>
                        <tr>
                            <td>x1.000</td>
                            <td>$608</td>
                            <td>$245</td>
                            <td>$980</td>
                            <td>$320</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5">*Papel ilustracion 300gr, full color</td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <h3>TARJETAS PERSONALES RECTANGULARES 9x5 CM</h3>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Simple Faz</th>
                            <th>Adicional Laminado S/F</th>
                            <th>Doble Faz</th>
                            <th>Adicional Laminado D/F</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>x100</td>
                            <td>$155</td>
                            <td>$70</td>
                            <td>$230</td>
                            <td>$80</td>
                        </tr>
                        <tr>
                            <td>x200</td>
                            <td>$286</td>
                            <td>$90</td>
                            <td>$434</td>
                            <td>$115</td>
                        </tr>
                        <tr>
                            <td>x300</td>
                            <td>$357</td>
                            <td>$110</td>
                            <td>$566</td>
                            <td>$160</td>
                        </tr>
                        <tr>
                            <td>x400</td>
                            <td>$450</td>
                            <td>$125</td>
                            <td>$764</td>
                            <td>$190</td>
                        </tr>
                        <tr>
                            <td>x500</td>
                            <td>$517</td>
                            <td>$155</td>
                            <td>$808</td>
                            <td>$210</td>
                        </tr>
                        <tr>
                            <td>x1.000</td>
                            <td>$853</td>
                            <td>$250</td>
                            <td>$1195</td>
                            <td>$340</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5">*Papel ilustracion 300gr, full color.</td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <h3>TARJETAS RECTANGULARES 9x5 CM en Papeles especiales</h3>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>MISIONERO 250gr (Simple Faz)</th>
                            <th>MISIONERO 250gr (Doble Faz)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>x100</td>
                            <td>$210</td>
                            <td>$325</td>
                        </tr>
                        <tr>
                            <td>x200</td>
                            <td>$415</td>
                            <td>$538</td>
                        </tr>
                        <tr>
                            <td>x300</td>
                            <td>$580</td>
                            <td>$795</td>
                        </tr>
                        <tr>
                            <td>x400</td>
                            <td>$725</td>
                            <td>$975</td>
                        </tr>
                        <tr>
                            <td>x500</td>
                            <td>$845</td>
                            <td>$1156</td>
                        </tr>
                        <tr>
                            <td>x1.000</td>
                            <td>$1307</td>
                            <td>$1677</td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>SUNDACE 300gr (Simple Faz)</th>
                            <th>SUNDACE 300gr (Doble Faz)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>x100</td>
                            <td>$270</td>
                            <td>$356</td>
                        </tr>
                        <tr>
                            <td>x200</td>
                            <td>$510</td>
                            <td>$680</td>
                        </tr>
                        <tr>
                            <td>x300</td>
                            <td>$650</td>
                            <td>$878</td>
                        </tr>
                        <tr>
                            <td>x400</td>
                            <td>$810</td>
                            <td>$1169</td>
                        </tr>
                        <tr>
                            <td>x500</td>
                            <td>$900</td>
                            <td>$1230</td>
                        </tr>
                        <tr>
                            <td>x1.000</td>
                            <td>$1395</td>
                            <td>$1785</td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>RECICLADO BEIGE 250gr (Simple Faz)</th>
                            <th>RECICLADO BEIGE 250gr (Doble Faz)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>x100</td>
                            <td>$190</td>
                            <td>$295</td>
                        </tr>
                        <tr>
                            <td>x200</td>
                            <td>$356</td>
                            <td>$558</td>
                        </tr>
                        <tr>
                            <td>x300</td>
                            <td>$454</td>
                            <td>$778</td>
                        </tr>
                        <tr>
                            <td>x400</td>
                            <td>$569</td>
                            <td>$878</td>
                        </tr>
                        <tr>
                            <td>x500</td>
                            <td>$656</td>
                            <td>$995</td>
                        </tr>
                        <tr>
                            <td>x1.000</td>
                            <td>$1017</td>
                            <td>$1385</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">COSTOS ADICIONALES<br/>
                                Corte mínimo y x100 hojas: $150<br/>
                                Perforado x500: $150 | x1.000: $300<br/>
                                Redondeo de Puntas Tarjetas x500: $200 | x1000: $350
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <h3>TERMINACIONES ESPECIALES - TARJETAS RECTANGULARES 9x5 CM</h3>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Simple Faz</th>
                            <th>Doble Faz</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>x1000</td>
                            <td>$1720</td>
                            <td>$1820</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">ILUSTRACIÓN 350 GR. + POLIPROPILENO MATE + LACA SECTORIZADA<br/>
                                *Demora entre 10 a 20 días hábiles - Entrega a convenir
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <h3>ETIQUETAS DE ROPA - 4x5 CM / 9x2,5 CM EN ILUSTRACIÓN 300 GR + PERFORACIÓN</h3>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Simple faz (Blanco y Negro)</th>
                            <th>Simple faz (Color)</th>
                            <th>Doble faz (Blanco y Negro)</th>
                            <th>Doble faz (Color)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>x500</td>
                            <td>$475</td>
                            <td>$595</td>
                            <td>$755</td>
                            <td>$875</td>
                        </tr>
                        <tr>
                            <td>x1000</td>
                            <td>$955</td>
                            <td>$1075</td>
                            <td>$1195</td>
                            <td>$1315</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5">*La perforacion es ovalada menos de 500 unidades, apartir de 1000 unidades la perforacion es circular<br/>
                                *Los precios pueden modificarse sin previo aviso.<br/>
                                *Los precios no incluyen IVA.
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <br>

                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2>INSTRUCTIVO ARMADO DE ARCHIVOS</h2>
                        <p style="text-align:center;"><a href="dual_color_tarjetas_personales.pdf"><button type="button" class="btn btn-default">Descargar PDF</button></a></p>
                        <h2 style="margin-top:45px;">ESPECIFICACIONES Y CONSEJOS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <h3>TARJETAS PERSONALES</h3>
                        <ul>
                            <li>Las tarjetas personales son impresas en papel ilustración de 300 gr mate ó brillante (a elección). En el caso de encargarlas sobre papeles especiales, el uso de los mismos se cotizará aparte.</li>
                            <li>Aceptamos archivos preferentemente en formato .AI o .PDF editable. En el caso de ser formato .JPG, debe contener demasía y poseer entre 150 y 300 dpi de resolución.</li>
                            <li>Convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas. En el caso de no incrustarlas, guardar las imágenes utilizadas en una carpeta.</li>
                            <li>El archivo debe tener una demasía mínima de 3 mm de cada lado.</li>
                        </ul>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>