CREATE TABLE `usuario` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `usuario`       VARCHAR(20),
    `clave`         VARCHAR(60)
);

CREATE TABLE `cliente` (
    `id`        INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`    VARCHAR(20),
    `telefono`  VARCHAR(20),
    `celular`   VARCHAR(20),
    `mail`      VARCHAR(40),
    `direccion` VARCHAR(30)
);

CREATE TABLE `responsable` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        VARCHAR(20)
);

CREATE TABLE `producto` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `descripcion`   VARCHAR(20)
);

CREATE TABLE `categoria` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        VARCHAR(25)
);

CREATE TABLE `formatoCategoria` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        VARCHAR(20),
    `categoria_id`  INTEGER,

    FOREIGN KEY(`categoria_id`) REFERENCES categoria(id)
);

CREATE TABLE `materialCategoria` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        VARCHAR(20),
    `categoria_id`  INTEGER,

    FOREIGN KEY(`categoria_id`) REFERENCES categoria(id)
);

CREATE TABLE `tipoCategoria` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        VARCHAR(30),
    `categoria_id`  INTEGER,

    FOREIGN KEY(`categoria_id`) REFERENCES categoria(id)
);

CREATE TABLE `colorTipo` (
    `id`           		INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`      	  	VARCHAR(20),
    `tipoCategoria_id`	INTEGER,

    FOREIGN KEY(`tipoCategoria_id`) REFERENCES tipoCategoria(id)
);

CREATE TABLE `colorCategoria` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        VARCHAR(20),
    `categoria_id`  INTEGER,

    FOREIGN KEY(`categoria_id`) REFERENCES categoria(id)
);

CREATE TABLE `tamanoTipo` (
    `id`           		INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`       		VARCHAR(20),
    `tipoCategoria_id`  INTEGER,

    FOREIGN KEY(`tipoCategoria_id`) REFERENCES tipoCategoria(id)
);

CREATE TABLE `colorTamano` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        VARCHAR(20),
    `tamanoTipo_id` INTEGER,

    FOREIGN KEY(`tamanoTipo_id`) REFERENCES tamanoTipo(id)
);

CREATE TABLE `tamanoMaterial` (
    `id`            		INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`      			VARCHAR(20),
    `materialCategoria_id`	INTEGER,

    FOREIGN KEY(`materialCategoria_id`) REFERENCES materialCategoria(id)
);

CREATE TABLE `tamanoColor` (
    `id`             	INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`            VARCHAR(20),
    `colorCategoria_id` INTEGER,

    FOREIGN KEY(`colorCategoria_id`) REFERENCES colorCategoria(id)
);

CREATE TABLE `tamanoCategoria` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        VARCHAR(20),
    `categoria_id`  INTEGER,

    FOREIGN KEY(`categoria_id`) REFERENCES categoria(id)
);

CREATE TABLE `materialTamano` (
    `id`                    INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`                VARCHAR(35),
    `tamanoCategoria_id`    INTEGER,

    FOREIGN KEY(`tamanoCategoria_id`) REFERENCES tamanoCategoria(id)
);

CREATE TABLE `papelTipo` (
    `id`       			INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`  			VARCHAR(25),
    `tipoCategoria_id` 	INTEGER,

    FOREIGN KEY(`tipoCategoria_id`) REFERENCES tipoCategoria(id)
);

CREATE TABLE `cantidadColor` (
    `id`                    INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`                VARCHAR(20),
    `colorCategoria_id`    INTEGER,

    FOREIGN KEY(`colorCategoria_id`) REFERENCES colorCategoria(id)
);

CREATE TABLE `cantidadTipo` (
    `id`            	INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`       		VARCHAR(20),
    `tipoCategoria_id`  INTEGER,

    FOREIGN KEY(`tipoCategoria_id`) REFERENCES tipoCategoria(id)
);

CREATE TABLE `cantidadCategoria` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        VARCHAR(20),
    `categoria_id`  INTEGER,

    FOREIGN KEY(`categoria_id`) REFERENCES categoria(id)
);

CREATE TABLE `cantidadMaterial` (
    `id`           			INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        		VARCHAR(20),
    `materialCategoria_id`  INTEGER,

    FOREIGN KEY(`materialCategoria_id`) REFERENCES materialCategoria(id)
);

CREATE TABLE `cantidadTamanoColor` (
    `id`                    INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`                VARCHAR(20),
    `tamanoColor_id`        INTEGER,

    FOREIGN KEY(`tamanoColor_id`) REFERENCES tamanoColor(id)
);

CREATE TABLE `cantidadTamanoTipo` (
    `id`                    INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`                VARCHAR(20),
    `tamanoTipo_id`         INTEGER,

    FOREIGN KEY(`tamanoTipo_id`) REFERENCES tamanoTipo(id)
);

CREATE TABLE `caraCategoria` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        VARCHAR(20),
    `categoria_id`  INTEGER,

    FOREIGN KEY(`categoria_id`) REFERENCES categoria(id)
);

CREATE TABLE `caraTamano` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        VARCHAR(20),
    `tamanoTipo_id` INTEGER,

    FOREIGN KEY(`tamanoTipo_id`) REFERENCES tamanoTipo(id)
);

CREATE TABLE `acabadoCategoria` (
    `id`       		INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`   		VARCHAR(20),
    `categoria_id`	INTEGER,

    FOREIGN KEY(`categoria_id`) REFERENCES categoria(id)
);

CREATE TABLE `solapaTipo` (
    `id`       			INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`   			VARCHAR(20),
    `tipoCategoria_id`	INTEGER,

    FOREIGN KEY(`tipoCategoria_id`) REFERENCES tipoCategoria(id)
);

CREATE TABLE `laminadoTipo` (
    `id`       			INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`   			VARCHAR(20),
    `tipoCategoria_id`	INTEGER,

    FOREIGN KEY(`tipoCategoria_id`) REFERENCES tipoCategoria(id)
);

CREATE TABLE `laminadoTamano` (
    `id`            INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`        VARCHAR(20),
    `tamanoTipo_id` INTEGER,

    FOREIGN KEY(`tamanoTipo_id`) REFERENCES tamanoTipo(id)
);

CREATE TABLE `brilloTipo` (
    `id`       			INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`   			VARCHAR(20),
    `tipoCategoria_id`	INTEGER,

    FOREIGN KEY(`tipoCategoria_id`) REFERENCES tipoCategoria(id)
);

CREATE TABLE `perforacionTipo` (
    `id`       			INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`   			VARCHAR(20),
    `tipoCategoria_id`	INTEGER,

    FOREIGN KEY(`tipoCategoria_id`) REFERENCES tipoCategoria(id)
);

CREATE TABLE `estado` (
    `id`                INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `nombre`            VARCHAR(20)
);

CREATE TABLE `pedido` (
    `id`                        INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `orden`                     INTEGER,
    `entrega`                   DATE,
    `otro_detalle`              VARCHAR(50),
    `medida`                    INTEGER,
    `cantidad`                  INTEGER,
    `notas`                     TEXT,
    `entregaEstimada`           DATE,
    `sena`                      FLOAT,
    `total`                     FLOAT,
    `formaPago`                 INTEGER(4),
    `pedidoFile`                VARCHAR(100),
    `categoria_id`              INTEGER,
    `formatoCategoria_id`       INTEGER,
    `materialCategoria_id`      INTEGER,
    `materialTamano_id`         INTEGER,
    `tamanoMaterial_id`         INTEGER,
    `tamanoColor_id`    	    INTEGER,
    `tamanoTipo_id`             INTEGER,
    `tamanoCategoria_id`        INTEGER,
    `tipoCategoria_id`          INTEGER,
    `colorTipo_id`              INTEGER,
    `colorCategoria_id`         INTEGER,
    `colorTamano_id` 		    INTEGER,
    `papelTipo_id`              INTEGER,
    `cantidadColor_id`    	    INTEGER,
    `cantidadTipo_id`           INTEGER,
    `cantidadCategoria_id`      INTEGER,
    `cantidadMaterial_id`  	    INTEGER,
    `cantidadTamanoColor_id`    INTEGER,
    `cantidadTamanoTipo_id`     INTEGER,
    `caraCategoria_id`	        INTEGER,
    `caraTamano_id`		        INTEGER,
    `acabadoCategoria_id`       INTEGER,
    `solapaTipo_id`			    INTEGER,
    `laminadoTipo_id`		    INTEGER,
    `laminadoTamano_id`		    INTEGER,
    `brilloTipo_id`			    INTEGER,
    `perforacionTipo_id`	    INTEGER,
    `cliente_id`                INTEGER,
    `producto_id`               INTEGER,
    `responsable_id`            INTEGER,
    `estado_id`                 INTEGER(4),

    FOREIGN KEY(`categoria_id`) REFERENCES categoria(id),
    FOREIGN KEY(`formatoCategoria_id`) REFERENCES formatoCategoria(id),
    FOREIGN KEY(`materialCategoria_id`) REFERENCES materialCategoria(id),
    FOREIGN KEY(`materialTamano_id`) REFERENCES materialTamano(id),
    FOREIGN KEY(`tamanoMaterial_id`) REFERENCES tamanoMaterial(id),
    FOREIGN KEY(`tamanoColor_id`) REFERENCES tamanoColor(id),
    FOREIGN KEY(`tamanoTipo_id`) REFERENCES tamanoTipo(id),
    FOREIGN KEY(`tamanoCategoria_id`) REFERENCES tamanoCategoria(id),
    FOREIGN KEY(`tipoCategoria_id`) REFERENCES tipoCategoria(id),
    FOREIGN KEY(`colorTipo_id`) REFERENCES colorTipo(id),
    FOREIGN KEY(`colorCategoria_id`) REFERENCES colorCategoria(id),
    FOREIGN KEY(`colorTamano_id`) REFERENCES colorTamano(id),
    FOREIGN KEY(`papelTipo_id`) REFERENCES papelTipo(id),
    FOREIGN KEY(`cantidadColor_id`) REFERENCES cantidadColor(id),
    FOREIGN KEY(`cantidadTipo_id`) REFERENCES cantidadTipo(id),
    FOREIGN KEY(`cantidadCategoria_id`) REFERENCES cantidadCategoria(id),
    FOREIGN KEY(`cantidadMaterial_id`) REFERENCES cantidadMaterial(id),
    FOREIGN KEY(`cantidadTamanoColor_id`) REFERENCES cantidadTamanoColor(id),
    FOREIGN KEY(`caraCategoria_id`) REFERENCES caraCategoria(id),
    FOREIGN KEY(`caraTamano_id`) REFERENCES caraCategoria(id),
    FOREIGN KEY(`acabadoCategoria_id`) REFERENCES acabadoCategoria(id),
    FOREIGN KEY(`solapaTipo_id`) REFERENCES solapaTipo(id),
    FOREIGN KEY(`laminadoTipo_id`) REFERENCES laminadoTipo(id),
    FOREIGN KEY(`laminadoTamano_id`) REFERENCES laminadoTamano(id),
    FOREIGN KEY(`brilloTipo_id`) REFERENCES brilloTipo(id),
    FOREIGN KEY(`perforacionTipo_id`) REFERENCES perforacionTipo(id),
    FOREIGN KEY(`cliente_id`) REFERENCES cliente(id),
    FOREIGN KEY(`producto_id`) REFERENCES producto(id),
    FOREIGN KEY(`responsable_id`) REFERENCES responsable(id),
    FOREIGN KEY(`estado_id`) REFERENCES estado(id)
);

INSERT INTO `usuario` (`id`, `usuario`, `clave`) VALUES (1, 'admin', '$2y$10$TMb669GtHUGpqTA7g3RPkOCu.l0ob9.8uTkfqUzLwBVadVreEWyQq');
INSERT INTO `categoria` (`id`, `nombre`) VALUES (1, 'Anillados'), (2, 'Carteleria'), (3, 'Folletería'), (4, 'Imanes'), (5, 'Impresiones Digitales'), (6, 'Laminado'), (7, 'Pines'), (8, 'Revistas'), (9, 'Stickers'), (10, 'Tarjeteria'), (11, 'Otro');
INSERT INTO `tipoCategoria` (`id`, `nombre`, `categoria_id`) VALUES (1, 'Lona Front', 2), (2, 'Lona Black out', 2), (3, 'Vinilo Interior blanco', 2), (4, 'Vinilo Exterior blanco', 2), (9, 'Vinilo Textil', 2), ('11', 'Plancha', '4'), ('12', 'Cantidad', '4'), ('13', 'Formas', '4'), ('14', 'Blocks', '8'), ('15', 'Carpetas', '8'), ('16', 'Catálogos', '8');
INSERT INTO `colorTipo` (`id`, `nombre`, `tipoCategoria_id`) VALUES (1, 'Blanco', 9), (2, 'Cian', 9);
INSERT INTO `colorCategoria` (`id`, `nombre`, `categoria_id`) VALUES (1, 'Blanco y Negro', 3), (2, 'Full Color', 3);
INSERT INTO `caraCategoria` (`id`, `nombre`, `categoria_id`) VALUES (1, 'Frente', 3), (2, 'Frente y Dorso', 3), (3, 'Simple Faz',  '6'), (4, 'Doble Faz', '6');
INSERT INTO `tamanoColor` (`id`, `nombre`, `colorCategoria_id`) VALUES (1, '11 x 17 cm', 1), (2, '11 x 22 cm', 1), (3, '7 x 10 cm', 1), (4, '10 x 15 cm', 1);
INSERT INTO `materialCategoria` (`id`, `nombre`, `categoria_id`) VALUES (1, 'Plástico', 1), (2, 'Metal', 1);
INSERT INTO `cantidadMaterial` (`id`, `nombre`, `materialCategoria_id`) VALUES (1, '50', 1), (2, '100', 1), (3, '150', 1), (4, '200', 1), (5, '300', 1);
INSERT INTO `cantidadTamanoColor` (`id`, `nombre`, `tamanoColor_id`) VALUES (1, '1000', 1), (2, '2000', 1);
INSERT INTO `formatoCategoria` (`id`, `nombre`, `categoria_id`) VALUES (1, 'A3', 1), (2, 'A4', 1);
INSERT INTO `tamanoMaterial` (`id`, `nombre`, `materialCategoria_id`) VALUES (1, 'Chico', 2), (2, 'Mediano', 2), (3, 'Grande', 2), (4, 'Extra Grande', 2);
INSERT INTO `cliente` (`id`, `nombre`) VALUES (1, 'Daniel');
INSERT INTO `responsable` (`id`, `nombre`) VALUES (1, 'Raquel');
INSERT INTO `tamanoTipo` (`id`, `nombre`, `tipoCategoria_id`) VALUES (1, 'A4', '11'), (2, 'A3', '11'), (3, 'SA3', '11'), (4, '6 x 4 cm', '12'), (5, '7 x 5 cm', '12'), (6, '8 x 6 cm', '12'), (7, '10 x 7 cm', '12'), (8, '10 x 15 cm', '12'), (9, '11 x 17 cm', '14'), (10, '22 x 17 cm', '14'), (11, 'A4', '15'), (12, 'A5', '16'), (13, 'A4', '16');
INSERT INTO `papelTipo` (`id`, `nombre`, `tipoCategoria_id`) VALUES (1, 'Ilustración mate 170', 11), (2, 'Ilustración brillo 170', 11), (3, 'Ilustración mate 250', 11), (4, 'Ilustración brillo 250', 11), (5, 'Ilustración mate 300', 11), (6, 'Ilustración brillo 300', 11);
INSERT INTO `cantidadCategoria` (`id`, `nombre`, `categoria_id`) VALUES (1, '1 a 15', '5'), (2, '16 a 30', '5'), (3, '31 a 50', '5'), (4, '51 a 100', '5'), (5, '101 a 200', '5'), (6, '201 a 300', '5'), (7, '301 a 499', '5'), (8, 'Más de 500', '5'), (9, '1', '7'), (10, '10', '7'), (11, '20', '7'), (12, '50', '7'), (13, '100', '7'), (14, '200', '7');
INSERT INTO `tamanoCategoria` (`id`, `nombre`, `categoria_id`) VALUES (1, 'DIGITAL A4', '5'), (2, 'DIGITAL A3', '5'), (3, 'DIGITAL SA3', '5'), (4, 'A4', '6'), (5, 'A3', '6'), (6, 'SA3', '6'), (7, '56 mm', '7');
INSERT INTO `materialTamano` (`id`, `nombre`, `tamanoCategoria_id`) VALUES (1, 'Obra láser color 80 gr', '1'), (2, 'Obra láser ByN 80 gr', '1'), (3, 'Obra láser ByN 80 gr (texto)', '1'), (4, 'Fotocopia láser blanco y negro', '1'), (5, 'Ilustración mate / brillo 115 gr', '1'), (6, 'Ilustración mate / brillo 150 gr', '1'), (7, 'Ilustración mate / brillo 250 gr', '1'), (8, 'Ilustración mate / brillo 300 gr', '1'), (9, 'Opalina 250 gr', '1'), (10, 'Autoadhesivo mate', '1'), (11, 'Autoadhesivo brillo', '1'), (12, 'Autoadhesivo transparente', '1'), (13, 'Bookcel 90 gr', '1'), (14, 'Bookcel 90 gr ByN (texto)', '1'), (15, 'Sulfito', '1'), (16, 'Papel vegetal 90 gr', '1'), (17, 'Filmina', '1'), (18, 'Misionero 250 gr', '1'), (19, 'Natura 75 gr', '1'), (20, 'Natura 75 gr ByN (texto)', '1'), (21, 'Sobre papeles especiales', '1'), (22, 'Sobre papeles esp. ByN (texto)', '1'), (23, 'Si traés tu papel', '1'), (24, 'Si traés tu papel ByN (texto)', '1'), (25, 'Obra láser color 80 gr', '2'), (26, 'Obra láser ByN 80 gr', '2'), (27, 'Obra láser ByN 80 gr (texto)', '2'), (28, 'Fotocopia láser blanco y negro', '2'), (29, 'Ilustración mate / brillo 115 gr', '2'), (30, 'Ilustración mate / brillo 150 gr', '2'), (31, 'Ilustración mate / brillo 250 gr', '2');
INSERT INTO `acabadoCategoria` (`id`, `nombre`, `categoria_id`) VALUES (1, 'Brillo',  '6'), (2, 'Mate', '6');
INSERT INTO `colorTamano` (`id`, `nombre`, `tamanoTipo_id`) VALUES (1, '1',  '9'), (2, '2', '9'), (3, '1',  '10');
INSERT INTO `estado` (`id`, `nombre`) VALUES (1, 'Ingresado'), (2, 'En espera'), (3, 'En impresión'), (4, 'Terminado'), (5, 'Entregado'), (6, 'Frenado'), (7, 'Cancelado');