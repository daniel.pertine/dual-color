<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li class="active"><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2>LISTA DE PRECIOS</h2>
                <h3>RECETARIOS</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>11x17 cm a 1 color</th>
                                    <th>11x17 cm a 2 colores</th> 
                                    <th>22x17 cm a 1 color</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x10</td>
                                    <td>$822</td> 
                                    <td>$942</td>
                                    <td>$1062</td>
                                </tr>
                                <tr>
                                    <td>x20</td>
                                    <td>$1038</td> 
                                    <td>$1158</td>
                                    <td>$1506</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">x10 Talonarios en A4 a 2 colores: $1962<br/>
                                        *Los precios pueden modificarse sin previo aviso.<br/>
                                        *Los precios no incluyen IVA.
                                    </td>
                                </tr>
                            </tfoot>
                        </table>                
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2>ESPECIFICACIONES Y CONSEJOS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <ul>
                            <li>Los recetarios son impresos en papel obra de 80 grs, con o sin duplicado a elección.</li>
                            <li>Aceptamos archivos preferentemente en formato .AI o .PDF editable. En el caso de ser formato .JPG, debe contener demasía y poseer entre 150 y 300 dpi de resolución.</li>
                            <li>Convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas. En el caso de no incrustarlas, guardar las imágenes utilizadas en una carpeta.</li>
                            <li>El archivo debe tener una demasía mínima de 3 mm de cada lado.</li>
                        </ul>                       
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>