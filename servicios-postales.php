<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li class="active"><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2>LISTA DE PRECIOS</h2>
                <h3>POSTALES 10x15 CM</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Simple Faz</th>
                                    <th>Adicional Laminado S/F</th> 
                                    <th>Doble Faz</th>
                                    <th>Adicional Laminado D/F</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x100</td>
                                    <td>$273</td> 
                                    <td>$115</td>
                                    <td>$455</td>
                                    <td>$150</td>
                                </tr>
                                <tr>
                                    <td>x200</td>
                                    <td>$481</td> 
                                    <td>$150</td>
                                    <td>$663</td>
                                    <td>$250</td>
                                </tr>
                                <tr>
                                    <td>x300</td>
                                    <td>$663</td> 
                                    <td>$200</td>
                                    <td>$1053</td>
                                    <td>$355</td>
                                </tr>
                                <tr>
                                    <td>x400</td>
                                    <td>$793</td> 
                                    <td>$250</td>
                                    <td>$1287</td>
                                    <td>$455</td>
                                </tr>
                                <tr>
                                    <td>x500</td>
                                    <td>$875</td> 
                                    <td>$305</td>
                                    <td>$1414</td>
                                    <td>$530</td>
                                </tr>
                                <tr>
                                    <td>x1.000</td>
                                    <td>$1357</td> 
                                    <td>$475</td>
                                    <td>$1966</td>
                                    <td>$785</td>
                                </tr>
                            </tbody>
                        </table>                
                        <br>
                        REDONDEO DE PUNTAS x500: $250 | x1000: $300<br>
                    </div>
                </div>

                <h3>TARJETONES 15x20 CM</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Simple Faz</th>
                                    <th>Adicional Laminado S/F</th>
                                    <th>Doble Faz</th>
                                    <th>Adicional Laminado D/F</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x100</td>
                                    <td>$494</td>
                                    <td>$145</td>
                                    <td>$936</td>
                                    <td>$225</td>
                                </tr>
                                <tr>
                                    <td>x200</td>
                                    <td>$754</td>
                                    <td>$250</td>
                                    <td>$1274</td>
                                    <td>$410</td>
                                </tr>
                                <tr>
                                    <td>x300</td>
                                    <td>$975</td>
                                    <td>$335</td>
                                    <td>$1625</td>
                                    <td>$530</td>
                                </tr>
                                <tr>
                                    <td>x400</td>
                                    <td>$1170</td>
                                    <td>$430</td>
                                    <td>$1820</td>
                                    <td>$690</td>
                                </tr>
                                <tr>
                                    <td>x500</td>
                                    <td>$1260</td>
                                    <td>$475</td>
                                    <td>$2012</td>
                                    <td>$795</td>
                                </tr>
                                <tr>
                                    <td>x1.000</td>
                                    <td>$1980</td>
                                    <td>$875</td>
                                    <td>$2986</td>
                                    <td>$975</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>

                <h3>SEÑALADORES 5x15 CM</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Simple Faz</th>
                                    <th>Adicional Laminado S/F</th>
                                    <th>Doble Faz</th>
                                    <th>Adicional Laminado D/F</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x100</td>
                                    <td>$214</td>
                                    <td>$85</td>
                                    <td>$351</td>
                                    <td>$95</td>
                                </tr>
                                <tr>
                                    <td>x200</td>
                                    <td>$332</td>
                                    <td>$120</td>
                                    <td>$520</td>
                                    <td>$180</td>
                                </tr>
                                <tr>
                                    <td>x300</td>
                                    <td>$416</td>
                                    <td>$135</td>
                                    <td>$663</td>
                                    <td>$240</td>
                                </tr>
                                <tr>
                                    <td>x400</td>
                                    <td>$520</td>
                                    <td>$145</td>
                                    <td>$832</td>
                                    <td>$270</td>
                                </tr>
                                <tr>
                                    <td>x500</td>
                                    <td>$650</td>
                                    <td>$175</td>
                                    <td>$995</td>
                                    <td>$350</td>
                                </tr>
                                <tr>
                                    <td>x1.000</td>
                                    <td>$885</td>
                                    <td>$320</td>
                                    <td>$1344</td>
                                    <td>$550</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>

                <h3>SEÑALADORES 5x20 CM</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Simple Faz</th>
                                    <th>Adicional Laminado S/F</th>
                                    <th>Doble Faz</th>
                                    <th>Adicional Laminado D/F</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x100</td>
                                    <td>$247</td>
                                    <td>$95</td>
                                    <td>$410</td>
                                    <td>$125</td>
                                </tr>
                                <tr>
                                    <td>x200</td>
                                    <td>$338</td>
                                    <td>$145</td>
                                    <td>$559</td>
                                    <td>$210</td>
                                </tr>
                                <tr>
                                    <td>x300</td>
                                    <td>$429</td>
                                    <td>$210</td>
                                    <td>$798</td>
                                    <td>$275</td>
                                </tr>
                                <tr>
                                    <td>x400</td>
                                    <td>$585</td>
                                    <td>$270</td>
                                    <td>$936</td>
                                    <td>$310</td>
                                </tr>
                                <tr>
                                    <td>x500</td>
                                    <td>$697</td>
                                    <td>$310</td>
                                    <td>$1025</td>
                                    <td>$430</td>
                                </tr>
                                <tr>
                                    <td>x1.000</td>
                                    <td>$1050</td>
                                    <td>$430</td>
                                    <td>$1530</td>
                                    <td>$730</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5">*Los precios pueden modificarse sin previo aviso.<br/>
                                        *Los precios no incluyen IVA.
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2>ESPECIFICACIONES Y CONSEJOS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <ul>
                            <li>Postales, tarjetones y señaladores son impresos en papel ilustración de 300 gr mate ó brillante (a elección). En el caso de encargarlas sobre papeles especiales, el uso de los mismos se cotizará aparte.</li>
                            <li>Aceptamos archivos preferentemente en formato .AI o .PDF editable. En el caso de ser formato .JPG, debe contener demasía y poseer entre 150 y 300 dpi de resolución.</li>
                            <li>Convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas. En el caso de no incrustarlas, guardar las imágenes utilizadas en una carpeta.</li>
                            <li>El archivo debe tener una demasía mínima de 3 mm de cada lado.</li>
                        </ul>                       
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>