<?php
    include 'inc/header.php';
?>

    <div class="container">
        <div class="row">
            <div class='list-group gallery'>
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo01.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo01.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>1. Vinilo Home</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo02.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo02.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>2. Vinilo Imaginar</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo03.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo03.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>3. Vinilo Paz</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo04.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo04.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>4. Vinilo Beautiful</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo05.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo05.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>5. Vinilo Adventures</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo06.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo06.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>6. Vinilo Gato</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo07.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo07.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>7. Vinilo Perro</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo08.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo08.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>8. Vinilo Historia</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo09.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo09.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>9. Vinilo Ride</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo10.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo10.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>10. Vinilo Cerati</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo11.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo11.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>11. Vinilo Bienvenidos</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo12.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo12.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>12. Vinilo Smile</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo13.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo13.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>13. Vinilo Rey</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo14.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo14.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>14. Vinilo Reina</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo15.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo15.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>15. Vinilo Bien Lejos</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo16.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo16.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>16. Vinilo Think Big</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo17.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo17.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>17. Vinilo Sí</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo18.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo18.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>18. Vinilo Love</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo19.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo19.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>19. Vinilo Días</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
                <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                    <a class="thumbnail fancybox" rel="ligthbox" href="images/vinilos/vinilo20.jpg">
                        <img class="img-responsive" alt="" src="images/vinilos/vinilo20.jpg" />
                        <div class='text-left'>
                            <small class='text-muted'><strong>20. Vinilo Sonríe</strong><br>&nbsp;&nbsp;&nbsp;Consultar colores</small>
                        </div> <!-- text-left / end -->
                    </a>
                </div> <!-- col-6 / end -->
            </div> <!-- list-group / end -->
        </div> <!-- row / end -->
    </div> <!-- container / end -->

<?php
    include 'inc/footer.php';
?>