<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li class="active"><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2>LISTA DE PRECIOS</h2>
                <h3>STICKERS 7x5 CM</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x100</td>
                                    <td>$234</td>
                                </tr>
                                <tr>
                                    <td>x200</td>
                                    <td>$306</td>
                                </tr>
                                <tr>
                                    <td>x300</td>
                                    <td>$360</td>
                                </tr>
                                <tr>
                                    <td>x400</td>
                                    <td>$444</td>
                                </tr>
                                <tr>
                                    <td>x500</td>
                                    <td>$588</td>
                                </tr>
                                <tr>
                                    <td>x1.000</td>
                                    <td>$912</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2">*Consultar por Stickers circulares individuales</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <br>
                <h3>PLANCHAS TROQUELADAS – ÁREA DE TROQUEL: 28x38 CM</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Adhesivo blanco</th>
                                    <th>Adhesivo Transparente</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x1</td>
                                    <td>$50</td> 
                                    <td>$75</td>
                                </tr>
                                <tr>
                                    <td>x5 o más</td>
                                    <td>$35</td> 
                                    <td>$60</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3">*La distancia mínima de corte entre cada trazo de troquel es de 2 mm.<br/>
                                    	*Los stickers deben tener una demasía mínima de 3 mm de cada lado<br/>
                                    	*Los precios pueden modificarse sin previo aviso.<br/>
                                        *Los precios no incluyen IVA.
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <br>
                
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2>INSTRUCTIVO ARMADO DE ARCHIVOS PARA TROQUELAR</h2>
                        <p style="text-align:center;"><a href="dual_color_base_troquel.pdf" target="_blank"><button type="button" class="btn btn-default">Descargar PDF</button></a></p>
                        
                        <h2 style="margin-top:45px;">ESPECIFICACIONES Y CONSEJOS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <ul>
                            <li>Los stickers con corte individual son impresos en papel ilustración autoadhesivo de 180 gr.</li>
                            <li>Para encargos de troquelados, utilizar el archivo disponible en esta web. El área de troquel es de 28x38 cm.</li>
                            <li>La distancia mínima de corte entre cada trazo de troquel es de 2 mm.</li>
                            <li>Aceptamos archivos en formato .AI o .PDF editable. </li>
                            <li>Convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas. En el caso de no incrustarlas, guardar las imágenes utilizadas en una carpeta. </li>
                            <li>Los stickers deben tener una demasía mínima de 3 mm de cada lado.</li>
                        </ul>                      
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>