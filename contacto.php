<?php
    include 'inc/header.php';
?>

    <div class="container-fluid noMargin" style="padding:0;">
        <div id="map_wrapper" style="height: 400px;">
            <div id="map_canvas" class="mapping" style="width: 100%;height: 100%;"></div>
        </div>
    </div>
    <div class="container contacto" style="margin-top: 20px;">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <h2>CONTACTANOS</h2>
                <p>Envianos tu mensaje para consultas y presupuestos.</p>
                <form action="contacto-do.php" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" id="" name="nombre" placeholder="Tu Nombre">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="" name="email" placeholder="Tu Email">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="" name="asunto" placeholder="Asunto">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="5" name="mensaje" placeholder="Mensaje"></textarea>
                    </div>
		 <div class="form-group">
			<div class="g-recaptcha" data-sitekey="6LcR1Q8UAAAAABqU_qZFPtZb4qiob7GI0lft1o4N"></div>
                    </div>
		<button id="legitSubmit" type="button" class="btn btn-default">Enviar</button>
		<input id="submit" type="submit" value="Enviar" style="display:none" /></p>
                </form>
            </div>
            <div class="col-md-5">
                <h3>DIRECCIÓN</h3>
                <p>Atencion al Publico:<br> Thames 2395, Palermo, Capital Federal</p>
                <p>Producción Grafica y Sublimacion en Gran Formato:<br> Nicaragua 4914, Palermo, Capital Federal</p>
                <h3>TELÉFONO</h3>
                <p>Atencion al Publico:<br> 011 4774-9186</p>
                <p>Producción Grafica y Sublimacion en Gran Formato:<br> 011 4831 0950</p>
                <h3>EMAIL</h3>
                <p>
                    Para consultas y presupuestos<br>
                    <a href="mailto:graficadualcolor@gmail.com">graficadualcolor@gmail.com</a>
                    <br>
                    <br>
                    Para pedidos de impresión<br>
                    <a href="mailto:pedidosdualcolor@gmail.com">pedidosdualcolor@gmail.com</a>
                    <br>
                    <br>
                    Para delivery FADU<br>
                    <a href="mailto:dualivery@gmail.com">dualivery@gmail.com</a>
                    <br>
                    <br>
                    Para Sublimacion en Gran Formato<br>
                    <a href="mailto:sublimaciondc@gmail.com">sublimaciondc@gmail.com</a>
                </p>
                <h3>HORARIOS</h3>
                <p>Atencion al Publico:</p>
                <p>
                    Thames 2395, Palermo, Capital Federal<br>
                    Lunes a Viernes: 8 a 20 hs.<br>
                    Sábados, Domingos y Feriados: 14 a 20 hs.<br>
                </p>
                <br>
                <p>Producción Grafica y Sublimacion en Gran Formato:</p>
                <p>
                    Nicaragua 4914,  Palermo, Capital Federal<br>
                    Lunes a Viernes: 9 a 18 hs.<br>
                    Sábados de 9 a 14hs.<br>
                    <a href="https://web.facebook.com/dualcolorgrafica/" class="icon-facebook" target="_blank" title="Seguinos en Facebook" style="margin-top:10px;float:left;"></a>
                    <a href="https://www.instagram.com/graficadualcolor/" target="_blank" title="Seguinos en Instagram"><img src="../images/instagram.svg" class="icon-instagram" style="display:block;margin-top:10px;margin-left:30px"/></a>
                </p>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>