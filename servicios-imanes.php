<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li class="active"><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2>LISTA DE PRECIOS</h2>
                <h3>IMANES RECTANGULARES</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>6x4 cm</th>
                                    <th>7x5 cm</th>
                                    <th>8x6 cm</th>
                                    <th>10x7 cm</th>
                                    <th>15x8 cm</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x250</td>
                                    <td>$588</td>
                                    <td>$708</td>
                                    <td>$948</td>
                                    <td>$1188</td>
                                    <td>$1788</td>
                                </tr>
                                <tr>
                                    <td>x500</td>
                                    <td>$708</td>
                                    <td>$948</td>
                                    <td>$1194</td>
                                    <td>$1788</td>
                                    <td>$2352</td>
                                </tr>
                                <tr>
                                    <td>x1.000</td>
                                    <td>$948</td>
                                    <td>$1194</td>
                                    <td>$1788</td>
                                    <td>$2352</td>
                                    <td>$3468</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6">Papel 300gr. | Full color (CMYK) | Puntas redondeadas | Laminado</td>
                                </tr>
                            </tfoot>
                        </table>
                        <br>
                    </div>
                </div>
                <h3>IMANES CON FORMAS – 7x5 CM DE DIÁMETRO APROX.</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Circular 5,7 cm</th>
                                    <th>Circular 7 cm</th>
                                    <th>Óvalo</th>
                                    <th>Teléfono</th>
                                    <th>Camioneta</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x250</td>
                                    <td>$636</td>
                                    <td>$936</td>
                                    <td>$912</td>
                                    <td>$816</td>
                                    <td>$816</td>
                                </tr>
                                <tr>
                                    <td>x500</td>
                                    <td>$1080</td>
                                    <td>$1344</td>
                                    <td>$1068</td>
                                    <td>$1092</td>
                                    <td>$1092</td>
                                </tr>
                                <tr>
                                    <td>x1.000</td>
                                    <td>$1524</td>
                                    <td>$2364</td>
                                    <td>$1548</td>
                                    <td>$1464</td>
                                    <td>$1464</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>
                <h3>MONTADO SOBRE PLANCHA DE IMÁN</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Tamaño</th>
                                    <th>Precio</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>A4</td>
                                    <td>$25 c/u + papel</td>
                                </tr>
                                <tr>
                                    <td>A3</td>
                                    <td>$45 c/u + papel</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <h3>PINES DE 5,6 CM</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>PRECIO</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x1</td>
                                    <td>$15</td>
                                </tr>
                                <tr>
                                    <td>x2</td>
                                    <td>$30</td>
                                </tr>
                                <tr>
                                    <td>x3</td>
                                    <td>$45</td>
                                </tr>
                                <tr>
                                    <td>x50</td>
                                    <td>$14 c/u</td>
                                </tr>
                                <tr>
                                    <td>x100</td>
                                    <td>$900 ($9 c/u)</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2">*LLAVEROS DE 5,6 CM - CONSULTAR<br/>
                                        *Los precios pueden modificarse sin previo aviso<br/>
                                        *Los precios no incluyen IVA
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <br>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>