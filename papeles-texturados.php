<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li class="active"><a href="papeles-texturados.php">Texturados</a></li>
                <li><a href="papeles-perlados.php">Perlados</a></li>
                <li><a href="papeles-calcos.php">Calcos</a></li>
                <li><a href="papeles-estandar.php">Estándar</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <div class="row">
                    <div class='list-group gallery'>
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_1.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_1.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 01</strong><br>&nbsp;&nbsp;&nbsp;118 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_2.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_2.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 02</strong><br>&nbsp;&nbsp;&nbsp;118 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_3.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_3.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 03</strong><br>&nbsp;&nbsp;&nbsp;118 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_4.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_4.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 04</strong><br>&nbsp;&nbsp;&nbsp;118 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_5.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_5.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 05</strong><br>&nbsp;&nbsp;&nbsp;118 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_6.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_6.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 06</strong><br>&nbsp;&nbsp;&nbsp;118 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_7.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_7.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 07</strong><br>&nbsp;&nbsp;&nbsp;120 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_8.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_8.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 08</strong><br>&nbsp;&nbsp;&nbsp;118 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_9.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_9.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 09</strong><br>&nbsp;&nbsp;&nbsp;118 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_10.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_10.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 10</strong><br>&nbsp;&nbsp;&nbsp;140 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_11.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_11.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 11</strong><br>&nbsp;&nbsp;&nbsp;118 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_12.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_12.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 12</strong><br>&nbsp;&nbsp;&nbsp;118 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_13.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_13.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 13 / 14</strong><br>&nbsp;&nbsp;&nbsp;118 / 250 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_15.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_15.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 15 / 16</strong><br>&nbsp;&nbsp;&nbsp;118 / 250 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_18.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_18.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 17 / 18</strong><br>&nbsp;&nbsp;&nbsp;118 / 250 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_19.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_19.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 19</strong><br>&nbsp;&nbsp;&nbsp;300 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_20.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_20.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 20</strong><br>&nbsp;&nbsp;&nbsp;300 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_21.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_21.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 21</strong><br>&nbsp;&nbsp;&nbsp;300 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_22.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_22.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 22</strong><br>&nbsp;&nbsp;&nbsp;300 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_23.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_23.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 23</strong><br>&nbsp;&nbsp;&nbsp;300 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_24.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_24.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 24</strong><br>&nbsp;&nbsp;&nbsp;250 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_25.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_25.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 25</strong><br>&nbsp;&nbsp;&nbsp;300 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_26.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_26.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 26</strong><br>&nbsp;&nbsp;&nbsp;250 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_27.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_27.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 27</strong><br>&nbsp;&nbsp;&nbsp;300 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_28.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_28.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 28</strong><br>&nbsp;&nbsp;&nbsp;300 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_29.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_29.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 29</strong><br>&nbsp;&nbsp;&nbsp;300 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_33.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_33.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 33</strong><br>&nbsp;&nbsp;&nbsp;300 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_34.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_34.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Texturado Nº 34</strong><br>&nbsp;&nbsp;&nbsp;300 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_35_turquesa.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_35_turquesa.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Nº 35 Turquesa</strong><br>&nbsp;&nbsp;&nbsp;90 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_35_celeste.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_35_celeste.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Nº 35 Celeste</strong><br>&nbsp;&nbsp;&nbsp;70 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_35_verde.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_35_verde.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Nº 35 Verde</strong><br>&nbsp;&nbsp;&nbsp;70 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_35_rosa.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_35_rosa.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Nº 35 Rosa</strong><br>&nbsp;&nbsp;&nbsp;70 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_35_amarillo.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_35_amarillo.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Nº 35 Amarillo</strong><br>&nbsp;&nbsp;&nbsp;70 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_36_amarillo.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_36_amarillo.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Nº 36 Fluo Amarillo</strong><br>&nbsp;&nbsp;&nbsp;90 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_36_naranja.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_36_naranja.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Nº 36 Fluo Naranja</strong><br>&nbsp;&nbsp;&nbsp;90 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_36_rosa.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_36_rosa.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Nº 36 Fluo Rosa</strong><br>&nbsp;&nbsp;&nbsp;90 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_36_verde.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_36_verde.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Nº 36 Fluo Verde</strong><br>&nbsp;&nbsp;&nbsp;90 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_natura.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_natura.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Natura</strong><br>&nbsp;&nbsp;&nbsp;80 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_bookcel.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_bookcel.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Bookcel</strong><br>&nbsp;&nbsp;&nbsp;80 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_sulfito.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_sulfito.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Sulfito</strong><br>&nbsp;&nbsp;&nbsp;70 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/texturados/papeles_slider_text_misionero.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/texturados/papeles_slider_text_misionero.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Misionero</strong><br>&nbsp;&nbsp;&nbsp;200 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                    </div> <!-- list-group / end -->
                </div> <!-- row / end -->
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>