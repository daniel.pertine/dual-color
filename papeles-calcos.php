<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="papeles-texturados.php">Texturados</a></li>
                <li><a href="papeles-perlados.php">Perlados</a></li>
                <li class="active"><a href="papeles-calcos.php">Calcos</a></li>
                <li><a href="papeles-estandar.php">Estándar</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <div class="row">
                    <div class='list-group gallery'>
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/calcos/papeles_slider_calc_16.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/calcos/papeles_slider_calc_16.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Calco Nº 16</strong><br>&nbsp;&nbsp;&nbsp;120 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/calcos/papeles_slider_calc_18.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/calcos/papeles_slider_calc_18.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Calco Nº 18</strong><br>&nbsp;&nbsp;&nbsp;250 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/calcos/papeles_slider_calc_19.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/calcos/papeles_slider_calc_19.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Calco Nº 19</strong><br>&nbsp;&nbsp;&nbsp;250 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/calcos/papeles_slider_calc_20.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/calcos/papeles_slider_calc_20.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Calco Nº 20</strong><br>&nbsp;&nbsp;&nbsp;90 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/calcos/papeles_slider_calc_21.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/calcos/papeles_slider_calc_21.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Calco Nº 21</strong><br>&nbsp;&nbsp;&nbsp;90 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/calcos/papeles_slider_calc_22.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/calcos/papeles_slider_calc_22.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Calco Nº 22</strong><br>&nbsp;&nbsp;&nbsp;90 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/calcos/papeles_slider_calc_23.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/calcos/papeles_slider_calc_23.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Calco Nº 23</strong><br>&nbsp;&nbsp;&nbsp;90 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                    </div> <!-- list-group / end -->
                </div> <!-- row / end -->
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>