<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                    <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                    <li><a href="servicios-ploteos.php">Ploteos</a></li>
                    <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                    <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                    <li class="active"><a href="servicios-volantes.php">Volantes</a></li>
                    <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                    <li><a href="servicios-stickers.php">Stickers</a></li>
                    <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                    <li><a href="servicios-recetarios.php">Recetarios</a></li>
                    <li><a href="servicios-carpetas.php">Carpetas</a></li>
                    <li><a href="servicios-laminados.php">Laminados</a></li>
                    <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                    <li><a href="servicios-revistas.php">Revistas</a></li>
                    <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2>LISTA DE PRECIOS</h2>
                <h3>VOLANTES A 1 COLOR - SIMPLE FAZ - PAPEL OBRA 80 GR.</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>11x17 cm</th>
                                    <th>11x22 cm</th> 
                                    <th>17x22 cm</th>
                                    <th>Oficio / A4</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x1.000</td>
                                    <td>$686</td> 
                                    <td>$772</td>
                                    <td>$928</td>
                                    <td>$1084</td>
                                </tr>
                                <tr>
                                    <td>x2.000</td>
                                    <td>$772</td> 
                                    <td>$928</td>
                                    <td>$1084</td>
                                    <td>$1396</td>
                                </tr>
                                <tr>
                                    <td>x3.000</td>
                                    <td>$928</td> 
                                    <td>$1084</td>
                                    <td>$1396</td>
                                    <td>$2005</td>
                                </tr>
                                <tr>
                                    <td>x4.000</td>
                                    <td>$1084</td> 
                                    <td>$1396</td>
                                    <td>$2005</td>
                                    <td>$2393</td>
                                </tr>
                                <tr>
                                    <td>x5.000</td>
                                    <td>$1396</td> 
                                    <td>$2005</td>
                                    <td>$2488</td>
                                    <td>$2873</td>
                                </tr>
                                <tr>
                                    <td>x10.000</td>
                                    <td>$1850</td> 
                                    <td>$2488</td>
                                    <td>$2873</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>x15.000</td>
                                    <td>-</td> 
                                    <td>$2873</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>x20.000</td>
                                    <td>$2873</td> 
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                            </tbody>
                        </table>                
                        <br>
                    </div>
                </div>

                <h3>VOLANTES A 1 COLOR - DOBLE FAZ - PAPEL OBRA 80 GR.</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>11x17 cm</th>
                                    <th>11x22 cm</th> 
                                    <th>17x22 cm</th>
                                    <th>Oficio / A4</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x1.000</td>
                                    <td>$893</td> 
                                    <td>$1004</td>
                                    <td>$1206</td>
                                    <td>$1409</td>
                                </tr>
                                <tr>
                                    <td>x2.000</td>
                                    <td>$1004</td> 
                                    <td>$1206</td>
                                    <td>$1409</td>
                                    <td>$1911</td>
                                </tr>
                                <tr>
                                    <td>x3.000</td>
                                    <td>$1206</td> 
                                    <td>$1411</td>
                                    <td>$1815</td>
                                    <td>$2607</td>
                                </tr>
                                <tr>
                                    <td>x4.000</td>
                                    <td>$1411</td> 
                                    <td>$1815</td>
                                    <td>$2607</td>
                                    <td>$3234</td>
                                </tr>
                                <tr>
                                    <td>x5.000</td>
                                    <td>$1815</td> 
                                    <td>$2607</td>
                                    <td>$3234</td>
                                    <td>$4046</td>
                                </tr>
                                <tr>
                                    <td>x10.000</td>
                                    <td>$2406</td> 
                                    <td>$2986</td>
                                    <td>$3734</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>x15.000</td>
                                    <td>-</td> 
                                    <td>$3734</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>x20.000</td>
                                    <td>$3734</td> 
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                            </tbody>
                        </table>                
                        <br>
                    </div>
                </div>

                <h3>FOLLETOS FULL COLOR - SIMPLE FAZ - PAPEL ILUSTRACIÓN 115 GR.</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>10x15 cm</th>
                                    <th>20x15 cm</th> 
                                    <th>30x15 cm</th>
                                    <th>20x30 cm / A4</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x100</td>
                                    <td>$195</td> 
                                    <td>$351</td>
                                    <td>$533</td>
                                    <td>$546</td>
                                </tr>
                                <tr>
                                    <td>x200</td>
                                    <td>$351</td> 
                                    <td>$578</td>
                                    <td>$948</td>
                                    <td>$1040</td>
                                </tr>
                                <tr>
                                    <td>x300</td>
                                    <td>$486</td> 
                                    <td>$858</td>
                                    <td>$1374</td>
                                    <td>$1501</td>
                                </tr>
                                <tr>
                                    <td>x400</td>
                                    <td>$572</td> 
                                    <td>$1092</td>
                                    <td>$1788</td>
                                    <td>$1950</td>
                                </tr>
                                <tr>
                                    <td>x500</td>
                                    <td>$689</td> 
                                    <td>$1222</td>
                                    <td>$2118</td>
                                    <td>$2307</td>
                                </tr>
                                <tr>
                                    <td>x1.000</td>
                                    <td>$1248</td> 
                                    <td>$1937</td>
                                    <td>-</td>
                                    <td>$3796</td>
                                </tr>
                                <tr>
                                    <td>x5.000</td>
                                    <td>$1364</td> 
                                    <td>$1964</td>
                                    <td>$3550</td>
                                    <td>$3961</td>
                                </tr>
                                <tr>
                                    <td>x10.000</td>
                                    <td>$2289</td> 
                                    <td>$3644</td>
                                    <td>-</td>
                                    <td>$6216</td>
                                </tr>
                            </tbody>
                        </table>                
                        <br>
                        *Demora entre 5 a 8 días hábiles<br>
                        COSTOS ADICIONALES<br>
                        REDONDEO DE PUNTAS<br>
                        Folletos: x1000: $435 | x5000: $735<br>
                        SERVICIO DE DOBLADO<br>
                        Dípticos, trípticos, hasta 150gr.: x5000: consultar precio<br>
                    </div>
                </div>

                <h3>FOLLETOS FULL COLOR - DOBLE FAZ - PAPEL ILUSTRACIÓN 115 GR.</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>10x15 cm</th>
                                    <th>20x15 cm</th> 
                                    <th>30x15 cm</th>
                                    <th>20x30 cm / A4</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x100</td>
                                    <td>$351</td> 
                                    <td>$624</td>
                                    <td>$773</td>
                                    <td>$1014</td>
                                </tr>
                                <tr>
                                    <td>x200</td>
                                    <td>$631</td> 
                                    <td>$949</td>
                                    <td>$1280</td>
                                    <td>$1976</td>
                                </tr>
                                <tr>
                                    <td>x300</td>
                                    <td>$884</td> 
                                    <td>$1281</td>
                                    <td>$1786</td>
                                    <td>$2496</td>
                                </tr>
                                <tr>
                                    <td>x400</td>
                                    <td>$1066</td> 
                                    <td>$1586</td>
                                    <td>$2324</td>
                                    <td>$2756</td>
                                </tr>
                                <tr>
                                    <td>x500</td>
                                    <td>$1118</td> 
                                    <td>$1716</td>
                                    <td>$2753</td>
                                    <td>$3406</td>
                                </tr>
                                <tr>
                                    <td>x1.000</td>
                                    <td>$1560</td> 
                                    <td>$2847</td>
                                    <td>-</td>
                                    <td>$4446</td>
                                </tr>
                                <tr>
                                    <td>x5.000*</td>
                                    <td>$1672</td> 
                                    <td>$2761</td>
                                    <td>$3820</td>
                                    <td>$4704</td>
                                </tr>
                                <tr>
                                    <td>x10.000*</td>
                                    <td>$2882</td> 
                                    <td>$4312</td>
                                    <td>-</td>
                                    <td>$8272</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5">*Demora entre 5 a 8 días hábiles<br/>
                                        **Los precios pueden modificarse sin previo aviso.<br/>
                                        **Los precios no incluyen IVA.
                                    </td>
                                </tr>
                            </tfoot>
                        </table>                
                        <br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2>ESPECIFICACIONES Y CONSEJOS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <ul>
                            <li>Los volantes a 1 color son impresos en papel obra de 80 grs, y los volantes full color en papel de 115 gr. En el caso de encargarlas sobre papeles especiales, el uso de los mismos se cotizará aparte. </li>
                            <li>Aceptamos archivos preferentemente en formato .AI o .PDF editable. En el caso de ser formato .JPG, debe contener demasía y poseer entre 150 y 300 dpi de resolución.</li>
                            <li>Convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas. En el caso de no incrustarlas, guardar las imágenes utilizadas en una carpeta. </li>
                            <li>El archivo debe tener una demasía mínima de 3 mm de cada lado.</li>
                        </ul>                       
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>