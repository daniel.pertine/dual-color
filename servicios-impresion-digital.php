<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li class="active"><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2>LISTA DE PRECIOS</h2>
                <h3>DIGITAL A4</h3>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>1 a 15</th>
                            <th>16 a 30</th>
                            <th>31 a 50</th>
                            <th>51 a 100</th>
                            <th>101 a 200</th>
                            <th>201 a 300</th>
                            <th>301 a 499</th>
                            <th>Más de 500</th>
                        </tr>                        
                    </thead>
                    <tbody>
                        <tr>
                            <td>Obra láser color 80 gr</td>
                            <td>$6,00</td> 
                            <td>$5,50</td>
                            <td>$5,00</td>
                            <td>$5,00</td> 
                            <td>$4,50</td>
                            <td>$3,50</td>
                            <td>$3,00</td>
                            <td>$2,50</td>
                        </tr>
                        <tr>
                            <td>Obra láser ByN 80 gr</td>
                            <td>$2,50</td> 
                            <td>$2,50</td>
                            <td>$2,50</td>
                            <td>$2,50</td> 
                            <td>$2,25</td>
                            <td>$2,00</td>
                            <td>$2,00</td>
                            <td>$2,00</td>
                        </tr>
                        <tr>
                            <td>Obra láser ByN 80 gr (texto)</td>
                            <td>$1,50</td> 
                            <td>$1,50</td>
                            <td>$1,50</td>
                            <td>$1,50</td> 
                            <td>$1,25</td>
                            <td>$1,00</td>
                            <td>$1,00</td>
                            <td>$1,00</td>
                        </tr>
                        <tr>
                            <td>Fotocopia láser blanco y negro</td>
                            <td>$1,50</td> 
                            <td>$1,50</td>
                            <td>$1,50</td>
                            <td>$1,50</td> 
                            <td>$1,25</td>
                            <td>$1,00</td>
                            <td>$1,00</td>
                            <td>$1,00</td>
                        </tr>
                        <tr>
                            <td>Ilustración mate / brillo 115 gr</td>
                            <td>$7,00</td> 
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td> 
                            <td>$4,50</td>
                            <td>$4,00</td>
                            <td>$3,50</td>
                            <td>$3,00</td>
                        </tr>
                        <tr>
                            <td>Ilustración mate / brillo 170 gr</td>
                            <td>$8,00</td> 
                            <td>$7,00</td>
                            <td>$6,50</td>
                            <td>$6,00</td> 
                            <td>$5,50</td>
                            <td>$5,00</td>
                            <td>$4,50</td>
                            <td>$4,00</td>
                        </tr>
                        <tr>
                            <td>Ilustración mate / brillo 250 gr</td>
                            <td>$9,00</td> 
                            <td>$8,00</td>
                            <td>$7,50</td>
                            <td>$7,00</td> 
                            <td>$6,50</td>
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td>
                        </tr>
                        <tr>
                            <td>Ilustración mate / brillo 300 gr</td>
                            <td>$10,00</td> 
                            <td>$9,00</td>
                            <td>$8,50</td>
                            <td>$8,00</td> 
                            <td>$7,50</td>
                            <td>$7,00</td>
                            <td>$6,50</td>
                            <td>$6,00</td>
                        </tr>
                        <tr>
                            <td>Opalina 250 gr</td>
                            <td>$9,00</td> 
                            <td>$8,00</td>
                            <td>$7,50</td>
                            <td>$7,00</td> 
                            <td>$6,50</td>
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td>
                        </tr>
                        <tr>
                            <td>Autoadhesivo mate</td>
                            <td>$10,00</td> 
                            <td>$9,00</td>
                            <td>$8,50</td>
                            <td>$8,00</td> 
                            <td>$7,50</td>
                            <td>$7,00</td>
                            <td>$6,50</td>
                            <td>$6,00</td>
                        </tr>
                        <tr>
                            <td>Autoadhesivo brillo</td>
                            <td>$10,00</td> 
                            <td>$9,00</td>
                            <td>$8,50</td>
                            <td>$8,00</td> 
                            <td>$7,50</td>
                            <td>$7,00</td>
                            <td>$6,50</td>
                            <td>$6,00</td>
                        </tr>
                        <tr>
                            <td>Autoadhesivo transparente</td>
                            <td>$20,00</td> 
                            <td>$18,00</td>
                            <td>$17,50</td>
                            <td>$16,50</td> 
                            <td>$16,00</td>
                            <td>$15,50</td>
                            <td>$15,00</td>
                            <td>$14,50</td>
                        </tr>
                        <tr>
                            <td>Bookcel 90 gr</td>
                            <td>$7,00</td> 
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td> 
                            <td>$4,50</td>
                            <td>$4,00</td>
                            <td>$3,50</td>
                            <td>$3,00</td>
                        </tr>
                        <tr>
                            <td>Bookcel 90 gr ByN (texto)</td>
                            <td>$3,00</td> 
                            <td>$3,00</td>
                            <td>$3,00</td>
                            <td>$2,50</td> 
                            <td>$2,25</td>
                            <td>$2,00</td>
                            <td>$1,75</td>
                            <td>$1,50</td>
                        </tr>
                        <tr>
                            <td>Sulfito</td>
                            <td>$8,00</td> 
                            <td>$6,50</td>
                            <td>$6,50</td>
                            <td>$6,00</td> 
                            <td>$6,00</td>
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td>
                        </tr>
                        <tr>
                            <td>Papel vegetal 90 gr</td>
                            <td>$10,00</td> 
                            <td>$8,00</td>
                            <td>$8,00</td>
                            <td>$7,50</td> 
                            <td>$7,00</td>
                            <td>$6,50</td>
                            <td>$6,00</td>
                            <td>$5,50</td>
                        </tr>
                        <tr>
                            <td>Filmina</td>
                            <td>$15,00</td> 
                            <td>$14,00</td>
                            <td>$13,00</td>
                            <td>$12,00</td> 
                            <td>$12,00</td>
                            <td>$12,00</td>
                            <td>$12,00</td>
                            <td>$12,00</td>
                        </tr>
                        <tr>
                            <td>Misionero 250 gr</td>
                            <td>$10,00</td> 
                            <td>$9,00</td>
                            <td>$8,00</td>
                            <td>$7,50</td> 
                            <td>$7,00</td>
                            <td>$6,50</td>
                            <td>$6,00</td>
                            <td>$5,50</td>
                        </tr>
                        <tr>
                            <td>Natura 75 gr</td>
                            <td>$7,00</td> 
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td> 
                            <td>$4,50</td>
                            <td>$4,00</td>
                            <td>$3,50</td>
                            <td>$3,00</td>
                        </tr>
                        <tr>
                            <td>Natura 75 gr ByN (texto)</td>
                            <td>$3,00</td> 
                            <td>$3,00</td>
                            <td>$3,00</td>
                            <td>$2,50</td> 
                            <td>$2,25</td>
                            <td>$2,00</td>
                            <td>$1,75</td>
                            <td>$1,50</td>
                        </tr>
                        <tr>
                            <td>Sobre papeles especiales</td>
                            <td>$6,00</td> 
                            <td>$5,50</td>
                            <td>$5,00</td>
                            <td>$4,50</td> 
                            <td>$4,00</td>
                            <td>$3,50</td>
                            <td>$3,00</td>
                            <td>$2,50</td>
                        </tr>
                        <tr>
                            <td>Sobre papeles esp. ByN (texto)</td>
                            <td>$3,00</td> 
                            <td>$3,00</td>
                            <td>$3,00</td>
                            <td>$2,50</td> 
                            <td>$2,25</td>
                            <td>$2,00</td>
                            <td>$2,00</td>
                            <td>$1,75</td>
                        </tr>
                        <tr>
                            <td>Si traés tu papel</td>
                            <td>$6,00</td> 
                            <td>$5,50</td>
                            <td>$5,50</td>
                            <td>$5,50</td> 
                            <td>$5,00</td>
                            <td>$5,00</td>
                            <td>$4,50</td>
                            <td>$4,00</td>
                        </tr>
                        <tr>
                            <td>Si traés tu papel ByN (texto)</td>
                            <td>$3,00</td> 
                            <td>$3,00</td>
                            <td>$3,00</td>
                            <td>$2,50</td> 
                            <td>$2,25</td>
                            <td>$2,00</td>
                            <td>$2,00</td>
                            <td>$1,75</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="9">*Los precios pueden modificarse sin previo aviso.<br/>
                                *Los precios no incluyen IVA
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <h3>DIGITAL A3</h3>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>1 a 15</th>
                            <th>16 a 30</th>
                            <th>31 a 50</th>
                            <th>51 a 100</th>
                            <th>101 a 200</th>
                            <th>201 a 300</th>
                            <th>301 a 499</th>
                            <th>Más de 500</th>
                        </tr>                        
                    </thead>
                    <tbody>
                        <tr>
                            <td>Obra láser color 80 gr</td>
                            <td>$9,00</td> 
                            <td>$8,00</td>
                            <td>$7,50</td>
                            <td>$7,00</td> 
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td>
                            <td>$4,25</td>
                        </tr>
                        <tr>
                            <td>Obra láser ByN 80 gr</td>
                            <td>$6,00</td> 
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td> 
                            <td>$5,00</td>
                            <td>$4,50</td>
                            <td>$4,00</td>
                            <td>$4,00</td>
                        </tr>
                        <tr>
                            <td>Fotocopia láser blanco y negro</td>
                            <td>$6,00</td> 
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td> 
                            <td>$5,00</td>
                            <td>$4,50</td>
                            <td>$4,00</td>
                            <td>$4,00</td>
                        </tr>
                        <tr>
                            <td>Ilustración mate / brillo 115 gr</td>
                            <td>$13,00</td> 
                            <td>$10,50</td>
                            <td>$10,00</td>
                            <td>$8,50</td> 
                            <td>$7,50</td>
                            <td>$6,50</td>
                            <td>$5,50</td>
                            <td>$4,50</td>
                        </tr>
                        <tr>
                            <td>Ilustración mate / brillo 170 gr</td>
                            <td>$14,00</td> 
                            <td>$11,50</td>
                            <td>$11,00</td>
                            <td>$9,50</td> 
                            <td>$8,50</td>
                            <td>$7,50</td>
                            <td>$6,50</td>
                            <td>$5,50</td>
                        </tr>
                        <tr>
                            <td>Ilustración mate / brillo 250 gr</td>
                            <td>$15,00</td> 
                            <td>$12,50</td>
                            <td>$12,00</td>
                            <td>$10,50</td> 
                            <td>$9,50</td>
                            <td>$8,50</td>
                            <td>$7,50</td>
                            <td>$6,50</td>
                        </tr>
                        <tr>
                            <td>Ilustración mate / brillo 300 gr</td>
                            <td>$16,00</td> 
                            <td>$13,50</td>
                            <td>$13,00</td>
                            <td>$11,50</td> 
                            <td>$10,50</td>
                            <td>$9,50</td>
                            <td>$8,50</td>
                            <td>$7,50</td>
                        </tr>
                        <tr>
                            <td>Opalina 250 gr</td>
                            <td>$15,00</td> 
                            <td>$12,50</td>
                            <td>$12,00</td>
                            <td>$10,50</td> 
                            <td>$9,50</td>
                            <td>$8,50</td>
                            <td>$7,50</td>
                            <td>$6,50</td>
                        </tr>
                        <tr>
                            <td>Autoadhesivo mate</td>
                            <td>$15,00</td> 
                            <td>$12,50</td>
                            <td>$12,00</td>
                            <td>$10,50</td> 
                            <td>$9,50</td>
                            <td>$8,50</td>
                            <td>$7,50</td>
                            <td>$6,50</td>
                        </tr>
                        <tr>
                            <td>Autoadhesivo brillo</td>
                            <td>$15,00</td> 
                            <td>$13,50</td>
                            <td>$13,00</td>
                            <td>$11,50</td> 
                            <td>$10,50</td>
                            <td>$9,50</td>
                            <td>$8,50</td>
                            <td>$7,50</td>
                        </tr>
                        <tr>
                            <td>Autoadhesivo transparente</td>
                            <td>$32,00</td> 
                            <td>$29,50</td>
                            <td>$29,00</td>
                            <td>$28,00</td> 
                            <td>$26,50</td>
                            <td>$26,00</td>
                            <td>$25,00</td>
                            <td>$23,00</td>
                        </tr>
                        <tr>
                            <td>Bookcel 90 gr</td>
                            <td>$12,00</td> 
                            <td>$9,50</td>
                            <td>$9,00</td>
                            <td>$8,00</td> 
                            <td>$6,50</td>
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,50</td>
                        </tr>
                        <tr>
                            <td>Bookcel 90 gr ByN (texto)</td>
                            <td>$7,00</td> 
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td> 
                            <td>$5,00</td>
                            <td>$4,50</td>
                            <td>$4,00</td>
                            <td>$4,00</td>
                        </tr>
                        <tr>
                            <td>Sulfito</td>
                            <td>$13,00</td> 
                            <td>$12,00</td>
                            <td>$12,00</td>
                            <td>$12,00</td> 
                            <td>$12,00</td>
                            <td>$11,50</td>
                            <td>$11,00</td>
                            <td>$10,50</td>
                        </tr>
                        <tr>
                            <td>Papel vegetal 90 gr</td>
                            <td>$16,00</td> 
                            <td>$13,00</td>
                            <td>$13,00</td>
                            <td>$13,00</td> 
                            <td>$13,00</td>
                            <td>$11,50</td>
                            <td>$11,00</td>
                            <td>$10,50</td>
                        </tr>
                        <tr>
                            <td>Filmina</td>
                            <td>$25,00</td> 
                            <td>$24,00</td>
                            <td>$24,00</td>
                            <td>$24,00</td> 
                            <td>$24,00</td>
                            <td>$24,00</td>
                            <td>$24,00</td>
                            <td>$24,00</td>
                        </tr>
                        <tr>
                            <td>Misionero 250 gr</td>
                            <td>$15,00</td> 
                            <td>$15,00</td>
                            <td>$15,00</td>
                            <td>$15,00</td> 
                            <td>$14,00</td>
                            <td>$13,50</td>
                            <td>$12,50</td>
                            <td>$11,00</td>
                        </tr>
                        <tr>
                            <td>Sobre papeles especiales</td>
                            <td>$9,00</td> 
                            <td>$8,50</td>
                            <td>$8,00</td>
                            <td>$7,00</td> 
                            <td>$5,50</td>
                            <td>$5,00</td>
                            <td>$4,50</td>
                            <td>$4,50</td>
                        </tr>
                        <tr>
                            <td>Sobre papeles esp. ByN (texto)</td>
                            <td>$6,50</td> 
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td> 
                            <td>$5,00</td>
                            <td>$4,50</td>
                            <td>$4,00</td>
                            <td>$4,00</td>
                        </tr>
                        <tr>
                            <td>Si traés tu papel</td>
                            <td>$9,00</td> 
                            <td>$9,00</td>
                            <td>$8,00</td>
                            <td>$7,50</td> 
                            <td>$7,00</td>
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td>
                        </tr>
                        <tr>
                            <td>Si traés tu papel ByN (texto)</td>
                            <td>$6,50</td> 
                            <td>$6,00</td>
                            <td>$5,50</td>
                            <td>$5,00</td> 
                            <td>$5,00</td>
                            <td>$4,50</td>
                            <td>$4,00</td>
                            <td>$4,00</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="9">*Los precios pueden modificarse sin previo aviso.<br/>
                                *Los precios no incluyen IVA
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <h3>DIGITAL SA3</h3>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>1 a 15</th>
                            <th>16 a 30</th>
                            <th>31 a 50</th>
                            <th>51 a 100</th>
                            <th>101 a 200</th>
                            <th>201 a 300</th>
                            <th>301 a 499</th>
                            <th>Más de 500</th>
                        </tr>                        
                    </thead>
                    <tbody>
                        <tr>
                            <td>Ilustración mate 115 gr</td>
                            <td>$14,00</td> 
                            <td>$11,50</td>
                            <td>$11,00</td>
                            <td>$10,00</td> 
                            <td>$8,50</td>
                            <td>$7,50</td>
                            <td>$6,50</td>
                            <td>$5,50</td>
                        </tr>
                        <tr>
                            <td>Ilustración brillo 170 gr</td>
                            <td>$15,00</td> 
                            <td>$12,50</td>
                            <td>$12,00</td>
                            <td>$11,00</td> 
                            <td>$9,50</td>
                            <td>$8,50</td>
                            <td>$7,50</td>
                            <td>$6,50</td>
                        </tr>
                        <tr>
                            <td>Ilustración mate 300 gr</td>
                            <td>$15,00</td> 
                            <td>$14,50</td>
                            <td>$14,00</td>
                            <td>$13,00</td> 
                            <td>$11,50</td>
                            <td>$10,50</td>
                            <td>$9,50</td>
                            <td>$8,50</td>
                        </tr>
                        <tr>
                            <td>Opalina 250 gr</td>
                            <td>$17,00</td> 
                            <td>$13,50</td>
                            <td>$13,00</td>
                            <td>$12,00</td> 
                            <td>$10,50</td>
                            <td>$9,50</td>
                            <td>$8,50</td>
                            <td>$7,50</td>
                        </tr>
                        <tr>
                            <td>Autoadhesivo mate</td>
                            <td>$17,00</td> 
                            <td>$13,50</td>
                            <td>$13,00</td>
                            <td>$12,00</td> 
                            <td>$10,50</td>
                            <td>$9,50</td>
                            <td>$8,50</td>
                            <td>$7,50</td>
                        </tr>
                        <tr>
                            <td>Autoadhesivo brillo</td>
                            <td>$17,00</td> 
                            <td>$14,50</td>
                            <td>$14,00</td>
                            <td>$13,00</td> 
                            <td>$11,50</td>
                            <td>$10,50</td>
                            <td>$9,50</td>
                            <td>$8,50</td>
                        </tr>
                        <tr>
                            <td>Autoadhesivo transparente</td>
                            <td>$35,00</td> 
                            <td>$31,50</td>
                            <td>$31,00</td>
                            <td>$30,00</td> 
                            <td>$28,50</td>
                            <td>$28,00</td>
                            <td>$27,00</td>
                            <td>$23,00</td>
                        </tr>
                        <tr>
                            <td>Bookcel 90 gr</td>
                            <td>$13,00</td> 
                            <td>$10,50</td>
                            <td>$10,00</td>
                            <td>$9,00</td> 
                            <td>$7,50</td>
                            <td>$7,00</td>
                            <td>$6,50</td>
                            <td>$6,00</td>
                        </tr>
                        <tr>
                            <td>Sulfito</td>
                            <td>$14,00</td> 
                            <td>$13,00</td>
                            <td>$13,00</td>
                            <td>$13,00</td> 
                            <td>$13,00</td>
                            <td>$12,50</td>
                            <td>$12,00</td>
                            <td>$11,50</td>
                        </tr>
                        <tr>
                            <td>Sobre papeles especiales</td>
                            <td>$12,00</td> 
                            <td>$10,50</td>
                            <td>$10,00</td>
                            <td>$9,00</td> 
                            <td>$7,50</td>
                            <td>$7,00</td>
                            <td>$6,50</td>
                            <td>$6,00</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="9">*Los precios pueden modificarse sin previo aviso.<br/>
                                *Los precios no incluyen IVA
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2>PRECIOS POR CANTIDAD</h2>
                        <p style="text-align:center;"><a href="precios.por.cantidad.2017.05.pdf" target="_blank"><button type="button" class="btn btn-default">Descargar PDF</button></a></p>
                        <h2 style="margin-top:45px;">MEDIDAS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <div class="row">
                            <div class="col-md-3"><img src="images/medidaA4.png" alt="A4" class="img-responsive"></div>
                            <div class="col-md-3"><img src="images/medidaA3.png" alt="A3" class="img-responsive"></div>
                            <div class="col-md-3"><img src="images/medidaSA3.png" alt="SA3" class="img-responsive"></div>
                            <div class="col-md-3"><img src="images/medidaSA3opalina.png" alt="SA3 opalina" class="img-responsive"></div>
                        </div>
                        <h2 style="margin-top:45px;">ESPECIFICACIONES Y CONSEJOS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <h3>BAJADAS DIGITALES</h3>
                        <ul>
                            <li>Recomendamos guardar los archivos en formato .PDF ó .JPG en modo de color CMYK (no RGB ni Pantone).</li>
                            <li>No aceptamos archivos de paquete office (Microsoft Word, Excel, Powerpoint, etc.). En el caso de utilizar estos programas, guardar los archivos en formato .PDF.</li>
                            <li>Para archivos de Autocad, preferentemente guardarlos en formato .PDF. En el caso de imprimir desde Autocad, tener presente la escala deseada y aclarar si incluye puntas.</li>
                            <li>Al preparar los archivos, tener en cuenta que para todos los soportes existe un área no imprimible de 5 mm de cada lado. Asimismo, recomendamos dejar demasía de 5 mm para los cortes, y en el caso de impresiones doble faz, contemplar que puede haber un desfasaje máximo de 3 mm.</li>
                            <li>Sólo aceptamos papeles especiales desde 90 grs hasta 300 grs, que no contengan encapados plásticos, ni residuos sueltos sobre la superficie ó en los bordes de la hoja, y estén cortados en los tamaños estandarizados.</li>
                            <li>En el caso de los archivos que contengan imágenes de gran tamaño, al exportar en formato .PDF, acoplar las capas.</li>
                            <li>Recomendamos trabajar con una resolución mínima de 150 dpi y máxima de 300 dpi.</li>
                            <li>Convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas. En el caso de no incrustarlas, guardar las imágenes utilizadas en una carpeta.</li>
                        </ul>
                        <h3>ENVÍO DE ARCHIVOS Y PEDIDOS ONLINE</h3>
                        <ul>
                            <li>En el nombre de cada archivo, especificar tamaño de hoja, tipo de papel y gramaje, y cantidad de copias. En el caso de ser doble faz y presentar las caras separadas en distintos archivos, nombrarlos de la misma forma, pero indicando “frente” y “dorso” como corresponda.</li>
                        </ul>                        
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>