<?php
    include 'inc/header.php';
?>

    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="proximamente.php">Volantes</a></li>
                <li><a href="proximamente.php">Postales y Señaladores</a></li>
                <li><a href="proximamente.php">Stickers</a></li>
                <li><a href="proximamente.php">Imanes y Pines</a></li>
                <li><a href="proximamente.php">Recetarios</a></li>
                <li><a href="proximamente.php">Carpetas</a></li>
                <li><a href="proximamente.php">Laminados</a></li>
                <li><a href="proximamente.php">Encuadernación</a></li>
                <li><a href="proximamente.php">Revistas</a></li>
                <li><a href="proximamente.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2 style="text-align:center; margin-top:50px">PROXIMAMENTE</h2>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>