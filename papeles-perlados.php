<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="papeles-texturados.php">Texturados</a></li>
                <li class="active"><a href="papeles-perlados.php">Perlados</a></li>
                <li><a href="papeles-calcos.php">Calcos</a></li>
                <li><a href="papeles-estandar.php">Estándar</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <div class="row">
                    <div class='list-group gallery'>
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/perlados/papeles_slider_perl_1-2.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/perlados/papeles_slider_perl_1-2.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Perlado Nº 01 / 02</strong><br>&nbsp;&nbsp;&nbsp;120 / 250 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/perlados/papeles_slider_perl_3-4.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/perlados/papeles_slider_perl_3-4.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Perlado Nº 03 / 04</strong><br>&nbsp;&nbsp;&nbsp;125 / 250 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/perlados/papeles_slider_perl_5-6.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/perlados/papeles_slider_perl_5-6.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Perlado Nº 05 / 06</strong><br>&nbsp;&nbsp;&nbsp;125 / 240 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/perlados/papeles_slider_perl_7.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/perlados/papeles_slider_perl_7.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Perlado Nº 07</strong><br>&nbsp;&nbsp;&nbsp;120 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/perlados/papeles_slider_perl_8.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/perlados/papeles_slider_perl_8.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Perlado Nº 08</strong><br>&nbsp;&nbsp;&nbsp;120 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/perlados/papeles_slider_perl_9.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/perlados/papeles_slider_perl_9.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Perlado Nº 09</strong><br>&nbsp;&nbsp;&nbsp;120 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/perlados/papeles_slider_perl_10.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/perlados/papeles_slider_perl_10.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Perlado Nº 10</strong><br>&nbsp;&nbsp;&nbsp;120 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/perlados/papeles_slider_perl_11.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/perlados/papeles_slider_perl_11.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Perlado Nº 11</strong><br>&nbsp;&nbsp;&nbsp;120 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/perlados/papeles_slider_perl_12.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/perlados/papeles_slider_perl_12.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Perlado Nº 12</strong><br>&nbsp;&nbsp;&nbsp;120 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/perlados/papeles_slider_perl_13.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/perlados/papeles_slider_perl_13.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Perlado Nº 13</strong><br>&nbsp;&nbsp;&nbsp;120 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/perlados/papeles_slider_perl_14.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/perlados/papeles_slider_perl_14.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Perlado Nº 14</strong><br>&nbsp;&nbsp;&nbsp;120 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="images/papeles/perlados/papeles_slider_perl_15.jpg">
                                <img class="img-responsive" alt="" src="images/papeles/perlados/papeles_slider_perl_15.jpg" />
                                <div class='text-left'>
                                    <small class='text-muted'><strong>Perlado Nº 15</strong><br>&nbsp;&nbsp;&nbsp;350 gr</small>
                                </div> <!-- text-left / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                    </div> <!-- list-group / end -->
                </div> <!-- row / end -->
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>