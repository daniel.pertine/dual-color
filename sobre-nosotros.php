<?php
    include 'inc/header.php';
?>

    <div class="container noMargin">
        <div class="row">
            <div class="col-md-12">
                <!--<img src="images/promociones.png" alt="Promociones" class="img-responsive">-->

                <div id="carousel-id" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-id" data-slide-to="0" class=""></li>
                        <li data-target="#carousel-id" data-slide-to="1" class=""></li>
                        <li data-target="#carousel-id" data-slide-to="2" class="active"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item">
                            <img src="images/nosotros1.png" alt="Sobre nosotros" class="img-responsive">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>¡Imprimí tus ideas!</h1>
                                    <hr>
                                    <p>Contamos con equipamiento técnico de la mejor calidad para ofrecer todo tipo de servicios y productos: Impresión Digital y Offset, Folletería, Stickers, Pines, Imanes, Gigantografías, Revistas y mucho más!</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <img src="images/nosotros2.png" alt="Sobre nosotros" class="img-responsive">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Nuestro Equipo</h1>
                                    <hr>
                                    <p>Somos una gráfica inaugurada en 2013 y conformada por un equipo de diseñadores y estudiantes de diseño y carreras afines.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item active">
                            <img src="images/nosotros3.png" alt="Sobre nosotros" class="img-responsive">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Soluciones Gráficas</h1>
                                    <hr>
                                    <p>Conformado por un equipo capacitado para resolver dudas en tus trabajos de impresión, y ofrecer soluciones gráficas para tu perfil ó empresa.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon icon-prev"></span></a>
                    <a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon icon-next"></span></a>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>