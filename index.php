<?php
    include 'inc/header.php';
?>

    <div class="container">
        <div class="row" style="margin:0">
            <div id="bloque1" class="col-sm-8 col-md-8" style="padding:0;">
                <div class="txtImg">
                    <h2>¡Imprimí sin esperar!</h2>
                    <p>Enviá tus archivos a <a href="mailto:pedidosdualcolor@gmail.com">pedidosdualcolor@gmail.com</a> especificando tamaño y tipo de papel y retirá tu pedido directo por mostrador.</p>
                </div>
            </div>
            <div id="bloque2" class="col-sm-4 col-md-4" style="padding:0;">
                <div class="txtImg" style="color:#2d2645;">
                    <h2>PAPELES ESPECIALES</h2>
                    <hr>
                    <p><a href="#">Accedé a nuestro muestrario <span>»</span></a></p>
                </div>
            </div>
        </div>    
        <div class="row" style="margin:0">
            <div id="bloque3" class="col-sm-8 col-md-8" style="padding:0;">
                <a href="#" title="Nuestros Servicios"><img style="max-width:100%;" src="images/nuestros_servicios.gif"></a>
            </div>
            <div id="bloque4" class="col-sm-4 col-md-4" style="padding:0;">
                <div class="txtImg" style="color:#2d2645;">
                    <h2>DISEÑOS<br>PERSONALIZADOS</h2>
                    <p>Casamientos | 15 años | Bautismos<br>Comuniones | Cumpleaños</p>
                    <p><a href="#">Solicitá tu presupuesto <span>»</span></a></p>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>