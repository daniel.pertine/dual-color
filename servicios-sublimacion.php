<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li class="active"><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>

            <div class="col-md-10" style="border-left: 1px solid #d55915;">

                <div class="row">
                    <div class="col-md-12">
                        <img src="images/sublimacion.png" alt="Sublimación" class="img-responsive">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2 style="margin-top:45px;">ESPECIFICACIONES Y CONSEJOS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <h3>SUBLIMACIÓN POR M2</h3>
                        <ul>
                            <li>
                                Recomendamos guardar los archivos en formato .PDF ó .JPG en modo de color CMYK o RGB - SIN ESPEJAR - (el archivo pasa por un rip automáticamente para buscar el color mas parecido al original y lo espeja).
                            </li>
                            <li>
                                Materiales aptos para sublimar telas o materiales con mas del 70% de poliéster.
                            </li>
                            <li>
                                Los trabajos de sublimación se entregan en el día por lo general, recomendamos consultar previamente.
                            </li>
                            <li>
                                Para algodón ofrecemos transfer para telas claras o oscuras en A3 o A4 (el algodón no se puede sublimar).
                            </li>
                            <li>
                                FRISELINA por el momento no sublimamos. Hay varios tipos de friselina o entretela, la mayoría no sublima porque no soportan la temperatura.
                            </li>
                            <li>
                                Sublimamos en CMYK (no tiene tinta blanca).
                            </li>
                            <li>
                                No aceptamos archivos de paquete office (Microsoft Word, Excel, Powerpoint, etc.). En el caso de utilizar estos programas, guardar los archivos en formato .PDF.
                            </li>
                            <li>
                                Al preparar los archivos, tener en cuenta que para todos los soportes existe un área no imprimible de 5 mm de cada lado. Ancho del plotter 100cm.
                            </li>
                            <li>
                                En el caso de los archivos que contengan imágenes de gran tamaño, al exportar en formato .PDF, acoplar las capas.
                            </li>
                            <li>
                                Recomendamos trabajar con una resolución mínima de 150 dpi y máxima de 300 dpi.
                            </li>
                            <li>
                                Convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas. En el caso de no incrustarlas, guardar las imágenes utilizadas en una carpeta.
                            </li>
                        </ul>
                        <br>
                        <img src="images/arrow.png" class="center-block">
                        <h3>ENVÍO DE ARCHIVOS Y PEDIDOS ONLINE</h3>
                        <ul>
                            <li>
                                En el nombre de cada archivo, especificar tamaño de ploteo y cantidad de copias.
                            </li>
                            <li>
                                Mails para sublimación a <a href="mailto:sublimaciondc@gmail.com">sublimaciondc@gmail.com</a>.
                            </li>
                        </ul>
                        <br>
                        <table>
                            <tfoot>
                                <tr>
                                    <td colspan="9">
                                        *Los precios pueden modificarse sin previo aviso.<br>
                                        *Los precios no incluyen IVA.
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>