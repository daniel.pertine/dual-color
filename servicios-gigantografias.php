<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li class="active"><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2>LISTA DE PRECIOS</h2>
                <h3>GRAN FORMATO</h3>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Unidad de Medida</th>
                            <th>Precio m<sup>2</sup></th>
                        </tr>                        
                    </thead>
                    <tbody>
                        <tr>
                            <td>Sublimación</td>
                            <td>Impresión sin bajada de plancha</td> 
                            <td>m<sup>2</sup></td>
                            <td>$95</td>
                        </tr>
                        <tr>
                            <td>Vinilo de Corte</td>
                            <td>Con posicionador de papel</td> 
                            <td>55x100cm</td>
                            <td>$325</td>
                        </tr>
                        <tr>
                            <td>Vinilo Termotransferible</td>
                            <td>Vinilo Textil Termotransferible</td> 
                            <td>45x100cm</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>Vinilo Impreso y Troquelado</td>
                            <td>Vinilo impreso y troquelado</td> 
                            <td>m<sup>2</sup></td>
                            <td>$595</td>
                        </tr>
                        <tr>
                            <td>Vinilo para Interiores (Blanco)</td>
                            <td>Vinilo impreso sin terminación</td> 
                            <td>m<sup>2</sup></td>
                            <td>$375</td>
                        </tr>
                        <tr>
                            <td>Vinilo para Exteriores (Blanco)</td>
                            <td>Vinilo impreso sin terminación</td> 
                            <td>m<sup>2</sup></td>
                            <td>$485</td>
                        </tr>
                        <tr>
                            <td>Vinilo Mate</td>
                            <td>Vinilo impreso sin terminación</td> 
                            <td>m<sup>2</sup></td>
                            <td>$485</td>
                        </tr>
                        <tr>
                            <td>Vinilo Transparente</td>
                            <td>Vinilo impreso sin terminación</td> 
                            <td>m<sup>2</sup></td>
                            <td>$485</td>
                        </tr>
                        <tr>
                            <td>Vinilo Transparente Mate</td>
                            <td>Vinilo impreso sin terminación</td> 
                            <td>m<sup>2</sup></td>
                            <td>$485</td>
                        </tr>
                        <tr>
                            <td>Vinilo Esmerilado</td>
                            <td>Vinilo impreso sin terminación</td> 
                            <td>m<sup>2</sup></td>
                            <td>$485</td>
                        </tr>
                        <tr>
                            <td>Microperforado</td>
                            <td>Vinilo impreso sin terminación</td> 
                            <td>m<sup>2</sup></td>
                            <td>$570</td>
                        </tr>
                        <tr>
                            <td>Lona Front</td>
                            <td>Lona impresa sin terminación</td> 
                            <td>m<sup>2</sup></td>
                            <td>$385</td>
                        </tr>
                        <tr>
                            <td>Lona Mesh</td>
                            <td>Lona impresa sin terminación</td> 
                            <td>m<sup>2</sup></td>
                            <td>$428</td>
                        </tr>
                        <tr>
                            <td>Lona Mate</td>
                            <td>Lona impresa sin terminación</td> 
                            <td>m<sup>2</sup></td>
                            <td>$385</td>
                        </tr>
                        <tr>
                            <td>Lona Black Out</td>
                            <td>Lona impresa sin terminación</td> 
                            <td>m<sup>2</sup></td>
                            <td>$460</td>
                        </tr>
                        <tr>
                            <td>Lona Backlight</td>
                            <td>Lona impresa sin terminación</td> 
                            <td>m<sup>2</sup></td>
                            <td>$520</td>
                        </tr>
                        <tr>
                            <td>Papel BlueBack</td>
                            <td>Papel BlueBack impreso</td> 
                            <td>m<sup>2</sup></td>
                            <td>$485</td>
                        </tr>
                        <tr>
                            <td>Tela Bandera</td>
                            <td>Tela Bandera impresa</td> 
                            <td>m<sup>2</sup></td>
                            <td>$485</td>
                        </tr>
                        <tr>
                            <td>PVC</td>
                            <td>PVC impreso o vinilo sobre pvc</td> 
                            <td>Consultar</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>Foam</td>
                            <td>Vinilo sobre Foamboard</td> 
                            <td>Consultar</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>Alto Impacto</td>
                            <td>Impreso o vinilo sobre alto impacto</td> 
                            <td>Consultar</td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td>Rollup</td>
                            <td>Lona con Portabanner (85 x 200cm)</td> 
                            <td>c/u</td>
                            <td>$1718</td>
                        </tr>

                        <tr style="background: #413966;">
                            <td colspan="4">Terminaciones para Lonas</td>
                        </tr>

                        <tr>
                            <td>Caños</td>
                            <td>Lona con bolsillo caño y tanza</td> 
                            <td>metro lineal</td>
                            <td>$56</td>
                        </tr>
                        <tr>
                            <td>Argolla</td>
                            <td>Lona rebatida con argollas</td> 
                            <td>cada argolla</td>
                            <td>$21</td>
                        </tr>
                        <tr>
                            <td>Rebatido</td>
                            <td>Lona, Rebatido</td> 
                            <td>metro lineal</td>
                            <td>$46</td>
                        </tr>
                    </tbody>
                </table>                
                <br>
                <h3>BANNERS</h3>
                <hr>
                <div class="banners row">
                    <div class="col-sm-4 col-md-4"><img src="images/banner1.png" alt="Banner Simple" class="img-responsive"></div>
                    <div class="col-sm-4 col-md-4"><img src="images/banner2.png" alt="Banner Doble" class="img-responsive middle"></div>
                    <div class="col-sm-4 col-md-4"><img src="images/banner3.png" alt="Banner Araña" class="img-responsive"></div>
                </div>
                <table class="table">
                    <tfoot>
                        <tr>
                            <td style="border-top:0;">
                                *Todos los baners incluyen estructura desmontable y bolsa para transportar.<br>
                                *Los precios pueden modificarse sin previo aviso. / *Los precios no incluyen IVA.
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2>ESPECIFICACIONES Y CONSEJOS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <h3>PLOTEOS EN LONA</h3>
                        <ul>
                            <li>Recomendamos guardar los archivos en formato .PDF ó .JPG en modo de color CMYK (no RGB ni Pantone).</li>
                            <li>Recomendamos trabajar con una resolución mínima de 150 dpi y máxima de 300 dpi.</li>
                            <li>Convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas. En el caso de no incrustarlas, guardar las imágenes utilizadas en una carpeta.</li>
                        </ul>
                        <h3>PLOTEOS EN VINILO DE CORTE</h3>
                        <ul>
                            <li>El ancho del plotter de corte es de 55 cm. Alto mínimo de 30 cm para colores disponibles, y 100 cm para colores a pedido.</li>
                            <li>Convertir las tipografías a curvas / contornos y unificar trazados superpuestos.</li>
                            <li>Preferentemente trabajamos archivos .ai (illustrator). En el caso de utilizar corel, convertir con compatibilidad para illustrator.</li>
                            <li>Mínimo de corte 2 mm de espesor para trazos, y 1 cm de alto para tipografías.</li>
                        </ul>                        
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>