<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li class="active"><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2>LISTA DE PRECIOS</h2>
                <h3>ANILLADO ESPIRAL PLÁSTICO</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>A4 (horizontal y vertical)</th>
                                    <th>A3 (horizontal)</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x50 hojas</td>
                                    <td>$30</td>
                                    <td>$35</td>
                                </tr>
                                <tr>
                                    <td>x100 hojas</td>
                                    <td>$35</td>
                                    <td>$40</td>
                                </tr>
                                <tr>
                                    <td>x150 hojas</td>
                                    <td>$40</td>
                                    <td>$45</td>
                                </tr>
                                <tr>
                                    <td>x200 hojas</td>
                                    <td>$55</td>
                                    <td>$60</td>
                                </tr>
                                <tr>
                                    <td>x300 hojas</td>
                                    <td>$75</td>
                                    <td>$95</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <h3>ANILLADO METÁLICO (doble alambre) / ANILLADO ESPIRAL METALICO</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>A4 (horizontal y vertical)</th>
                                    <th>A3 (horizontal)</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Tamaño chico</td>
                                    <td>$35</td>
                                    <td>$45</td>
                                </tr>
                                <tr>
                                    <td>Tamaño mediano</td>
                                    <td>$45</td>
                                    <td>$55</td>
                                </tr>
                                <tr>
                                    <td>Tamaño grande</td>
                                    <td>$55</td>
                                    <td>$65</td>
                                </tr>
                                <tr>
                                    <td>Tamaño extragrande</td>
                                    <td>$65</td>
                                    <td>$75</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <h3>ENCUADERNACIÓN BINDER</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>A4 (horizontal y vertical)</th>
                                    <th>A3 (horizontal)</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x1</td>
                                    <td>$50</td>
                                    <td>$75</td>
                                </tr>
                                <tr>
                                    <td>x2</td>
                                    <td>$40</td>
                                    <td>$65</td>
                                </tr>
                                <tr>
                                    <td>x3</td>
                                    <td>$30</td>
                                    <td>$55</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <h3>ENCUADERNACIÓN BINDER + REFILADO (de los 3 lados)</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>A4 (horizontal y vertical)</th>
                                    <th>A3 (horizontal)</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x1</td>
                                    <td>$120</td>
                                    <td>$140</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3">* hasta 150 hojas de 90gr</td>
                                </tr>
                            </tfoot>
                        </table>
                        <br>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <h3>PLASTIFICADO</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Credencial</th>
                                    <th>A4</th>
                                    <th>A3</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x1</td>
                                    <td>$20</td>
                                    <td>$25</td>
                                    <td>$45</td>
                                </tr>
                                <tr>
                                    <td>x10</td>
                                    <td>$18</td>
                                    <td>$20</td>
                                    <td>$40</td>
                                </tr>
                                <tr>
                                    <td>x50</td>
                                    <td>$15</td>
                                    <td>$18</td>
                                    <td>$38</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">*Los precios pueden modificarse sin previo aviso.<br/>
                                        *Los precios no incluyen IVA<br/>
                                        Los precios estan detallados por unidad.<br/>
                                        Bajada de plancha A3 $10 (impreso en el local)<br/>
                                        Bajada de plancha A3 $25 (si lo provee el cliente)
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <br>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2>ESPECIFICACIONES Y CONSEJOS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <ul>
                            <li>Anillamos cartón con gramaje máximo de 1 mm (solamente anillado metálico).</li>
                            <li>La distancia máxima entre el borde de la hoja y el anillado es de 5 mm.</li>
                            <li>El refilado para encuadernaciones binder posee un costo adicional (consultar).</li>
                        </ul>                       
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>