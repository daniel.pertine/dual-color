<?php
	if(isset($_POST['g-recaptcha-response']))
		$captcha=$_POST['g-recaptcha-response'];

        if(!$captcha){
		echo '<h2>Por favor revisa el captcha.</h2>';
		exit;
        }
	
        $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcR1Q8UAAAAAKrB7rcqjOTg3SPW62Gx1prqKjK0&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
        if($response['success'] == false)
        {
		echo '<h2>Sos un spammer! Vete de aquí</h2>';
		exit();
        }
	
	$body = "Contacto del formulario de Dual Color<br /><br />
	Nombre: ".$_POST["nombre"]."<br /><br />
	Email: ".$_POST["email"]."<br /><br />
	Asunto: ".$_POST["asunto"]."<br /><br />
	Mensaje: ".$_POST["mensaje"]."<br /><br />";
	
	if(! mail ('graficadualcolor@gmail.com', 'Mensaje desde la web graficadualcolor.com.ar', $body, 'From: info@graficadualcolor.com.ar' ))
		$respuesta = 'Error al enviar email de contacto, intente en unos minutos por favor.';
	
	if(!isset($respuesta))
	{
		$body = "Contacto del formulario de graficadualcolor.com.ar<br /><br />
		Gracias por contactarnos<br /><br />
		Su mensaje ha sido enviado, en menos de 24 hs le contestaremos.<br /><br />
		<b>Dual Color (+54.011) 4774-9186</b>";
		
		if(! mail ($_POST["email"], 'Mensaje desde la web graficadualcolor.com.ar', $body, 'From: info@graficadualcolor.com.ar' ))
			$respuesta = 'Error al enviar email de contacto, intente en unos minutos por favor.';
	}
	
	if(!isset($respuesta))
		$respuesta = 'Gracias por contactarnos. Su mensaje ha sido enviado, en menos de 24 hs le contestaremos.';

	echo json_encode($respuesta);