<?php
	include ('../vendor/autoload.php');
	include ('inc/bd.php');
    
    //Get the data from the serverRequest
    $request = Zend\Diactoros\ServerRequestFactory::fromGlobals($_POST);
	$data 	= $request->getParsedBody();

	//Actualizar pedido 
	$db->pedido
    ->update()
    ->data(['estado_id' => $data['estado']])
    ->where('id = :id', [':id' => $data['pedidoId']])
    ->run();
    
    return true;
?>