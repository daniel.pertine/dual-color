<?php
	include ('../vendor/autoload.php');
	include ('inc/bd.php');
    
    //Get the data from the serverRequest
    $request = Zend\Diactoros\ServerRequestFactory::fromGlobals($_POST);
	$data 	= $request->getParsedBody();

	//Agregar cliente 
	$cliente = $db->cliente
    ->insert()
    ->data(['nombre' => $data['nombre'],
            'telefono' => $data['telefono'],
            'celular' => $data['celular'],
            'mail' => $data['mail'],
            'direccion' => $data['direccion']
            ])->run();
    
    header('Location: '.($data['pedido'] ? 'crear_pedido.php?cliente='.$cliente : 'clientes.php'));
?>