<?php 	
	session_start();
	if(!isset($_SESSION['id']))
		header('Location: /admin');

	include ('../../vendor/autoload.php');
	include ('bd.php');

	//Get the data from the serverRequest
    $request 	= Zend\Diactoros\ServerRequestFactory::fromGlobals($_GET);
    $data		= $request->getQueryParams();

	$caras	= $db->caraTamano
				->select()
				->where('tamanoTipo_id = :tamanoTipo_id', [':tamanoTipo_id' => $data['tamanoTipo_id']])
				->run();
	
	echo json_encode($caras);
?>