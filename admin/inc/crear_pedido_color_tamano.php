<?php 	
	session_start();
	if(!isset($_SESSION['id']))
		header('Location: /admin');

	include ('../../vendor/autoload.php');
	include ('bd.php');

	//Get the data from the serverRequest
    $request 	= Zend\Diactoros\ServerRequestFactory::fromGlobals($_GET);
    $data		= $request->getQueryParams();

	$tamanos	= $db->tamanoColor
					->select()
					->where('colorCategoria_id = :colorCategoria_id', [':colorCategoria_id' => $data['colorCategoria_id']])
					->run();
	
	echo json_encode($tamanos);
?>