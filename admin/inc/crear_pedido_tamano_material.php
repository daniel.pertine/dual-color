<?php 	
	session_start();
	if(!isset($_SESSION['id']))
		header('Location: /admin');

	include ('../../vendor/autoload.php');
	include ('bd.php');

	//Get the data from the serverRequest
    $request 	= Zend\Diactoros\ServerRequestFactory::fromGlobals($_GET);
    $data		= $request->getQueryParams();

	$materiales		= $db->materialTamano
					->select()
					->where('tamanoCategoria_id = :tamano_id', [':tamano_id' => $data['tamano_id']])
					->run();
	
	echo json_encode($materiales);
?>