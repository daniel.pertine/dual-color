<?php 	
	session_start();
	if(!isset($_SESSION['id']))
		header('Location: /admin');

	include ('../../vendor/autoload.php');
	include ('bd.php');

	//Get the data from the serverRequest
    $request 	= Zend\Diactoros\ServerRequestFactory::fromGlobals($_GET);
    $data		= $request->getQueryParams();

	$cantidades		= $db->cantidadMaterial
					->select()
					->where('materialCategoria_id = :materialCategoria_id', [':materialCategoria_id' => $data['materialCategoria_id']])
					->run();

	echo json_encode($cantidades);
?>