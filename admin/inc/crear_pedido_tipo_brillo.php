<?php 	
	session_start();
	if(!isset($_SESSION['id']))
		header('Location: /admin');

	include ('../../vendor/autoload.php');
	include ('bd.php');

	//Get the data from the serverRequest
    $request 	= Zend\Diactoros\ServerRequestFactory::fromGlobals($_GET);
    $data		= $request->getQueryParams();

	$brillos	= $db->BrilloTipo
				->select()
				->where('tipoCategoria_id = :tipoCategoria_id', [':tipoCategoria_id' => $data['tipoCategoria_id']])
				->run();
	
	echo json_encode($brillos);
?>