<?php
	include ('../../vendor/autoload.php');
	include ('bd.php');
	
	$usuario = $db->usuario
				->select()
				->one()
				->where('usuario = :usuario', [':usuario' => $_POST['user']])
				->run();
	
	if($usuario && password_verify($_POST['password'], $usuario->clave))
	{
		session_start();
		$_SESSION['id'] = $usuario->id;
		$_SESSION['usuario'] = $usuario->usuario;
		header('Location: /admin/pedidos.php');
		exit();
	}

	echo "Clave o usuario incorrectos";
?>