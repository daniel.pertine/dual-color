<?php
use SimpleCrud\SimpleCrud;
date_default_timezone_set ('America/Argentina/Buenos_Aires');

$dsn 		= 'mysql:dbname=dual_color;host=172.20.0.3';
$usuario 	= 'root';
$contraseña = 'hunter2';

$opciones = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
); 

$pdo = new PDO($dsn, $usuario, $contraseña, $opciones);

$db = new SimpleCrud($pdo);

//Set the uploads path
$db->setAttribute(SimpleCrud::ATTR_UPLOADS, __DIR__.'/../uploads');
?>