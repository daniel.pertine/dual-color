<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="latin1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Dual Color</title>

		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    	<link rel="icon" href="favicon.ico" type="image/x-icon">

		<!-- Bootstrap -->
    	<link href="css/bootstrap.min.css" rel="stylesheet">
    	
    	<!-- Custom CSS -->
    	<link href="css/fancybox/jquery.fancybox.css" rel="stylesheet" media="screen">
    	<link href="css/styles.css" rel="stylesheet">
    	<link href="css/jquery-ui-1.12.1.min.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
