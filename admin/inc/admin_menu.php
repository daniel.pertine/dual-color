		<div class="container-fluid" id="adminMenu">
			<div class="row">
				<div class="container">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<nav class="navbar navbar-default" role="navigation">
							<div class="container-fluid">
								<!-- Brand and toggle get grouped for better mobile display -->
								<div class="navbar-header">
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
									<a class="navbar-brand" href="#"></a>
								</div>					
								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse navbar-ex1-collapse">						
									<ul class="nav nav-justified">
										<!--<li><a href="#" <?php echo  isset($seccion) && $seccion == 'mostrador' ? 'class="active"' : '';?>>MOSTRADOR<br><br><span class="icon-mostrador"></span></a></li>-->
										<li><a href="pedidos.php" <?php echo isset($seccion) && $seccion == 'pedidos' ? 'class="active"' : '';?>>PEDIDOS<br><br><span class="icon-pedidos"></span></a></li>
										<li><a href="clientes.php" <?php echo isset($seccion) && $seccion == 'clientes' ? 'class="active"' : '';?>>CLIENTES<br><br><span class="icon-clientes"></span></a></li>
										<li><a href="usuarios.php" <?php echo isset($seccion) && $seccion == 'usuarios' ? 'class="active"' : '';?>>USUARIOS<br><br><span class="icon-usuarios"></span></a></li>
										<!--
										<li><a href="#" <?php echo isset($seccion) && $seccion == 'proveedores' ? 'class="active"' : '';?>>PROVEEDORES<br><br><span class="icon-proveedores"></span></a></li>
										<li><a href="#" <?php echo isset($seccion) && $seccion == 'gastos' ? 'class="active"' : '';?>>GASTOS DIARIOS<br><br><span class="icon-gastos"></span></a></li>
										<li><a href="#" <?php echo isset($seccion) && $seccion == 'control' ? 'class="active"' : '';?>>CONTROL<br><br><span class="icon-control"></span></a></li>-->
										<li><a class="navbar-brand" href="/admin"><br><span class="icon-logo"></span></a></li>
									</ul>
								</div><!-- /.navbar-collapse -->
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>