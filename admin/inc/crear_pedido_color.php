<?php 	
	session_start();
	if(!isset($_SESSION['id']))
		header('Location: /admin');

	include ('../../vendor/autoload.php');
	include ('bd.php');

	//Get the data from the serverRequest
    $request 	= Zend\Diactoros\ServerRequestFactory::fromGlobals($_GET);
    $data		= $request->getQueryParams();

	$colores	= $db->colorCategoria
				->select()
				->where('categoria_id = :categoria_id', [':categoria_id' => $data['categoria_id']])
				->run();
	
	echo json_encode($colores);
?>