<?php
	session_start();
	if(!isset($_SESSION['id']))
		header('Location: /admin');
	
	include ('../vendor/autoload.php');
	include ('inc/bd.php');
	
	$clientes	= $db->cliente
				->select()
				->run();

	include('inc/header.php');
	$seccion = 'clientes';
?>
	<body>

		<?php include('inc/user_menu.php');?>
		<?php include('inc/admin_menu.php');?>


		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">									
					<div class="panel panel-default">
						<!-- Default panel contents -->
						<div class="panel-body">
							<ul class="nav nav-pills nav-justified" id="referencias">
								<li role="presentation"><a href="crear_cliente.php"><button type="submit" class="btn adminBtn">AGREGAR CLIENTE</button></a></li>
							</ul>
						</div>

						<hr class="separador">

						<!-- Table -->
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>NOMBRE</th>
									<th>TELEFONO</th>
									<th>CELULAR</th>
									<th>MAIL</th>
									<th>DIRECCION</th>
								</tr>
							</thead>
							<tbody>
<?php 						foreach($clientes as $cliente)
							{
?>
								<tr>
									<td><?php echo $cliente->nombre;?></td>
									<td><?php echo $cliente->telefono;?></td>
									<td><?php echo $cliente->celular;?></td>
									<td><?php echo $cliente->mail;?></td>
									<td><?php echo $cliente->direccion;?></td>
								</tr>
<?php 						}
?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<ul class="pager">
			<li><a href="#"><span class="icon-prev"></span></a></li>
			<li>Página</li>
			<li><a href="#"><span class="icon-next"></span></a></li>
		</ul>
<?php
	include('inc/footer.php');	
?>