<?php
    session_start();
    if(!isset($_SESSION['id']))
        header('Location: /admin');

	include ('../vendor/autoload.php');
	include ('inc/bd.php');
    
    //Get the data from the serverRequest
    $request = Zend\Diactoros\ServerRequestFactory::fromGlobals();
	$data 	= $request->getParsedBody();
	$file 	= $request->getUploadedFiles();

    $fields = array('formato' => null, 'material' => null, 'cantidadMaterial' => null, 'cantidadCategoria' => null, 'cantidadTipo' => null, 'cantidadTamanoColor' => null, 'cantidadTamanoTipo' => null, 'tamanoMaterial' => null, 'tamano' => null, 'tamanoColor' => null, 'tamanoTipo' => null, 'tipo' => null, 'colorTipo' => null, 'color' => null, 'colorTamano' => null, 'medida' => null, 'cara' => null, 'caraTamano' => null, 'acabado' => null, 'brilloTipo' => null);
    $values = array_merge($fields, $data);

	//Agregar pedido 
	$db->pedido
    ->insert()
    ->data([
        'cliente_id' 			 => $values['cliente'],
        'sena' 					 => $values['sena'],
        'total'                  => $values['total'],
        'formaPago' 			 => $values['formaPago'],
        'orden' 				 => $values['orden'],
        'categoria_id'			 => $values['categoria'],
        'otro_detalle'           => $values['otroDetalle'],
        'formatoCategoria_id'	 => $values['formato'],
        'materialCategoria_id'	 => $values['material'],
        'materialTamano_id'      => $values['materialTamano'],
        'cantidadMaterial_id'	 => $values['cantidadMaterial'],
        'cantidadCategoria_id'   => $values['cantidadCategoria'],
        'cantidadTipo_id'        => $values['cantidadTipo'],
        'cantidadTamanoColor_id' => $values['cantidadTamanoColor'],
        'cantidadTamanoTipo_id'  => $values['cantidadTamanoTipo'],
        'tamanoMaterial_id'      => $values['tamanoMaterial'],
        'tamanoCategoria_id'     => $values['tamano'],
        'tamanoTipo_id'          => $values['tamanoTipo'],
        'tamanoColor_id'         => $values['tamanoColor'],
        'tipoCategoria_id'       => $values['tipo'],
        'colorTipo_id'           => $values['colorTipo'],
        'colorCategoria_id'      => $values['color'],
        'colorTamano_id'         => $values['colorTamano'],
        'caraCategoria_id'       => $values['cara'],
        'caraTamano_id'          => $values['caraTamano'],
        'medida'                 => $values['medida'],
        'acabadoCategoria_id'    => $values['acabado'],
        'papelTipo_id'           => $values['papelTipo'],
        'brilloTipo_id'          => $values['brilloTipo'],
        'laminadoTamano_id'      => $values['laminadoTamano'],
        'cantidad'				 => $values['cantidad'],
        'entregaEstimada'		 => $values['entregaEstimada'],
        'notas'					 => $values['notas'],
        'responsable_id'         => $values['responsable'],
        'estado_id'              => 1, // Ingresado
        'pedidoFile'			 => $_FILES['pedidoFile']['name'] ? $file['pedidoFile'] : ''
    ])->run();
    
    header('Location: pedidos.php');
?>