<?php 	
	session_start();
	if(!isset($_SESSION['id']))
		header('Location: /admin');

	include ('../vendor/autoload.php');
	include ('inc/bd.php');

	//Get the data from the serverRequest
    $request = Zend\Diactoros\ServerRequestFactory::fromGlobals();
	$data 	= $request->getQueryParams();
	
	include('inc/header.php');
	$seccion = 'clientes';
?>
	<body>

		<?php include('inc/user_menu.php');?>
		<?php include('inc/admin_menu.php');?>

		<div class="container">
			<form action="crear_cliente_do.php" method="post">
			<h3 class="tituloSeparador">CLIENTE</h3>
			<hr class="separador">
			<br>
			<div class="row" style="margin-top: 23px;">
				<div class="col-md-6">
					<div class="row" style="margin-top: 23px;">
						<div class="col-md-3">
							<label class="naranja" style="font-weight:1.5em; font-size:1.5em;">Nombre: </label>
						</div>
						<div class="col-md-9">
							<div class="input-group">
								<input type="text" name="nombre" id="input" class="form-control noBorderR naranja" value="" required="required" title="" placeholder="" style="font-size:1.5em!important;">
								<span class="input-group-addon icon-edit noBorderL" id=""></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
				</div>
			</div>
			<div class="row" style="margin-top: 23px;">
				<div class="col-md-6">
					<div class="row" style="margin-top: 23px;">
						<div class="col-md-3">
							<label class="naranja" style="font-weight:1.5em; font-size:1.5em;">Tel&eacute;fono: </label>
						</div>
						<div class="col-md-9">
							<div class="input-group">
								<input type="text" name="telefono" id="input" class="form-control noBorderR naranja" value="" required="required" title="" placeholder="" style="font-size:1.5em!important;">
								<span class="input-group-addon icon-edit noBorderL" id=""></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row" style="margin-top: 23px;">
						<div class="col-md-3">
							<label class="naranja" style="font-weight:1.5em; font-size:1.5em;">Celular: </label>
						</div>
						<div class="col-md-9">
							<div class="input-group">
								<input type="text" name="celular" id="input" class="form-control noBorderR naranja" value="" required="required" title="" placeholder="" style="font-size:1.5em!important;">
								<span class="input-group-addon icon-edit noBorderL" id=""></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row" style="margin-top: 23px;">
				<div class="col-md-6">
					<div class="row" style="margin-top: 23px;">
						<div class="col-md-3">
							<label class="naranja" style="font-weight:1.5em; font-size:1.5em;">Mail: </label>
						</div>
						<div class="col-md-9">
							<div class="input-group">
								<input type="text" name="mail" id="input" class="form-control noBorderR naranja" value="" required="required" title="" placeholder="" style="font-size:1.5em!important;">
								<span class="input-group-addon icon-edit noBorderL" id=""></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row" style="margin-top: 23px;">
						<div class="col-md-3">
							<label class="naranja" style="font-weight:1.5em; font-size:1.5em;">Direcci&oacute;n: </label>
						</div>
						<div class="col-md-9">
							<div class="input-group">
								<input type="text" name="direccion" id="input" class="form-control noBorderR naranja" value="" required="required" title="" placeholder="" style="font-size:1.5em!important;">
								<span class="input-group-addon icon-edit noBorderL" id=""></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row" style="margin-top: 50px;text-align: center">
				<button type="submit" class="btn adminBtn">CREAR</button>
			</div>
			<input type="hidden" name="pedido" value="<?php echo $data['ref'] ? 1 : 0;?>" required="required" title="">
		</form>
		</div>
<?php
	include('inc/footer.php');	
?>