<?php 	
	session_start();
	if(!isset($_SESSION['id']))
		header('Location: /admin');

	include ('../vendor/autoload.php');
	include ('inc/bd.php');

	include('inc/header.php');
	$seccion = 'usuarios';
?>
	<body>

		<?php include('inc/user_menu.php');?>
		<?php include('inc/admin_menu.php');?>

		<div class="container">
			<form action="crear_usuario_do.php" method="post">
			<h3 class="tituloSeparador">USUARIO</h3>
			<hr class="separador">
			<br>
			<div class="row" style="margin-top: 23px;">
				<div class="col-md-6">
					<div class="row" style="margin-top: 23px;">
						<div class="col-md-3">
							<label class="naranja" style="font-weight:1.5em; font-size:1.5em;">Nombre: </label>
						</div>
						<div class="col-md-9">
							<div class="input-group">
								<input type="text" name="nombre" id="input" class="form-control noBorderR naranja" value="" required="required" title="" placeholder="" style="font-size:1.5em!important;">
								<span class="input-group-addon icon-edit noBorderL" id=""></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row" style="margin-top: 23px;">
						<div class="col-md-3">
							<label class="naranja" style="font-weight:1.5em; font-size:1.5em;">Clave: </label>
						</div>
						<div class="col-md-9">
							<div class="input-group">
								<input type="password" name="clave" id="input" class="form-control noBorderR naranja" value="" required="required" title="" placeholder="" style="font-size:1.5em!important;">
								<span class="input-group-addon icon-edit noBorderL" id=""></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row" style="margin-top: 50px;text-align: center">
				<button type="submit" class="btn adminBtn">CREAR</button>
			</div>
		</form>
		</div>
<?php
	include('inc/footer.php');	
?>