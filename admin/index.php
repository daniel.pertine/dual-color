<?php
	session_start();
	if(isset($_SESSION['id']))
		header('Location: pedidos.php');
	include('inc/header.php');	
?>
	<body>

		<?php include('inc/user_menu.php');?>	
		<?php include('inc/admin_menu.php');?>

		<div class="container" style="position: relative;">
			
			<!-- Modal -->
			<div class="modal fade" id="userLogin" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
							<h4 class="modal-title">INICIAR SESIÓN</h4>
						</div>
						<div class="modal-body">
							<form action="inc/loguear_usuario.php" method="POST" role="form">
								<div class="form-group form-inline">
									<label for="inputUser">USUARIO: </label>
									<input type="text" name="user" id="inputUser" class="form-control" value="" required="required" title="" placeholder="click aquí">
									<br>
									<label for="inputPassword">CONTRASEÑA: </label>
									<input type="password" name="password" id="inputPassword" class="form-control" value="" required="required" title="" placeholder="click aquí">
								</div>

								<button type="submit" class="btn loginBtn">INGRESAR</button>
							</form>
						</div>
					</div>
				</div>
			</div>

		</div>
<?php
	include('inc/footer.php');
?>