<?php 	
	session_start();
	if(!isset($_SESSION['id']))
		header('Location: /admin');

	include ('../vendor/autoload.php');
	include ('inc/bd.php');
	
	$clientes		= $db->cliente
					->select()
					->run();

	$categorias		= $db->categoria
					->select()
					->run();

	$responsables	= $db->usuario
					->select()
					->run();

	$orden			= $db->pedido
					->count()
					->run();
	
	//Get the data from the serverRequest
    $request = Zend\Diactoros\ServerRequestFactory::fromGlobals();
	$data 	= $request->getQueryParams();

	$pedido = $data['id'] ? 

				$db->pedido
			    ->select()
			    ->one()
			    ->by('id', $data['id'])
			    ->run()
			    :
			    $db->pedido->create();

	include('inc/header.php');
	$seccion = 'pedidos';
?>
	<body>

		<?php include('inc/user_menu.php');?>
		<?php include('inc/admin_menu.php');?>

		<div class="container">
		
			<!--
			<form action="" method="POST" role="form">
				<legend>Form title</legend>
			
				<div class="form-group">
					<label for="">label</label>
					<input type="text" class="form-control" id="" placeholder="Input field">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
			-->
			<form action="crear_pedido_do.php" method="post" enctype='multipart/form-data'>
			<h3 class="tituloSeparador">CLIENTE</h3>
			<hr class="separador">
			<br>
			<div class="row">
				<div class="col-md-5">
					<div class="row">
						<div class="col-md-10">
							<label for="input-id" class="col-md-12 titleForm">Nombre</label>
							<div class="input-group col-md-12">
								<div class="ui-widget">
							  		<select name="cliente" id="combobox" class="form-control" required="required">
								 	   <option value=''>Seleccione cliente</option>
<?php 										foreach($clientes as $cliente)
												echo "<option value='{$cliente->id}'".($pedido->cliente_id == $cliente->id || $data['cliente'] == $cliente->id ? 'selected=selected' : '').">{$cliente->nombre}</option>";
?>
								  	</select>
								</div>
							</div>
						</div>
						<div class="col-md-10">
							<div style="float:right; margin-top:20px;">
								<button type="button" class="btnIcon link" data-target="_self" data-rel="crear_cliente.php?ref=pedido"><span class="icon-add"></span></button>
								<button type="button" class="btnIcon"><span class="icon-down2"></span></button>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="row" style="margin-top: 23px;">
						<div class="col-md-4">
							<label class="naranja" style="font-weight:1.5em; font-size:1.5em; margin-top:2px;">TOTAL: </label>
							<div class="input-group" style="width:110px; float:right;">
								<input type="text" name="total" id="input" class="form-control noBorderR naranja" value="<?php echo $pedido->total;?>" required="required"title="" placeholder="$0" style="font-size:1.5em!important;">
								<span class="input-group-addon icon-edit noBorderL" id=""></span>
							</div>
						</div>
						<div class="col-md-4">
							<label class="naranja" style="font-weight:1.5em; font-size:1.5em; margin-top:2px;">SEÑA: </label>
							<div class="input-group" style="width:110px; float:right;">
								<input type="text" name="sena" id="input" class="form-control noBorderR naranja" value="<?php echo $pedido->sena;?>" required="required"title="" placeholder="$0" style="font-size:1.5em!important;">
								<span class="input-group-addon icon-edit noBorderL" id=""></span>
							</div>
						</div>
						<div class="col-md-4">
							<button type="submit" class="btn adminBtn" style="width: 190px;">LISTO</button>
						</div>
						<div class="col-md-12">
							<div class="row" style="margin-top:10px;">
								<div class="col-md-3">
									<p class="txtForm" style="margin-top:6px;"><span class="naranja">*</span> FORMA DE PAGO</p>
								</div>
								<div class="col-md-3">
									<div class="form2">
										<div class="checkbox">
											<input name="formaPago" value="1" id="efectivo" type="checkbox" class="image-checkbox" <?php echo $pedido->formaPago == 1 ? 'checked=checked' : '';?>/>
											<label for="efectivo" class="image-checkbox-label txtForm" style="padding: 2px 0 0 30px;">Efectivo</label>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form2">
										<div class="checkbox">
											<input name="formaPago" value="2" id="transferencia" type="checkbox" class="image-checkbox" <?php echo $pedido->formaPago == 2 ? 'checked=checked' : '';?>/>
											<label for="transferencia" class="image-checkbox-label txtForm" style="padding: 2px 0 0 30px;">Transf.</label>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form2">
										<div class="checkbox">
											<input name="formaPago" value="3" id="cheque" type="checkbox" class="image-checkbox" <?php echo $pedido->formaPago == 3 ? 'checked=checked' : '';?>/>    
											<label for="cheque" class="image-checkbox-label txtForm" style="padding: 2px 0 0 30px;">Cheque</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row" style="margin-top: 15px;">
				<div class="col-md-12">

					<div role="tabpanel">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist"></ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="pedido1">
								<div class="row">
									<div class="col-md-3">
										<div class="input-group">
											<span class="input-group-addon noBorder" id="basic-addon3" style="padding-left:3px;">N° de orden:</span>
											<input name="orden" type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" value="<?php echo $pedido->orden ? $pedido->orden : $orden+1;?>" placeholder="" style="border-radius:4px; width:60px;" required="required">
										</div>
									</div>
								</div>
								<div class="row" style="margin-top:20px;">
									<div class="col-md-2">
										<label class="titleForm">Categoría</label>
										<div class="form1">
											<div class="checkbox">
												<input type="checkbox" class="image-checkbox" checked />    
												<label for="categoria" class="image-checkbox-label"></label>
											</div>
										</div>
										<select name="categoria" id="categoria" class="form-control" required="required">
											<option value=''>Seleccione categoría</option>
<?php 								foreach($categorias as $categoria)
										echo "<option value='{$categoria->id}' ".($pedido->categoria_id == $categoria->id ? 'selected=selected' : '').">{$categoria->nombre}</option>";
?>
										</select>
									</div>
									<div id="otroDetalleDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->otro_detalle ? 'style="display:none"' : '';?>>
										<label class="titleForm">Detalle</label>
										<input type="text" name="otroDetalle" id="input" class="form-control" value="<?php echo $pedido->otro_detalle;?>">
									</div>
									<div id="formatoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->formatoCategoria_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Formato</label>
										<div class="form1">
											<div class="checkbox">
												<input id="formatoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="formato" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->formatoCategoria_id)
										{
											$formatos = $db->formatoCategoria
													    ->select()
													    ->by('categoria_id', $pedido->categoria_id)
													    ->run();
?>
											<select name="formato" id="formato" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione formato</option>
<?php 											foreach($formatos as $formato)
													echo "<option value='{$formato->id}' ".($pedido->formatoCategoria_id == $formato->id ? 'selected=selected' : '').">{$formato->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="materialDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->materialCategoria_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Material</label>
										<div class="form1">
											<div class="checkbox">
												<input id="materialCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="material" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->materialCategoria_id)
										{
											$materiales = $db->materialCategoria
													    ->select()
													    ->by('categoria_id', $pedido->categoria_id)
													    ->run();
?>
											<select name="material" id="material" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione material</option>
<?php 											foreach($materiales as $material)
													echo "<option value='{$material->id}' ".($pedido->materialCategoria_id == $material->id ? 'selected=selected' : '').">{$material->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="cantidadMaterialDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->cantidadMaterial_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Hojas</label>
										<div class="form1">
											<div class="checkbox">
												<input id="cantidadCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="cantidad" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->cantidadMaterial_id)
										{
											$cantidades = $db->cantidadMaterial
													    ->select()
													    ->by('materialCategoria_id', $pedido->materialCategoria_id)
													    ->run();
?>
											<select name="cantidadMaterial" id="cantidadMaterial" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione cantidad</option>
<?php 											foreach($cantidades as $cantidad)
													echo "<option value='{$cantidad->id}' ".($pedido->cantidadMaterial_id == $cantidad->id ? 'selected=selected' : '').">{$cantidad->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="tamanoMaterialDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->tamanoMaterial_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Tamaño</label>
										<div class="form1">
											<div class="checkbox">
												<input id="tamañoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="tamaño" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->tamanoMaterial_id)
										{
											$tamaños = $db->tamanoMaterial
													    ->select()
													    ->by('materialCategoria_id', $pedido->materialCategoria_id)
													    ->run();
?>
											<select name="tamanoMaterial" id="tamanoMaterial" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione tamaño</option>
<?php 											foreach($tamaños as $tamaño)
													echo "<option value='{$tamaño->id}' ".($pedido->materialCategoria_id == $tamaño->id ? 'selected=selected' : '').">{$tamaño->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="tipoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->tipoCategoria_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Tamaño</label>
										<div class="form1">
											<div class="checkbox">
												<input id="tipoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="tipo" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->tipoCategoria_id)
										{
											$tipos = $db->tipoCategoria
													    ->select()
													    ->by('categoria_id', $pedido->categoria_id)
													    ->run();
?>
											<select name="tipo" id="tipo" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione tipo</option>
<?php 											foreach($tipos as $tipo)
													echo "<option value='{$tipo->id}' ".($pedido->tipoCategoria_id == $tipo->id ? 'selected=selected' : '').">{$tipo->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="colorTipoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->colorTipo_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Tamaño</label>
										<div class="form1">
											<div class="checkbox">
												<input id="colorTipoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="colorTipo" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->colorTipo_id)
										{
											$colores = $db->colorTipo
													    ->select()
													    ->by('tipoCategoria_id', $pedido->tipoCategoria_id)
													    ->run();
?>
											<select name="colorTipo" id="colorTipo" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione color</option>
<?php 											foreach($colores as $color)
													echo "<option value='{$color->id}' ".($pedido->colorTipo_id == $color->id ? 'selected=selected' : '').">{$color->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="medidaDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->medida ? 'style="display:none"' : '';?>>
										<label class="titleForm">Medida</label>
										<input type="number" name="medida" id="input" class="form-control" value="<?php echo $pedido->medida;?>" min="" max="" step="" title="" style="margin-top:8px;">
									</div>
									<div id="colorDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->colorTipo_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Color</label>
										<div class="form1">
											<div class="checkbox">
												<input id="colorCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="color" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->colorCategoria_id)
										{
											$colores = $db->colorCategoria
													    ->select()
													    ->by('categoria_id', $pedido->categoria_id)
													    ->run();
?>
											<select name="color" id="color" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione color</option>
<?php 											foreach($colores as $color)
													echo "<option value='{$color->id}' ".($pedido->colorCategoria_id == $color->id ? 'selected=selected' : '').">{$color->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="tamanoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->tamanoCategoria_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Color</label>
										<div class="form1">
											<div class="checkbox">
												<input id="tamanoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="tamano" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->tamanoCategoria_id)
										{
											$tamaños = $db->tamanoCategoria
													    ->select()
													    ->by('categoria_id', $pedido->categoria_id)
													    ->run();
?>
											<select name="tamano" id="tamano" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione tamaño</option>
<?php 											foreach($tamaños as $tamaño)
													echo "<option value='{$tamaño->id}' ".($pedido->tamanoCategoria_id == $tamaño->id ? 'selected=selected' : '').">{$tamaño->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="acabadoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->acabadoCategoria_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Acabado</label>
										<div class="form1">
											<div class="checkbox">
												<input id="acabadoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="acabado" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->acabadoCategoria_id)
										{
											$acabados = $db->acabadoCategoria
													    ->select()
													    ->by('categoria_id', $pedido->categoria_id)
													    ->run();
?>
											<select name="acabado" id="acabado" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione tamaño</option>
<?php 											foreach($acabados as $acabado)
													echo "<option value='{$acabado->id}' ".($pedido->acabadoCategoria_id == $acabado->id ? 'selected=selected' : '').">{$acabado->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="caraDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->caraCategoria_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Acabado</label>
										<div class="form1">
											<div class="checkbox">
												<input id="acabadoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="acabado" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->caraCategoria_id)
										{
											$caras = $db->caraCategoria
													    ->select()
													    ->by('categoria_id', $pedido->categoria_id)
													    ->run();
?>
											<select name="cara" id="cara" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione cara</option>
<?php 											foreach($cara as $caras)
													echo "<option value='{$cara->id}' ".($pedido->caraCategoria_id == $cara->id ? 'selected=selected' : '').">{$cara->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="caraDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->tamanoColor_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Tamaño</label>
										<div class="form1">
											<div class="checkbox">
												<input id="tamanoColorCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="tamanoColor" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->tamanoColor_id)
										{
											$tamaños = $db->tamanoColor
													    ->select()
													    ->by('colorCategoria_id', $pedido->colorCategoria_id)
													    ->run();
?>
											<select name="tamanoColor" id="tamanoColor" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione tamaño</option>
<?php 											foreach($tamaños as $tamaño)
													echo "<option value='{$tamaño->id}' ".($pedido->tamanoColor_id == $tamaño->id ? 'selected=selected' : '').">{$tamaño->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="tamanoTipoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->tamanoTipo_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Tamaño</label>
										<div class="form1">
											<div class="checkbox">
												<input id="tamanoTipoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="tamanoTipo" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->tamanoTipo_id)
										{
											$tamaños = $db->tamanoTipo
													    ->select()
													    ->by('tipoCategoria_id', $pedido->tipoCategoria_id)
													    ->run();
?>
											<select name="tamanoTipo" id="tamanoTipo" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione tamaño</option>
<?php 											foreach($tamaños as $tamaño)
													echo "<option value='{$tamaño->id}' ".($pedido->tamanoTipo_id == $tamaño->id ? 'selected=selected' : '').">{$tamaño->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="cantidadTamanoColorDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->cantidadTamanoColor_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Cantidad</label>
										<div class="form1">
											<div class="checkbox">
												<input id="cantidadTamanoColorCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="cantidadTamanoColor" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->cantidadTamanoColor_id)
										{
											$cantidades = $db->cantidadTamanoColor
													    ->select()
													    ->by('tamanoColor_id', $pedido->tamanoColor_id)
													    ->run();
?>
											<select name="cantidadTamanoColor" id="cantidadTamanoColor" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione cantidad</option>
<?php 											foreach($cantidades as $cantidad)
													echo "<option value='{$cantidad->id}' ".($pedido->cantidadTamanoColor_id == $cantidad->id ? 'selected=selected' : '').">{$cantidad->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="materialTamanoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->materialTamano_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Material</label>
										<div class="form1">
											<div class="checkbox">
												<input id="materialTamanoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="materialTamano" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->materialTamano_id)
										{
											$materiales = $db->materialTamano
													    ->select()
													    ->by('tamanoCategoria_id', $pedido->tamanoCategoria_id)
													    ->run();
?>
											<select name="materialTamano" id="materialTamano" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione material</option>
<?php 											foreach($materiales as $material)
													echo "<option value='{$material->id}' ".($pedido->materialTamano_id == $material->id ? 'selected=selected' : '').">{$material->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="cantidadCategoriaDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->cantidadCategoria_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Hojas</label>
										<div class="form1">
											<div class="checkbox">
												<input id="cantidadCategoriaCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="cantidadCategoria" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->cantidadCategoria_id)
										{
											$cantidades = $db->cantidadCategoria
													    ->select()
													    ->by('categoria_id', $pedido->categoria_id)
													    ->run();
?>
											<select name="cantidadCategoria" id="cantidadCategoria" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione hojas</option>
<?php 											foreach($cantidades as $cantidad)
													echo "<option value='{$cantidad->id}' ".($pedido->cantidadCategoria_id == $cantidad->id ? 'selected=selected' : '').">{$cantidad->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="colorTamanoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->colorTamano_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Color</label>
										<div class="form1">
											<div class="checkbox">
												<input id="colorTamanoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="colorTamano" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->colorTamano_id)
										{
											$colores = $db->colorTamano
													    ->select()
													    ->by('tamanoTipo_id', $pedido->tamanoTipo_id)
													    ->run();
?>
											<select name="colorTamano" id="colorTamano" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione color</option>
<?php 											foreach($colores as $color)
													echo "<option value='{$color->id}' ".($pedido->colorTamano_id == $color->id ? 'selected=selected' : '').">{$color->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="caraTamanoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->caraTamano_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Caras</label>
										<div class="form1">
											<div class="checkbox">
												<input id="caraTamanoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="caraTamano" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->caraTamano_id)
										{
											$caras = $db->caraTamano
													    ->select()
													    ->by('tamanoTipo_id', $pedido->tamanoTipo_id)
													    ->run();
?>
											<select name="caraTamano" id="caraTamano" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione cara</option>
<?php 											foreach($caras as $cara)
													echo "<option value='{$cara->id}' ".($pedido->caraTamano_id == $cara->id ? 'selected=selected' : '').">{$cara->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="cantidadTamanoTipoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->cantidadTamanoTipo_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Cantidad</label>
										<div class="form1">
											<div class="checkbox">
												<input id="cantidadTamanoTipoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="cantidadTamanoTipo" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->cantidadTamanoTipo_id)
										{
											$cantidades = $db->cantidadTamanoTipo
													    ->select()
													    ->by('tamanoTipo_id', $pedido->tamanoTipo_id)
													    ->run();
?>
											<select name="cantidadTamanoTipo" id="cantidadTamanoTipo" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione cantidad</option>
<?php 											foreach($cantidades as $cantidad)
													echo "<option value='{$cantidad->id}' ".($pedido->cantidadTamanoTipo_id == $cantidad->id ? 'selected=selected' : '').">{$cantidad->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="laminadoTamanoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->laminadoTamano_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Laminado</label>
										<div class="form1">
											<div class="checkbox">
												<input id="laminadoTamanoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="laminadoTamano" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->laminadoTamano_id)
										{
											$laminados = $db->laminadoTamano
													    ->select()
													    ->by('tamanoTipo_id', $pedido->tamanoTipo_id)
													    ->run();
?>
											<select name="laminadoTamano" id="laminadoTamano" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione laminado</option>
<?php 											foreach($laminados as $laminado)
													echo "<option value='{$laminado->id}' ".($pedido->laminadoTamano_id == $laminado->id ? 'selected=selected' : '').">{$laminado->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="brilloTipoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->brilloTipo_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Brillo</label>
										<div class="form1">
											<div class="checkbox">
												<input id="brilloTipoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="brilloTipo" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->brilloTipo_id)
										{
											$brillos = $db->brilloTipo
													    ->select()
													    ->by('tipoCategoria_id', $pedido->tipoCategoria_id)
													    ->run();
?>
											<select name="brilloTipo" id="brilloTipo" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione brillo</option>
<?php 											foreach($brillos as $brillo)
													echo "<option value='{$brillo->id}' ".($pedido->brilloTipo_id == $brillo->id ? 'selected=selected' : '').">{$brillo->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div id="cantidadTipoDiv" class="col-md-2 dinamicDivs" <?php echo !$pedido->cantidadTipo_id ? 'style="display:none"' : '';?>>
										<label class="titleForm">Hojas</label>
										<div class="form1">
											<div class="checkbox">
												<input id="cantidadTipoCheckbox" type="checkbox" class="image-checkbox" checked />    
												<label for="cantidadTipo" class="image-checkbox-label"></label>
											</div>
										</div>
<?php									if($pedido->cantidadTipo_id)
										{
											$cantidades = $db->cantidadTipo
													    ->select()
													    ->by('tipoCategoria_id', $pedido->tipoCategoria_id)
													    ->run();
?>
											<select name="cantidadTipo" id="cantidadTipo" class="form-control dinamicSelects" required="required">
												<option value=''>Seleccione cantidad</option>
<?php 											foreach($cantidades as $cantidad)
													echo "<option value='{$cantidad->id}' ".($pedido->cantidadTipo_id == $cantidad->id ? 'selected=selected' : '').">{$cantidad->nombre}</option>";
?>
											</select>
<?php 									}
?>
									</div>
									<div class="col-md-2">
										<label class="titleForm">Cantidad</label>
										<input type="number" name="cantidad" id="input" class="form-control" value="<?php echo $pedido->cantidad;?>" min="" max="" step="" required="required" title="" style="margin-top:8px;">
									</div>
								</div>
								<div class="row" style="margin-top: 15px; margin-bottom: 70px;">
									<div class="col-md-9">
										<h3 class="tituloSeparador">DATOS GENERALES</h3>
										<hr class="separador">
										<br>
										<div class="row">
											<div class="col-md-6">
												<div class="row">
													<div class="col-sm-12">
														<div class="form1">
															<div class="checkbox">
																<input id="responsable" type="checkbox" class="image-checkbox" checked />    
																<label for="responsable" class="image-checkbox-label"></label>
															</div>
														</div>
														<label>*Responsable</label>
														<select name="responsable" id="input" class="form-control" required="required">
															<option value=''>Seleccione responsable</option>
<?php 													foreach($responsables as $responsable)
															echo "<option value='{$responsable->id}'>{$responsable->usuario}</option>";
?>	
														</select>
													</div>
													<div class="col-sm-12" style="margin-top:10px;">
														<label>*Entrega estimada</label>
														<div class="form1">
															<div class="checkbox">
																<input id="entregaInmediata" type="checkbox" class="image-checkbox" checked />    
																<label for="entregaInmediata" class="image-checkbox-label"></label>
															</div>
														</div>
														<input type="text" name="entregaEstimada" id="input" class="form-control" value="<?php echo $pedido->entregaEstimada ? $pedido->entregaEstimada->format('d-m-Y') :  '';?>" required="required" title="">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<label>Notas</label>
												<textarea name="notas" id="input" class="form-control" rows="5" required="required" placeholder=""><?php echo $pedido->notas;?></textarea>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<h3 class="tituloSeparador">DATOS ARCHIVO</h3>
										<hr class="separador">
										<br>
										<label>Ubicación del archivo</label>
										<div class="input-group">
											<input type="file" name="pedidoFile" id="input" class="form-control noBorderR" value="" pattern="" title="">
											<span class="input-group-addon icon-edit noBorderL" id=""></span>
										</div>
										<div style="float:left; margin-top:20px;">
											<button type="button" class="btnIcon"><span class="icon-attachment"></span></button>
											<button type="button" class="btnIcon link" data-target="_blank" data-rel="<?php echo str_replace('/var/www/html','',$pedido->pedidoFile);?>"><span class="icon-down1"></span></button>
										</div>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="pedido2"></div>
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>
<?php
	include('inc/footer.php');	
?>