$(document).ready(function(){
	
	$('#userLogin').modal('show');

    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });

    //Google ReCaptcha
	$('#legitSubmit').click(function(event) {
		if(grecaptcha.getResponse() == "")
		    alert("Falta confirmar el captcha!");
		else{	
		    $('#submit').trigger('click');
		}
	});

	$('body').on('change', '#categoria', function() {
		$( ".dinamicSelects" ).remove();
		$( ".dinamicDivs" ).hide();
		switch($( this ).val())
		{
			case '1':
				// Anillados
				$.get( "inc/crear_pedido_formato.php", {categoria_id: $( this ).val()}, function( data ) {
					options = "<option value=''>Seleccione formato</option>";
					options += $.map(data, function(value) {
						return "<option value='"+value.id+"'>"+value.nombre+"</option>";
					});
					$( "#formatoDiv" ).append('<select name="formato" id="formato" class="form-control dinamicSelects" required="required">'+options+'</select>');
					$( "#formatoDiv" ).show();
				}, "json" );
				break;
			case '2':
				// Cartelería
			case '4':
				// Imanes
			case '8':
				// Revistas
			case '9':
				// Stickers
			case '10':
				// Tarjeteria
				$.get( "inc/crear_pedido_tipo.php", {categoria_id: $( this ).val()}, function( data ) {
					options = "<option value=''>Seleccione tipo</option>";
					options += $.map(data, function(value) {
						return "<option value='"+value.id+"'>"+value.nombre+"</option>";
					});
					$( "#tipoDiv" ).append('<select name="tipo" id="tipo" class="form-control dinamicSelects" required="required">'+options+'</select>');
					$( "#tipoDiv" ).show();
				}, "json" );
				break;
			case '3':
				// Folletería
				$.get( "inc/crear_pedido_color.php", {categoria_id: $( this ).val()}, function( data ) {
					options = "<option value=''>Seleccione color</option>";
					options += $.map(data, function(value) {
						return "<option value='"+value.id+"'>"+value.nombre+"</option>";
					});
					$( "#colorDiv" ).append('<select name="color" id="color" class="form-control dinamicSelects" required="required">'+options+'</select>');
					$( "#colorDiv" ).show();
				}, "json" );
				$.get( "inc/crear_pedido_cara.php", {categoria_id: $( this ).val()}, function( data ) {
					options = "<option value=''>Seleccione caras</option>";
					options += $.map(data, function(value) {
						return "<option value='"+value.id+"'>"+value.nombre+"</option>";
					});
					$( "#caraDiv" ).append('<select name="cara" id="cara" class="form-control dinamicSelects" required="required">'+options+'</select>');
					$( "#caraDiv" ).show();
				}, "json" );
				break;
			case '5':
				// Impresiones
				$.get( "inc/crear_pedido_cantidad.php", {categoria_id: $( this ).val()}, function( data ) {
					options = "<option value=''>Seleccione cantidad</option>";
					options += $.map(data, function(value) {
						return "<option value='"+value.id+"'>"+value.nombre+"</option>";
					});
					$( "#cantidadCategoriaDiv" ).append('<select name="cantidadCategoria" id="cantidadCategoria" class="form-control dinamicSelects" required="required">'+options+'</select>');
					$( "#cantidadCategoriaDiv" ).show();
				}, "json" );
			case '6':
				// Laminados
			case '7':
				// Pines
				$.get( "inc/crear_pedido_tamano.php", {categoria_id: $( this ).val()}, function( data ) {
					options = "<option value=''>Seleccione tamaño</option>";
					options += $.map(data, function(value) {
						return "<option value='"+value.id+"'>"+value.nombre+"</option>";
					});
					$( "#tamanoDiv" ).append('<select name="tamano" id="tamano" class="form-control dinamicSelects" required="required">'+options+'</select>');
					$( "#tamanoDiv" ).show();
				}, "json" );
				if($( this ).val()==6)
				{
					$.get( "inc/crear_pedido_acabado.php", {categoria_id: $( this ).val()}, function( data ) {
						options = "<option value=''>Seleccione acabado</option>";
						options += $.map(data, function(value) {
							return "<option value='"+value.id+"'>"+value.nombre+"</option>";
						});
						$( "#acabadoDiv" ).append('<select name="acabado" id="acabado" class="form-control dinamicSelects" required="required">'+options+'</select>');
						$( "#acabadoDiv" ).show();
					}, "json" );
					$.get( "inc/crear_pedido_cara.php", {categoria_id: $( this ).val()}, function( data ) {
						options = "<option value=''>Seleccione caras</option>";
						options += $.map(data, function(value) {
							return "<option value='"+value.id+"'>"+value.nombre+"</option>";
						});
						$( "#caraDiv" ).append('<select name="cara" id="cara" class="form-control dinamicSelects" required="required">'+options+'</select>');
						$( "#caraDiv" ).show();
					}, "json" );
				}
				break;
			case '11':
				// Otro
				$( "#otroDetalleDiv" ).show();
		}
  	});

	// Pedido Arma Select Material
	$('body').on('change', '#formato', function() {
	    $.get( "inc/crear_pedido_material.php", {categoria_id: $( this ).val()}, function( data ) {
	    	$( "#materialDiv #material" ).remove();
	    	options = "<option value=''>Seleccione material</option>";
			options += $.map(data, function(value) {
				return "<option value='"+value.id+"'>"+value.nombre+"</option>";
			});
			$( "#materialDiv" ).append('<select name="material" id="material" class="form-control dinamicSelects" required="required">'+options+'</select>');
			$( "#materialDiv" ).show();
		}, "json" );
	});

	// Pedido Arma Select Material Cantidad
	$('body').on('change', '#material', function() {
		if($( this ).val()==1)
		{
			// Plástico
		    $.get( "inc/crear_pedido_material_cantidad.php", {materialCategoria_id: $( this ).val()}, function( data ) {
		    	$( "#cantidadMaterialDiv #cantidadMaterial" ).remove();
		    	options = "<option value=''>Seleccione cantidad</option>";
				options += $.map(data, function(value) {
					return "<option value='"+value.id+"'>"+value.nombre+"</option>";
				});
				$( "#cantidadMaterialDiv" ).append('<select name="cantidadMaterial" id="cantidadMaterial" class="form-control dinamicSelects" required="required">'+options+'</select>');
				$( "#cantidadMaterialDiv" ).show();
			}, "json" );
		}
		else
		{
			// Metal
		    $.get( "inc/crear_pedido_material_tamano.php", {materialCategoria_id: $( this ).val()}, function( data ) {
		    	$( "#tamanoMaterialDiv #tamanoMaterial" ).remove();
		    	options = "<option value=''>Seleccione tamaño</option>";
				options += $.map(data, function(value) {
					return "<option value='"+value.id+"'>"+value.nombre+"</option>";
				});
				$( "#tamanoMaterialDiv" ).append('<select name="tamanoMaterial" id="tamanoMaterial" class="form-control dinamicSelects" required="required">'+options+'</select>');
				$( "#tamanoMaterialDiv" ).show();
			}, "json" );
		}	
	});

	$('body').on('change', '#tipo', function() {
		switch($( this ).val())
		{
			case 1: // Lona Front
			case 2: // Lona Black out
			case 3: // Vinilo Interior blanco
			case 4: // Vinilo Exterior blanco
			case 5: // Vinilo Transparente
			case 6: // Microperforado
				$( "#medidaDiv label" ).html('Medidas 1mt x ...');
				break;		
			case 7: // Vinilo Textil
			case 9:
				// Vinilo Textil / Vinilo de corte
			    $.get( "inc/crear_pedido_tipo_color.php", {tipoCategoria_id: $( this ).val()}, function( data ) {
			    	$( "#colorTipoDiv #colorTipo" ).remove();
			    	options = "<option value=''>Seleccione color</option>";
					options += $.map(data, function(value) {
						return "<option value='"+value.id+"'>"+value.nombre+"</option>";
					});
					$( "#colorTipoDiv" ).append('<select name="colorTipo" id="colorTipo" class="form-control dinamicSelects" required="required">'+options+'</select>');
					$( "#colorTipoDiv" ).show();
				}, "json" );
				$( "#medidaDiv label" ).html('Medidas 55 cm x ...');
				break;
			case 8:
				// Vinilo impreso troquelado
				$( "#medidaDiv label" ).html('Planchas 145 x 55 cm');
				break;
			case 10:
				// Banner
				$( "#medidaDiv label" ).html('90x190 cm');
				break;
			case 11:
				// Plancha
				$.get( "inc/crear_pedido_tipo_papel.php", {tipoCategoria_id: $( this ).val()}, function( data ) {
				    	$( "#papelTipoDiv #papelTipo" ).remove();
				    	options = "<option value=''>Seleccione papel</option>";
						options += $.map(data, function(value) {
							return "<option value='"+value.id+"'>"+value.nombre+"</option>";
						});
						$( "#papelTipoDiv" ).append('<select name="papelTipo" id="papelTipo" class="form-control dinamicSelects" required="required">'+options+'</select>');
						$( "#papelTipoDiv" ).show();
					}, "json" );
			case 12:
				// Cantidad
			case 14:
				// Blocks
			case 15:
				// Carpetas
			case 16:
				// Catálogos
			case 19:
				// Cuadradas
			case 20:
				// Personales
			case 21:
				// Postales
			case 22:
				// Tarjetones
			case 23:
				// Etiquetas ropa
			case 24:
				// Señaladores
			    $.get( "inc/crear_pedido_tipo_tamano.php", {tipoCategoria_id: $( this ).val()}, function( data ) {
			    	$( "#tamanoTipoDiv #tamanoTipo" ).remove();
			    	options = "<option value=''>Seleccione tamano</option>";
					options += $.map(data, function(value) {
						return "<option value='"+value.id+"'>"+value.nombre+"</option>";
					});
					$( "#tamanoTipoDiv" ).append('<select name="tamanoTipo" id="tamanoTipo" class="form-control dinamicSelects" required="required">'+options+'</select>');
					$( "#tamanoTipoDiv" ).show();
				}, "json" );
				if($( this ).val() != 12 && $( this ).val() != 14)
					break;
					// Sigue cantidad y block
			case 17:
				// 7 x 5 cm	
			case 18:
				// Plancha
				$.get( "inc/crear_pedido_tipo_cantidad.php", {tipoCategoria_id: $( this ).val()}, function( data ) {
			    	$( "#cantidadTipoDiv #cantidadTipo" ).remove();
			    	options = "<option value=''>Seleccione cantidad</option>";
					options += $.map(data, function(value) {
						return "<option value='"+value.id+"'>"+value.nombre+"</option>";
					});
					$( "#cantidadTipoDiv" ).append('<select name="cantidadTipo" id="cantidadTipo" class="form-control dinamicSelects" required="required">'+options+'</select>');
					$( "#cantidadTipoDiv" ).show();
				}, "json" );
				if($( this ).val() != 18)
					break;
					// Sigue Plancha
				$.get( "inc/crear_pedido_tipo_brillo.php", {tipoCategoria_id: $( this ).val()}, function( data ) {
			    	$( "#brilloTipoDiv #brilloTipo" ).remove();
			    	options = "<option value=''>Seleccione brillo</option>";
					options += $.map(data, function(value) {
						return "<option value='"+value.id+"'>"+value.nombre+"</option>";
					});
					$( "#brilloTipoDiv" ).append('<select name="brilloTipo" id="brilloTipo" class="form-control dinamicSelects" required="required">'+options+'</select>');
					$( "#brilloTipoDiv" ).show();
				}, "json" );
				break;
		}
		// Pedido Muestra medida
		$( "#medidaDiv" ).show();
		$( "#medidaDiv").attr('required', ''); 
	});

	// Pedido Arma Select Tamano
	$('body').on('change', '#colorCategoria', function() {
	    $.get( "inc/crear_pedido_color_tamano.php", {colorCategoria_id: $( this ).val()}, function( data ) {
	    	$( "#tamanoColorDiv #tamanoColor" ).remove();
	    	options = "<option value=''>Seleccione tamaño</option>";
			options += $.map(data, function(value) {
				return "<option value='"+value.id+"'>"+value.nombre+"</option>";
			});
			$( "#tamanoColorDiv" ).append('<select name="tamanoColor" id="tamanoColor" class="form-control dinamicSelects" required="required">'+options+'</select>');
			$( "#tamanoColorDiv" ).show();
		}, "json" );
	});

	// Pedido Arma Select Cantidad
	$('body').on('change', '#tamanoColor', function() {
	    $.get( "inc/crear_pedido_color_tamano_cantidad.php", {tamanoColor_id: $( this ).val()}, function( data ) {
	    	$( "#cantidadTamanoDiv #cantidadTamano" ).remove();
	    	options = "<option value=''>Seleccione cantidad</option>";
			options += $.map(data, function(value) {
				return "<option value='"+value.id+"'>"+value.nombre+"</option>";
			});
			$( "#cantidadTamanoDiv" ).append('<select name="cantidadTamano" id="cantidadTamano" class="form-control dinamicSelects" required="required">'+options+'</select>');
			$( "#cantidadTamanoDiv" ).show();
		}, "json" );
	});

	// Pedido Arma Select Material
	$('body').on('change', '#tamano', function() {
	    $.get( "inc/crear_pedido_tamano_material.php", {tamano_id: $( this ).val()}, function( data ) {
	    	$( "#materialTamanoDiv #materialTamano" ).remove();
	    	options = "<option value=''>Seleccione material</option>";
			options += $.map(data, function(value) {
				return "<option value='"+value.id+"'>"+value.nombre+"</option>";
			});
			$( "#materialTamanoDiv" ).append('<select name="materialTamano" id="materialTamano" class="form-control dinamicSelects" required="required">'+options+'</select>');
			$( "#materialTamanoDiv" ).show();
		}, "json" );
	});

	// Pedido Arma Select Material
	$('body').on('change', '#tamanoTipo', function() {
		if($( '#tipo' ).val() == 14)
		{
			$.get( "inc/crear_pedido_tipo_tamano_color.php", {tamanoTipo_id: $( this ).val()}, function( data ) {
		    	$( "#colorTamanoDiv #colorTamano" ).remove();
		    	options = "<option value=''>Seleccione colores</option>";
				options += $.map(data, function(value) {
					return "<option value='"+value.id+"'>"+value.nombre+"</option>";
				});
				$( "#colorTamanoDiv" ).append('<select name="colorTamano" id="colorTamano" class="form-control dinamicSelects" required="required">'+options+'</select>');
				$( "#colorTamanoDiv" ).show();
			}, "json" );
		}
		if($( '#tipo' ).val() >= 19 || $( '#tipo' ).val() <= 24)
		{
			$.get( "inc/crear_pedido_tipo_tamano_cantidad.php", {tamanoTipo_id: $( this ).val()}, function( data ) {
		    	$( "#cantidadTamanoDiv #cantidadTamano" ).remove();
		    	options = "<option value=''>Seleccione cantidad</option>";
				options += $.map(data, function(value) {
					return "<option value='"+value.id+"'>"+value.nombre+"</option>";
				});
				$( "#cantidadTamanoDiv" ).append('<select name="cantidadTamano" id="cantidadTamano" class="form-control dinamicSelects" required="required">'+options+'</select>');
				$( "#cantidadTamanoDiv" ).show();
			}, "json" );
			$.get( "inc/crear_pedido_tipo_tamano_cara.php", {tamanoTipo_id: $( this ).val()}, function( data ) {
		    	$( "#caraTamanoDiv #caraTamano" ).remove();
		    	options = "<option value=''>Seleccione caras</option>";
				options += $.map(data, function(value) {
					return "<option value='"+value.id+"'>"+value.nombre+"</option>";
				});
				$( "#caraTamanoDiv" ).append('<select name="caraTamano" id="caraTamano" class="form-control dinamicSelects" required="required">'+options+'</select>');
				$( "#caraTamanoDiv" ).show();
			}, "json" );
			$.get( "inc/crear_pedido_tipo_tamano_laminado.php", {tamanoTipo_id: $( this ).val()}, function( data ) {
		    	$( "#laminadoTamanoDiv #laminadoTamano" ).remove();
		    	options = "<option value=''>Seleccione laminado</option>";
				options += $.map(data, function(value) {
					return "<option value='"+value.id+"'>"+value.nombre+"</option>";
				});
				$( "#laminadoTamanoDiv" ).append('<select name="laminadoTamano" id="laminadoTamano" class="form-control dinamicSelects" required="required">'+options+'</select>');
				$( "#laminadoTamanoDiv" ).show();
			}, "json" );
		}
	});

	$('body').on('change', '.cambioEstado', function() {
		$.ajax({
				method: "POST",
				url: "pedido_cambiar_estado_do.php",
			  	data: { estado: $( this ).val(), pedidoId: $( this ).attr('data-rel') }
			})
		 	.done(function( response ) {
		  	});
	});

	//access link
	$('.link').click(function(event) {
		window.open($( this ).attr('data-rel'), $( this ).attr('data-target'));
	});

	// AUTOCOMPLETE
	$( function() {
		$.widget( "custom.combobox", {
	      _create: function() {
	        this.wrapper = $( "<span>" )
	          .addClass( "custom-combobox" )
	          .insertAfter( this.element );
	 
	        this._createAutocomplete();
	        this._createShowAllButton();
	      },
	 
	      _createAutocomplete: function() {
	        var selected = this.element.children( ":selected" ),
	          value = selected.val() ? selected.text() : "";
	 
	        this.input = $( "<input>" )
	          .appendTo( this.wrapper )
	          .val( value )
	          .attr( "title", "" )
	          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
	          .autocomplete({
	            delay: 0,
	            minLength: 0,
	            source: $.proxy( this, "_source" )
	          })
	          .tooltip({
	            classes: {
	              "ui-tooltip": "ui-state-highlight"
	            }
	          });
	 
	        this._on( this.input, {
	          autocompleteselect: function( event, ui ) {
	            ui.item.option.selected = true;
	            this._trigger( "select", event, {
	              item: ui.item.option
	            });
	          },
	 
	          autocompletechange: "_removeIfInvalid"
	        });
	      },
	 
	      _createShowAllButton: function() {
	        var input = this.input,
	          wasOpen = false;
	 		
	        $( "<a>" )
	          .attr( "tabIndex", -1 )
	          .attr( "title", "Show All Items" )
	          .tooltip()
	          .appendTo( this.wrapper )
	          .button({
	            icons: {
	              primary: "ui-icon-triangle-1-s"
	            },
	            text: false
	          })
	          .removeClass( "ui-corner-all" )
	          .addClass( "ui-corner-right" )
	          .on( "mousedown", function() {
	            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
	          })
	          .on( "click", function() {
	            input.trigger( "focus" );
	 
	            // Close if already visible
	            if ( wasOpen ) {
	              return;
	            }
	 			
	            // Pass empty string as value to search for, displaying all results
	            input.autocomplete( "search", "" );
	          });
	      },
	 
	      _source: function( request, response ) {
	        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
	        response( this.element.children( "option" ).map(function() {
	          var text = $( this ).text();
	          if ( this.value && ( !request.term || matcher.test(text) ) )
	            return {
	              label: text,
	              value: text,
	              option: this
	            };
	        }) );
	      },
	 
	      _removeIfInvalid: function( event, ui ) {
	 
	        // Selected an item, nothing to do
	        if ( ui.item ) {
	          return;
	        }
	 
	        // Search for a match (case-insensitive)
	        var value = this.input.val(),
	          valueLowerCase = value.toLowerCase(),
	          valid = false;
	        this.element.children( "option" ).each(function() {
	          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
	            this.selected = valid = true;
	            return false;
	          }
	        });
	 
	        // Found a match, nothing to do
	        if ( valid ) {
	          return;
	        }
	 
	        // Remove invalid value
	        this.input
	          .val( "" )
	          .attr( "title", value + " didn't match any item" )
	          .tooltip( "open" );
	        this.element.val( "" );
	        this._delay(function() {
	          this.input.tooltip( "close" ).attr( "title", "" );
	        }, 2500 );
	        this.input.autocomplete( "instance" ).term = "";
	      },
	 
	      _destroy: function() {
	        this.wrapper.remove();
	        this.element.show();
	      }
	    });
	 	
	    $( "#combobox" ).combobox();
     } );
    // AUTOCOMPLETE (FIN)
});