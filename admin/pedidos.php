<?php
	session_start();
	if(!isset($_SESSION['id']))
		header('Location: /admin');
	
	include ('../vendor/autoload.php');
	include ('inc/bd.php');
	
	$pedidos	= $db->pedido
				->select()
				->run();

	$estados	= $db->estado
				->select()
				->run();

	include('inc/header.php');
	$seccion = 'pedidos';
?>
	<body>

		<?php include('inc/user_menu.php');?>
		<?php include('inc/admin_menu.php');?>


		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">									
					<div class="panel panel-default">
						<!-- Default panel contents -->
						<div class="panel-body">
							<ul class="nav nav-pills nav-justified" id="referencias">
								<li role="presentation"><a href="#"><span class="icon-bullet b1"></span>Ingresado</a></li>
								<li role="presentation"><a href="#"><span class="icon-bullet b2"></span>En espera</a></li>
								<li role="presentation"><a href="#"><span class="icon-bullet b3"></span>En impresión</a></li>
								<li role="presentation"><a href="#"><span class="icon-bullet b4"></span>Terminado</a></li>
								<li role="presentation"><a href="#"><span class="icon-bullet b5"></span>Entregado</a></li>
								<li role="presentation"><a href="#"><span class="icon-bullet b6"></span>Frenado</a></li>
								<li role="presentation"><a href="#"><span class="icon-bullet b7"></span>Cancelado</a></li>
								<li role="presentation"><a href="crear_pedido.php"><button type="submit" class="btn adminBtn">AGREGAR PEDIDO</button></a></li>
							</ul>
						</div>

						<hr class="separador">

						<!-- Table -->
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>ORDEN</th>
									<th>CLIENTE</th>
									<th>PRODUCTO</th>
									<th>ENTREGA </th>
									<th>ESTADO</th>
									<th>RESPONSABLE</th>
									<th>RESTA</th>
									<th>FACTURA</th>
								</tr>
							</thead>
							<tbody>
<?php 						foreach($pedidos as $pedido)
							{
?>
								<tr>
									<td><a href="crear_pedido.php?id=<?php echo $pedido->id;?>" style="color: #3C365F;"><?php echo $pedido->orden;?></td>
									<td><?php echo $pedido->cliente->nombre;?></td>
									<td><?php echo $pedido->cantidad.' '.$pedido->categoria->nombre;?></td>
									<td><?php echo $pedido->entregaEstimada->format('d-m-Y');?></td>
									<td>
										<div class="input-group">
	  										<span class="icon-bullet noBorder b<?php echo $pedido->estado_id;?>" style="vertical-align:bottom"></span>
	  										<div class="form1">
												<select name="estado" data-rel="<?php echo $pedido->id;?>" class="form-control cambioEstado">
<?php 												foreach($estados as $estado)
														echo "<option value='{$estado->id}' ".($pedido->estado_id == $estado->id ? 'selected=selected' : '').">{$estado->nombre}</option>";
?>
												</select>
											</div>
										</div>
									</td>
									<td><?php echo $pedido->responsable->nombre;?></td>
									<td><?php echo $pedido->total - $pedido->sena;?></td>
									<td>
										<div class="form2">
											<div class="checkbox">
												<input id="option2" type="checkbox" class="image-checkbox" />    
												<label for="option2" class="image-checkbox-label"></label>
											</div>
										</div>
									</td>
								</tr>
<?php 						}
?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<ul class="pager">
			<li><a href="#"><span class="icon-prev"></span></a></li>
			<li>Página</li>
			<li><a href="#"><span class="icon-next"></span></a></li>
		</ul>
<?php
	include('inc/footer.php');	
?>