<?php
	include ('../vendor/autoload.php');
	include ('inc/bd.php');
    
    //Get the data from the serverRequest
    $request = Zend\Diactoros\ServerRequestFactory::fromGlobals($_POST);
	$data 	= $request->getParsedBody();

	//Agregar usuario 
	$db->usuario
    ->insert()
    ->data(['usuario'    => $data['nombre'],
            'clave'     => password_hash($data['clave'], PASSWORD_DEFAULT)
            ])->run();
    
    header('Location: usuarios.php');
?>