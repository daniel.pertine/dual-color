<?php
    include 'inc/header.php';
?>

    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <img src="images/promociones.png" alt="Promociones" class="img-responsive">
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>