<?php
    include 'inc/header.php';
?>

    <div class="container invitaciones">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="invitaciones-casamientos.php">Casamientos</a></li>
                <li><a href="invitaciones-15años.php">15 Años</a></li>
                <li><a href="invitaciones-cumpleaños.php">Cumpleaños</a></li>
                <li><a href="invitaciones-bautismos.php">Bautismos</a></li>
                <li class="active"><a href="invitaciones-primera-comunion.php">Primera Comunión</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <div class="row">
                    <ul class="invitacionesProducto list-inline">
                        <li>
                            <a href="invitaciones-producto.php"><img src="images/invitaciones/casamiento/casamiento01.jpg" width="180"></a>
                        </li>
                        <li>
                            <a href="invitaciones-producto.php"><img src="images/invitaciones/casamiento/casamiento02.jpg" width="180"></a>
                        </li>
                        <li>
                            <a href="invitaciones-producto.php"><img src="images/invitaciones/casamiento/casamiento03.jpg" width="180"></a>
                        </li>
                        <li>
                            <a href="invitaciones-producto.php"><img src="images/invitaciones/casamiento/casamiento04.jpg" width="180"></a>
                        </li>
                        <li>
                            <a href="invitaciones-producto.php"><img src="images/invitaciones/casamiento/casamiento05.jpg" width="180"></a>
                        </li>
                        <li>
                            <a href="invitaciones-producto.php"><img src="images/invitaciones/casamiento/casamiento06.jpg" width="180"></a>
                        </li>
                        <li>
                            <a href="invitaciones-producto.php"><img src="images/invitaciones/casamiento/casamiento07.jpg" width="180"></a>
                        </li>
                        <li>
                            <a href="invitaciones-producto.php"><img src="images/invitaciones/casamiento/casamiento08.jpg" width="180"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>