<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="papeles-texturados.php">Texturados</a></li>
                <li><a href="papeles-perlados.php">Perlados</a></li>
                <li><a href="papeles-calcos.php">Calcos</a></li>
                <li class="active"><a href="papeles-estandar.php">Estándar</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h3>IMPRESIÓN DIGITAL</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Papeles</th>
                                    <th>Tamaños Disponibles</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Obra 80 gr</td>
                                    <td>A4 / A3</td>
                                </tr>
                                <tr>
                                    <td>Ilustración mate / brillo 115 gr</td>
                                    <td>A4 / A3 / SA3</td>
                                </tr>
                                <tr>
                                    <td>Ilustración mate / brillo 170 gr</td>
                                    <td>A4 / A3 / SA3</td>
                                </tr>
                                <tr>
                                    <td>Ilustración mate / brillo 250 gr</td>
                                    <td>A4 / A3</td>
                                </tr>
                                <tr>
                                    <td>Ilustración mate / brillo 300 gr</td>
                                    <td>A4 / A3 / SA3</td>
                                </tr>
                                <tr>
                                    <td>Opalina 250 gr</td>
                                    <td>A4 / A3 / SA3</td>
                                </tr>
                                <tr>
                                    <td>Autoadhesivo mate</td>
                                    <td>A4 / A3 / SA3</td>
                                </tr>
                                <tr>
                                    <td>Autoadhesivo brillo</td>
                                    <td>A4 / A3 / SA3</td>
                                </tr>
                                <tr>
                                    <td>Autoadhesivo transparente</td>
                                    <td>A4 / A3 / SA3</td>
                                </tr>
                                <tr>
                                    <td>Bookcel 90 gr</td>
                                    <td>A4 / A3 / SA3</td>
                                </tr>
                                <tr>
                                    <td>Sulfito</td>
                                    <td>A4 / A3 / SA3</td>
                                </tr>
                                <tr>
                                    <td>Papel vegetal 90 gr</td>
                                    <td>A4 / A3</td>
                                </tr>
                                <tr>
                                    <td>Filmina</td>
                                    <td>A4 / A3</td>
                                </tr>
                                <tr>
                                    <td>Misionero 250 gr</td>
                                    <td>A4 / A3 / SA3</td>
                                </tr>
                                <tr>
                                    <td>Sublimación</td>
                                    <td>A4 / A3</td>
                                </tr>
                                <tr>
                                    <td>Transfer (para telas claras)</td>
                                    <td>A4 / A3</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <h3>PLOTEOS</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Papeles</th>
                                    <th>Tamaños Disponibles</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Obra 90 gr</td>
                                    <td>Ancho de 100 cm</td>
                                </tr>
                                <tr>
                                    <td>Recubierto 150 gr</td>
                                    <td>Ancho de 100 cm</td>
                                </tr>
                                <tr>
                                    <td>Fotográfico Brillo 170 gr</td>
                                    <td>Ancho de 100 cm</td>
                                </tr>
                                <tr>
                                    <td>Fotográfico Mate Satín 260 gr</td>
                                    <td>Ancho de 100 cm</td>
                                </tr>
                                <tr>
                                    <td>Vegetal 90 gr</td>
                                    <td>Ancho de 100 cm</td>
                                </tr>
                                <tr>
                                    <td>Canvas / Lienzo</td>
                                    <td>Ancho de 60 cm</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>