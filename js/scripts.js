function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [
        ['Atención al Público', -34.5822841,-58.4251646],
        ['Producción Grafica y Sublimacion en Gran Formato', -34.585923,-58.4304247]
    ];
                        
    // Info Window Content
    var infoWindowContent = [
        ['<div class="info_content">' +
        '<h3>Atención al Público</h3>' +
        '<p>Thames 2395</p>' +
        '</div>'],
        ['<div class="info_content">' +
        '<h3>Producción Grafica y<br/> Sublimacion en Gran Formato</h3>' +
        '<p>Nicaragua 4914</p>' +
        '</div>']
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));


        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(15);
        google.maps.event.removeListener(boundsListener);
    });    
}


$(document).ready(function(){

	// Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "//maps.googleapis.com/maps/api/js?callback=initialize&key=AIzaSyAMljpTARcXg7qvuF-H5Eg9IHdDpxPcb78";
    document.body.appendChild(script);

    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });

    //Google ReCaptcha
	$('#legitSubmit').click(function(event) {
		if(grecaptcha.getResponse() == "")
		    alert("Falta confirmar el captcha!");
		else{
		    $('#submit').trigger('click');
		}
	});

	/* Custom Carousel */
	$('#myCarousel').carousel({
		interval: 4000
	});

	// handles the carousel thumbnails
	$('[id^=carousel-selector-]').click( function(){
		var id_selector = $(this).attr("id");
		var id = id_selector.substr(id_selector.length -1);
		id = parseInt(id);
		$('#myCarousel').carousel(id);
		$('[id^=carousel-selector-]').removeClass('selected');
		$(this).addClass('selected');
	});

	// when the carousel slides, auto update
	$('#myCarousel').on('slid', function (e) {
		var id = $('.item.active').data('slide-number');
		id = parseInt(id);
		$('[id^=carousel-selector-]').removeClass('selected');
		$('[id=carousel-selector-'+id+']').addClass('selected');
	});

	// Masonry + imagesLoaded

	var $grid = $('.grid').imagesLoaded( function() {
	  // init Masonry after all images have loaded

		$grid.masonry({
		    itemSelector: '.grid-item',
			columnWidth: 194,
			//gutter: 5
		});

	});

});