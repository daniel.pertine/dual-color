<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li class="active"><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2>LISTA DE PRECIOS</h2>
                <h3>CARPETAS TAMAÑO A4 CERRADO – CON SOLAPA IMPRESA</h3>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Simple Faz</th>
                            <th>Adicional Laminado S/F</th> 
                            <th>Doble Faz</th>
                            <th>Adicional Laminado D/F</th>
                        </tr>                        
                    </thead>
                    <tbody>
                        <tr>
                            <td>x1</td>
                            <td>$39</td> 
                            <td>$30</td>
                            <td>$65</td>
                            <td>$50</td>
                        </tr>
                        <tr>
                            <td>x5</td>
                            <td>$195</td> 
                            <td>$95</td>
                            <td>$312</td>
                            <td>$142</td>
                        </tr>
                        <tr>
                            <td>x10</td>
                            <td>$351</td> 
                            <td>$120</td>
                            <td>$546</td>
                            <td>$156</td>
                        </tr>
                        <tr>
                            <td>x20</td>
                            <td>$676</td> 
                            <td>$215</td>
                            <td>$1027</td>
                            <td>$278</td>
                        </tr>
                        <tr>
                            <td>x50</td>
                            <td>$1346</td> 
                            <td>$485</td>
                            <td>$2145</td>
                            <td>$630</td>
                        </tr>
                        <tr>
                            <td>x100</td>
                            <td>$2522</td> 
                            <td>$900</td>
                            <td>$3796</td>
                            <td>$1120</td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tfoot>
                        <tr>
                            <td colspan="5">*Simple faz: impreso frente, Doble faz: impreso dorso, NO INTERIOR.<br/>
                                *Los precios pueden modificarse sin previo aviso.<br/>
                                *Los precios no incluyen IVA
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <br>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2>ESPECIFICACIONES Y CONSEJOS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <ul>
                            <li>Los carpetas son impresas en papel ilustración de 300 grs. En el caso de encargarlas sobre papeles especiales, el uso de los mismos se cotizará aparte.</li>
                            <li>Aceptamos archivos preferentemente en formato .AI o .PDF editable. En el caso de ser formato .JPG, debe contener demasía y poseer entre 150 y 300 dpi de resolución.</li>
                            <li>Convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas. En el caso de no incrustarlas, guardar las imágenes utilizadas en una carpeta.</li>
                            <li>El archivo debe tener una demasía mínima de 3 mm de cada lado.</li>
                        </ul>                       
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>