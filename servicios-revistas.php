<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li class="active"><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2>LISTA DE PRECIOS</h2>
                <h3>BROCHURES/CATÁLOGOS</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>A5</th>
                                    <th>A4</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x1</td>
                                    <td>$66</td>
                                    <td>$114</td>
                                </tr>
                                <tr>
                                    <td>x5</td>
                                    <td>$222</td>
                                    <td>$274</td>
                                </tr>
                                <tr>
                                    <td>x10</td>
                                    <td>$420</td>
                                    <td>$528</td>
                                </tr>
                                <tr>
                                    <td>x20</td>
                                    <td>$792</td>
                                    <td>$960</td>
                                </tr>
                                <tr>
                                    <td>x50</td>
                                    <td>$1710</td>
                                    <td>$2310</td>
                                </tr>
                                <tr>
                                    <td>x100</td>
                                    <td>$2820</td>
                                    <td>$4020</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="3">*Los precios pueden modificarse sin previo aviso.<br/>
                                        *Los precios no incluyen IVA.
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <br>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2>ESPECIFICACIONES Y CONSEJOS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <ul>
                            <li>Impresión full color en papel de 115 gr doblado y encuadernación acaballada (engrapado en el centro/lomo). 12 páginas.</li>
                            <li>Tamaño A5 cerrado: 3 hojas A4 doble faz / Tamaño A4 cerrado: 3 hojas A3 doble faz.</li>
                        </ul>                       
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>