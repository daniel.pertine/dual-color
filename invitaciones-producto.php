<?php
    include 'inc/header.php';
?>

    <div class="container invitaciones">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li class="active"><a href="#">Casamientos</a></li>
                <li><a href="#">15 Años</a></li>
                <li><a href="#">Cumpleaños</a></li>
                <li><a href="#">Bautismos</a></li>
                <li><a href="#">Primera Comunión</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <div class="row">
                    <div class="col-md-6" style="padding:0;">
                        <!-- main slider carousel -->
                        <div class="col-md-12" id="slider">
                            <div class="col-md-12" id="carousel-bounding-box">
                                <div id="myCarousel" class="carousel slide">
                                    <!-- main slider carousel items -->
                                    <div class="carousel-inner">
                                        <div class="active item" data-slide-number="0">
                                            <img src="images/invitaciones/casamiento/floral/floral.jpg" class="img-responsive">
                                        </div>
                                        <div class="item" data-slide-number="1">
                                            <img src="images/invitaciones/casamiento/floral/floral.jpg" class="img-responsive">
                                        </div>
                                        <div class="item" data-slide-number="2">
                                            <img src="images/invitaciones/casamiento/floral/floral.jpg" class="img-responsive">
                                        </div>
                                        <div class="item" data-slide-number="3">
                                            <img src="images/invitaciones/casamiento/floral/floral.jpg" class="img-responsive">
                                        </div>
                                        <div class="item" data-slide-number="4">
                                            <img src="images/invitaciones/casamiento/floral/floral.jpg" class="img-responsive">
                                        </div>
                                        <div class="item" data-slide-number="5">
                                            <img src="images/invitaciones/casamiento/floral/floral.jpg" class="img-responsive">
                                        </div>
                                        <div class="item" data-slide-number="6">
                                            <img src="images/invitaciones/casamiento/floral/floral.jpg" class="img-responsive">
                                        </div>
                                        <div class="item" data-slide-number="7">
                                            <img src="images/invitaciones/casamiento/floral/floral.jpg" class="img-responsive">
                                        </div>
                                        <div class="item" data-slide-number="8">
                                            <img src="images/invitaciones/casamiento/floral/floral.jpg" class="img-responsive">
                                        </div>
                                        <div class="item" data-slide-number="9">
                                            <img src="images/invitaciones/casamiento/floral/floral.jpg" class="img-responsive">
                                        </div>
                                    </div>
                                    <!-- main slider carousel nav controls -->
                                    <a class="carousel-control left" href="#myCarousel" data-slide="prev"><img src="images/arrowL.svg" style="width:35px; margin-left: 20px;"></a>
                                    <a class="carousel-control right" href="#myCarousel" data-slide="next"><img src="images/arrowR.svg" style="width:35px; margin-right: 20px;"></a>
                                </div>
                            </div>
                        </div>
                        <!--/main slider carousel-->

                        <!-- thumb navigation carousel -->
                        <div class="col-md-12 hidden-sm hidden-xs" id="slider-thumbs">
                            <!-- thumb navigation carousel items -->
                            <ul class="list-inline">
                                <li>
                                    <a id="carousel-selector-0" class="selected">
                                        <img src="images/invitaciones/casamiento/floral/floral3.jpg" class="img-responsive">
                                    </a>
                                </li>
                                <li>
                                    <a id="carousel-selector-1">
                                        <img src="images/invitaciones/casamiento/floral/floral3.jpg" class="img-responsive">
                                    </a>
                                </li>
                                <li>
                                    <a id="carousel-selector-2">
                                        <img src="images/invitaciones/casamiento/floral/floral3.jpg" class="img-responsive">
                                    </a>
                                </li>
                                <li>
                                    <a id="carousel-selector-3">
                                        <img src="images/invitaciones/casamiento/floral/floral3.jpg" class="img-responsive">
                                    </a>
                                </li>
                                <li>
                                    <a id="carousel-selector-4">
                                        <img src="images/invitaciones/casamiento/floral/floral3.jpg" class="img-responsive">
                                    </a>
                                </li>
                                <li>
                                    <a id="carousel-selector-5">
                                        <img src="images/invitaciones/casamiento/floral/floral3.jpg" class="img-responsive">
                                    </a>
                                </li>
                                <li>
                                    <a id="carousel-selector-6">
                                        <img src="images/invitaciones/casamiento/floral/floral3.jpg" class="img-responsive">
                                    </a>
                                </li>
                                <li>
                                    <a id="carousel-selector-7">
                                        <img src="images/invitaciones/casamiento/floral/floral3.jpg" class="img-responsive">
                                    </a>
                                </li>
                                <li>
                                    <a id="carousel-selector-8">
                                        <img src="images/invitaciones/casamiento/floral/floral3.jpg" class="img-responsive">
                                    </a>
                                </li>
                                <li>
                                    <a id="carousel-selector-9">
                                        <img src="images/invitaciones/casamiento/floral/floral3.jpg" class="img-responsive">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <h2>CASAMIENTOS</h2>
                                <h3>Modelo Floral</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="#">
                                    <span class="favorito">Añadido a Favoritos</span>
                                    <img src="images/check.svg" alt="Añadir a Favoritos" style="width: 17px; margin: -5px 0 0px 3px;">
                                </a>
                                <br>
                                <a href="#">
                                    <span class="quiero">Lo Quiero!</span><img src="images/heart.svg" alt="Lo Quiero!" style="width: 18px; margin: 10px 4px 12px 6px;">
                                </a>
                            </div>
                        </div>
                        <hr>
                        <h4>CONTENIDO DEL KIT</h4>
                        <ul>
                            <li>Tarjetón de 13 x 18 cm</li>
                            <li>Sobre 13,5 x 18,5 cm en papel blanco, negro, crema, texturado o Misionero de 140 gr</li>
                        </ul>

                        <p style="margin-bottom: 0;">Extras / Opcionales:</p>
                        <ul>
                            <li>2 tarjetas 8,5 x 5,4 cm (Fiesta & Regalo)</li>
                            <li>Stickers Cierrasobres</li>
                            <li>Lining interno para sobres</li>
                        </ul>

                        <h4>MÁS INFORMACIÓN</h4>
                        <p>
                            Tarjetón y tarjeta de Regalo en papel Ilustracion de 300 gr impresos full color.<br>
                            Terminación Premium en laminado OPP mate (suave al tacto y mas rigidas y resistentes).<br>
                            Tarjeta de Fiesta en Kraft extra rigido de 300 gr.
                        </p>
                        <p>Consultar por otras opciones de papel (lineas Cordenons, Fedrigoni y Arjowiggins).</p>
                    </div>
                </div>
                <h2 style="margin-top:30px; margin-bottom:15px; text-align: left;">PRODUCTOS RELACIONADOS</h2>
                <div class="row">
                    <div class="col-md-3"><a href="invitaciones-producto.php"><img src="images/invitaciones/casamiento/floral/floral2.jpg"></a></div>
                    <div class="col-md-3"><a href="invitaciones-producto.php"><img src="images/invitaciones/casamiento/floral/floral2.jpg"></a></div>
                    <div class="col-md-3"><a href="invitaciones-producto.php"><img src="images/invitaciones/casamiento/floral/floral2.jpg"></a></div>
                    <div class="col-md-3"><a href="invitaciones-producto.php"><img src="images/invitaciones/casamiento/floral/floral2.jpg"></a></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>