<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li class="active"><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2>LISTA DE PRECIOS</h2>
                <h3>PLOTEOS CAD</h3>
                <hr>
                <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Líneas Blanco y Negro</th>
                            <th>Hatch Blanco y Negro</th> 
                            <th>Líneas Color</th>
                            <th>Hatch Color</th>
                        </tr>                        
                    </thead>
                    <tbody>
                        <tr>
                            <td>Obra 90 gr</td>
                            <td>$45,00</td> 
                            <td>$85,00</td>
                            <td>$95,00</td>
                            <td>$115,00</td>
                        </tr>
                        <tr>
                            <td>Recubierto 150 gr</td>
                            <td>$150,00</td> 
                            <td>$160,00</td>
                            <td>$170,00</td>
                            <td>$185,00</td>
                        </tr>
                        <tr>
                            <td>Vegetal 90 gr</td>
                            <td>$115,00</td> 
                            <td>$145,00</td>
                            <td>$155,00</td>
                            <td>$175,00</td>
                        </tr>
                    </tbody>
                </table>                
                <br>
                <h3>PLOTEOS</h3>
                <hr>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>PAPELES</th>
                                    <th>METRO CUADRADO</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Obra Color 90 gr</td>
                                    <td>$125,00</td>
                                </tr>
                                <tr>
                                    <td>Obra BN 90 gr</td>
                                    <td>$115,00</td>
                                </tr>
                                <tr>
                                    <td>Recubierto 150 gr</td>
                                    <td>$215,00</td>
                                </tr>
                                <tr>
                                    <td>Fotográfico Brillo 170 gr</td>
                                    <td>$315,00</td>
                                </tr>
                                <tr>
                                    <td>Fotográfico Mate Satín 260 gr</td>
                                    <td>$415,00</td>
                                </tr>
                                <tr>
                                    <td>Vegetal 90 gr</td>
                                    <td>$195,00</td>
                                </tr>
                                <tr>
                                    <td>Sobre tu papel o papel especial</td>
                                    <td>$195,00</td>
                                </tr>
                                <tr>
                                    <td>Canvas Poliéster 100x60 cm (metro lineal)</td>
                                    <td>$355,00</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2">*Los precios pueden modificarse sin previo aviso.<br/>
                                        *Los precios no incluyen IVA.
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <br>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h2>ESPECIFICACIONES Y CONSEJOS</h2>
                        <img src="images/arrow.png" class="center-block">
                        <h3>PLOTEOS  INKjET</h3>
                        <ul>
                            <li>No realizamos ploteos doble faz.</li>
                            <li>El ancho imprimible del plotter es de 100 cm. La superficie mínima para plotear es de 30 cm de alto. El costo se calcula por metro lineal, tener en cuenta la posición de los archivos y ubicarlos de forma horizontal, para aprovechar el ancho de 100 cm.</li>
                            <li>En el caso de plotear la superficie total de pliegos sueltos, contemplar un margen perimetral no imprimible de, al menos, 1 cm.</li>
                            <li>Recomendamos guardar los archivos en formato .PDF ó .JPG en modo de color CMYK (no RGB ni Pantone).</li>
                            <li>No aceptamos archivos de paquete office (Microsoft Word, Excel, Powerpoint, etc.). En el caso de utilizar estos programas, guardar los archivos en formato .PDF.</li>
                            <li>Para archivos de Autocad, preferentemente guardarlos en formato .PDF. En el caso de imprimir desde Autocad, tener presente la escala deseada y aclarar si incluye puntas.</li>
                            <li>Sólo aceptamos papeles especiales desde 90 grs hasta 300 grs, que no contengan encapados plásticos, ni residuos sueltos sobre la superficie ó en los bordes de la hoja.</li>
                            <li>En el caso de los archivos que contengan imágenes de gran tamaño, al exportar en formato .PDF, acoplar las capas.</li>
                            <li>Recomendamos trabajar con una resolución mínima de 150 dpi y máxima de 300 dpi.</li>
                            <li>Convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas. En el caso de no incrustarlas, guardar las imágenes utilizadas en una carpeta.</li>
                        </ul>
                        <h3>ENVÍO DE ARCHIVOS Y PEDIDOS ONLINE</h3>
                        <ul>
                            <li>En el nombre de cada archivo, especificar tamaño de ploteo, tipo de papel y gramaje, y cantidad de copias.</li>
                        </ul>                        
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>