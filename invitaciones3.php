<?php
    include 'inc/header.php';
?>

    <div class="container invitaciones">
        <div class="row">
            <div class="col-md-12">

                <!-- Masonry -->
                <div class="grid">
                    <div class="grid-item grid-item--width2 grid-item--height2">
                        <a href="#"><img src="images/invitaciones/grilla-01.jpg" style="width:388px; height:388px;"></a>
                    </div>
                    <div class="grid-item grid-item--width2">
                        <a href="#"><img src="images/invitaciones/grilla-02.jpg" style="width:388px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-03.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-04.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-05.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-06.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item grid-item--height2">
                        <a href="#"><img src="images/invitaciones/grilla-07.jpg" style="width:194px; height:388px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-08.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-09.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item grid-item--width2">
                        <a href="#"><img src="images/invitaciones/grilla-10.jpg" style="width:388px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-11.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-12.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-13.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-14.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-15.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-16.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-17.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-18.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-19.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-20.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item grid-item--width2">
                        <a href="#"><img src="images/invitaciones/grilla-21.jpg" style="width:388px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-22.jpg" style="width:194px; height:194px;"></a>
                    </div>
                    <div class="grid-item">
                        <a href="#"><img src="images/invitaciones/grilla-23.jpg" style="width:194px; height:194px;"></a>
                    </div>
                </div>

            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>