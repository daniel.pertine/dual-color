<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-12">
                <h2>AYUDA</h2>
                <h3>ENVÍO DE ARCHIVOS Y PEDIDOS ONLINE</h3>
                <hr>
                <h3 class="ayuda">IMPRESIÓN DIGITAL</h3>
                <p>
                    En el nombre de cada archivo, especificar tamaño de hoja, tipo de papel y gramaje, y cantidad de copias. En el caso de ser doble faz y presentar las caras separadas en distintos archivos, nombrarlos de la misma forma, pero indicando “frente” y “dorso” como corresponda.
                </p>
                <h3 class="ayuda">PLOTEOS</h3>
                <p>
                    En el nombre de cada archivo, especificar tamaño de ploteo, tipo de papel y gramaje, y cantidad de copias.
                </p>
                <h3 class="ayuda">VINILO DE CORTE</h3>
                <p>
                    Convertir las tipografías a curvas / contornos y unificar trazados superpuestos. Preferentemente trabajamos archivos .ai (illustrator). En el caso de utilizar corel, convertir con compatibilidad para illustrator. Mínimo de corte 2 mm de espesor para trazos, y 1 cm de alto para tipografías.
                </p>
                <h3 class="ayuda">TARJETAS PERSONALES / VOLANTES / FOLLETOS / POSTALES</h3>
                <p>
                El archivo debe tener una demasía mínima de 3 mm de cada lado. Para encargo de tarjetas personales, utilizar el archivo disponible en esta web. Aceptamos archivos preferentemente en formato .AI o .PDF editable. Convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas. En el caso de no incrustarlas, guardar las imágenes utilizadas en una carpeta. En el caso de ser formato .JPG, debe contener demasía y poseer entre 150 y 300 dpi de resolución.
                </p>
                <a href="dual_color_tarjetas_personales.pdf"><button type="button" class="btn btn-default">Descargar PDF</button></a>
                <h3 class="ayuda">AUTOADHESIVO IMPRESO Y TROQUELADO</h3>
                <p>
                    Para encargos de troquelados, utilizar el archivo disponible en esta web. El área de troquel es de 28x38 cm. La distancia mínima de corte entre cada trazo de troquel es de 2 mm. Aceptamos archivos en formato .AI o .PDF editable (convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas). Los stickers deben tener una demasía mínima de 3 mm de cada lado.
                </p>
                <a href="dual_color_base_troquel.pdf"><button type="button" class="btn btn-default">Descargar PDF</button></a>
                <br>
                <br>
                <h3 id="Consejos">CONSEJOS</h3>
                <hr>
                <ul>
                    <li>
                        Recomendamos guardar los archivos en formato .PDF ó .JPG en modo de color CMYK (no RGB ni Pantone).
                    </li>
                    <li>
                        No aceptamos archivos de paquete office (Microsoft Word, Excel, Powerpoint, etc.). En el caso de utilizar estos programas, guardar los archivos en formato .PDF.
                    </li>
                    <li>
                        Para archivos de Autocad, preferentemente guardarlos en formato .PDF. En el caso de imprimir desde Autocad, tener presente la escala deseada y aclarar si incluye puntas.
                    </li>
                    <li>
                        Al preparar los archivos, tener en cuenta que para todos los soportes existe un área no imprimible de 5 mm de cada lado. Asimismo, recomendamos dejar demasía de 5 mm para los cortes, y en el caso de impresiones doble faz, contemplar que puede haber un desfasaje máximo de 3 mm.
                    </li>
                    <li>
                        Sólo aceptamos papeles especiales desde 90 grs hasta 300 grs, que no contengan encapados plásticos, ni residuos sueltos sobre la superficie ó en los bordes de la hoja, y estén cortados en los tamaños estandarizados.
                    </li>
                    <li>
                        En el caso de los archivos que contengan imágenes de gran tamaño, al exportar en formato .PDF, acoplar las capas.
                    </li>
                    <li>
                        Recomendamos trabajar con una resolución mínima de 150 dpi y máxima de 300 dpi.
                    </li>
                    <li>
                        Convertir las tipografías a curvas / contornos, e incrustar las imágenes insertadas. En el caso de no incrustarlas, guardar las imágenes utilizadas en una carpeta.
                    </li>
                </ul>                       
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>