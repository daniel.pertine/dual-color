<?php
    include 'inc/header.php';
?>

    <div class="container servicios">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li><a href="servicios-impresion-digital.php">Impresión Digital</a></li>
                <li><a href="servicios-ploteos.php">Ploteos</a></li>
                <li><a href="servicios-gigantografias.php">Gigantografías</a></li>
                <li><a href="servicios-tarjetas-personales.php">Tarjetas Personales</a></li>
                <li><a href="servicios-volantes.php">Volantes</a></li>
                <li><a href="servicios-postales.php">Postales y Señaladores</a></li>
                <li><a href="servicios-stickers.php">Stickers</a></li>
                <li><a href="servicios-imanes.php">Imanes y Pines</a></li>
                <li><a href="servicios-recetarios.php">Recetarios</a></li>
                <li><a href="servicios-carpetas.php">Carpetas</a></li>
                <li class="active"><a href="servicios-laminados.php">Laminados</a></li>
                <li><a href="servicios-encuadernacion.php">Encuadernación</a></li>
                <li><a href="servicios-revistas.php">Revistas</a></li>
                <li><a href="servicios-sublimacion.php">Sublimación</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <h2>LISTA DE PRECIOS</h2>
                <h3>LAMINADO BRILLO O MATE – SIMPLE FAZ</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>A4</th>
                                    <th>A3</th>
                                    <th>SA3</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x1</td>
                                    <td>$25</td>
                                    <td>$30</td>
                                    <td>$40</td>
                                </tr>
                                <tr>
                                    <td>x10</td>
                                    <td>$100</td>
                                    <td>$120</td>
                                    <td>$130</td>
                                </tr>
                                <tr>
                                    <td>x20</td>
                                    <td>$180</td>
                                    <td>$215</td>
                                    <td>$230</td>
                                </tr>
                                <tr>
                                    <td>x50</td>
                                    <td>$400</td>
                                    <td>$485</td>
                                    <td>$510</td>
                                </tr>
                                <tr>
                                    <td>x100</td>
                                    <td>$750</td>
                                    <td>$900</td>
                                    <td>$920</td>
                                </tr>
                                <tr>
                                    <td>x200</td>
                                    <td>$1210</td>
                                    <td>$1320</td>
                                    <td>$1350</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>
                </div>
                <h3>LAMINADO BRILLO O MATE – DOBLE FAZ</h3>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>A4</th>
                                    <th>A3</th>
                                    <th>SA3</th>
                                </tr>                        
                            </thead>
                            <tbody>
                                <tr>
                                    <td>x1</td>
                                    <td>$45</td>
                                    <td>$50</td>
                                    <td>$60</td>
                                </tr>
                                <tr>
                                    <td>x10</td>
                                    <td>$130</td>
                                    <td>$156</td>
                                    <td>$166</td>
                                </tr>
                                <tr>
                                    <td>x20</td>
                                    <td>$230</td>
                                    <td>$278</td>
                                    <td>$290</td>
                                </tr>
                                <tr>
                                    <td>x50</td>
                                    <td>$520</td>
                                    <td>$630</td>
                                    <td>$645</td>
                                </tr>
                                <tr>
                                    <td>x100</td>
                                    <td>$975</td>
                                    <td>$1120</td>
                                    <td>$1150</td>
                                </tr>
                                <tr>
                                    <td>x200</td>
                                    <td>$1573</td>
                                    <td>$1710</td>
                                    <td>$1760</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">
                                        *Los precios pueden modificarse sin previo aviso.<br/>
                                        *Los precios no incluyen IVA.
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>