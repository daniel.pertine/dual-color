
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-12"><hr></div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3">
                        <h3>NOSOTROS</h3>
                        <ul class="list-unstyled">
                            <li><a href="sobre-nosotros.php">Sobre Nosotros</a></li>
                            <li><a href="contacto.php">Cómo llegar</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2 col-md-2">
                        <h3>AYUDA</h3>
                        <ul class="list-unstyled">
                            <li><a href="envio-de-archivos.php">Envío de archivos</a></li>
                            <li><a href="envio-de-archivos.php#Consejos">Consejos</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <h3>USUARIO</h3>
                        <ul class="list-unstyled">
                            <li><a href="#">Mi cuenta</a></li>
                            <li><a href="#">Olvidé mi contraseña</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <ul class="list-unstyled" style="margin-top: 6px; font-size: 12px">
                            <li>Atencion al Publico:<br/>Thames 2395 / 011 4774-9186</li>
                            <li>Producción Grafica y Sublimacion en Gran Formato:<br/>Nicaragua 4914 / 011 4831 0950</li>
                            <li>
                                <a href="contacto.php" class="icon-mail"></a>
                                <a href="https://web.facebook.com/dualcolorgrafica/" class="icon-facebook" target="_blank" title="Seguinos en Facebook"></a>
                                <a href="https://www.instagram.com/graficadualcolor/" target="_blank" title="Seguinos en Instagram"><img src="../images/instagram.svg" class="icon-instagram" style="vertical-align: top" /></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>        
        </footer>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-3.1.0.min.js" type="text/javascript"></script>
        <script src="js/jquery.fancybox.js" type="text/javascript"></script>
        <script src="js/scripts.js" type="text/javascript"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

    </body>
    </html>