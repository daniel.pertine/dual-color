<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Dual Color</title>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/fancybox/jquery.fancybox.css" rel="stylesheet" media="screen">
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
    <header>
        <div class="container hidden-xs">
            <div class="row" style="padding-top: 50px; padding-bottom: 50px;">
                <div class="col-sm-4 col-md-4">
                    <p style="font-size:12px;">Atencion al Publico:<br/>Thames 2395 / 011 4774-9186</p>
                    <p style="font-size:12px;">Producción Grafica y Sublimacion en Gran Formato:<br/>Nicaragua 4914 / 011 4831 0950</p>
                </div>
                <div class="col-sm-4 col-md-4">
                    <a href="index.php"><h1 class="icon-logo"></h1></a>
                </div>
                <div class="col-sm-4 col-md-4">
                    <ul class="social"">
                        <li><a href="contacto.php" class="icon-mail" title="Escribinos"></a></li>
                        <li><a href="https://web.facebook.com/dualcolorgrafica/" class="icon-facebook" target="_blank" title="Seguinos en Facebook"></a></li>
                        <li><a href="https://www.instagram.com/graficadualcolor/" target="_blank" title="Seguinos en Instagram"><img src="../images/instagram.svg" class="icon-instagram" /></a></li>
                        <!--<li><a href="#" class="icon-user" title="Ingresá a tu cuenta"></a></li>-->
                    </ul>
                </div>
            </div>
        </div>
        <div id="menuBar" class="container-fluid">
            <nav class="navbar navbar-default" style="background-color: #3e3762;">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand visible-xs icon-logo" href="#"></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="container">
                        <div class="collapse navbar-collapse" style="padding: 10px;" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-default" role="navigation">
                                <li><a href="servicios.php">SERVICIOS</a></li>
                                <li><a href="proximamente.php">INVITACIONES</a></li>
                                <li><a href="vinilos.php">VINILOS</a></li>
                                <li><a href="proximamente.php">DISEÑOS</a></li>
                                <li><a href="papeles-texturados.php">PAPELES</a></li>
                                <li><a href="contacto.php">CONTACTO</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div>
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </header>