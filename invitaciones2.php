<?php
    include 'inc/header.php';
?>

    <div class="container invitaciones">
        <div class="row">
            <div class="col-md-2">
                <ul class="menuServicios">
                <li class="active"><a href="#">Casamientos</a></li>
                <li><a href="#">15 Años</a></li>
                <li><a href="#">Cumpleaños</a></li>
                <li><a href="#">Bautismos</a></li>
                <li><a href="#">Primera Comunión</a></li>
                </ul>
            </div>
            <div class="col-md-10" style="border-left: 1px solid #d55915;">
                <div class="row">
                    <div class="col-md-3"><a href="#"><img src="images/invitaciones/casamiento/casamiento01.jpg" width="180"></a></div>
                    <div class="col-md-3"><a href="#"><img src="images/invitaciones/casamiento/casamiento02.jpg" width="180"></a></div>
                    <div class="col-md-3"><a href="#"><img src="images/invitaciones/casamiento/casamiento03.jpg" width="180"></a></div>
                    <div class="col-md-3"><a href="#"><img src="images/invitaciones/casamiento/casamiento04.jpg" width="180"></a></div>
                    <div class="col-md-3"><a href="#"><img src="images/invitaciones/casamiento/casamiento05.jpg" width="180"></a></div>
                    <div class="col-md-3"><a href="#"><img src="images/invitaciones/casamiento/casamiento06.jpg" width="180"></a></div>
                    <div class="col-md-3"><a href="#"><img src="images/invitaciones/casamiento/casamiento07.jpg" width="180"></a></div>
                    <div class="col-md-3"><a href="#"><img src="images/invitaciones/casamiento/casamiento08.jpg" width="180"></a></div>
                </div>
            </div>
        </div>
    </div>

<?php
    include 'inc/footer.php';
?>